package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.EcoSystemIOSLib;
import com.haygroup.ios.page.EcoSystemPageIOS;
import com.haygroup.web.page.EcoSystemPage;



public class TC_IOS_ActivateMobile_ACT_002 extends EcoSystemIOSLib {

	@DataProvider
	public Object[][] getTestDataFor_EcoSystemspage() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_EcoSystemspage")
	public void ActivateMobile002Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.activatemobile");	
				loginToIOSApplication(data.get("UserNameMobile"), data.get("PasswordMobile"));
				optimizeforChina(data.get("UserNameMobile"), data.get("PasswordMobile"));
				mobileSettingsPageButtonIOS();
				assertTextMatching(EcoSystemPageIOS.getDynamicLocatorByID(data.get("UserNameMobile")), data.get("UserNameMobile"), "UserName Verification");
				clickDoneButton();
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		logOutMobileApplication();

		//captureBrowserConsoleLogs(Driver);
	}
}


