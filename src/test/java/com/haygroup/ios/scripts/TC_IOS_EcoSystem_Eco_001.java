package com.haygroup.ios.scripts;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.ios.libs.EcoSystemIOSLib;
import com.haygroup.ios.page.EcoSystemPageIOS;
import com.haygroup.web.libs.JobDescriptionLib;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;


/*This Test is To create a failure Job description*/
public class TC_IOS_EcoSystem_Eco_001 extends EcoSystemIOSLib {
	@DataProvider
	public Object[][] getTestDataFor_EcoSystemspage() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_EcoSystemspage")
	public void EcoSystems001IOSTest(Hashtable<String, String> data) {
		//public void mobileSettingsPage002Test() {
		try {
			//if (data.get("RunMode").equals("Y")) {
			this.reporter.initTestCaseDescription("TC.settingspage");	
			/* Login to the application */
			loginToIOSApplication(data.get("UserNameMobile"), data.get("PasswordMobile"));
			//loginToIOSApplication("user.auto1@haygroup.com", "Activate123");
			mobileSettingsPageButtonIOS();
			languagechange_mobile_IOS(data.get("LanguageMobile"));
			loginToIOSApplication(data.get("UserNameMobile"), data.get("PasswordMobile"));
			mobileSettingsPageButtonIOS();
			assertTextMatching(EcoSystemPageIOS.inputTypeLanguage(data.get("LanguageMobile")), data.get("LanguageMobile"), "Language Verification");
			languagechange_mobile_IOS(data.get("DefaultLanguage"));
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		//logOutMobileApplication();

		//captureBrowserConsoleLogs(Driver);
	}
}
