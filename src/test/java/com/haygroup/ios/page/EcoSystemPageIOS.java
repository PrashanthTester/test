package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class EcoSystemPageIOS {

	public static By mobileSettingsButton;
	public static By backButton;
	public static By overLayMenu;
	public static By selectPreferedLanguage;
	public static By okBtn;
	public static By languageIcon;
	public static By saveBtn;
	public static By dynamicLocator;
	public static By doneBtn;
	public static By optimizeforChina;
	public static By notifyIcon_rightScreen;
	public static By zoomBtn;
	
	

	


	static {

		mobileSettingsButton=By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton");
//		notifyIcon_rightScreen = By.xpath("(//XCUIElementTypeOther//XCUIElementTypeOther[3])[1]/XCUIElementTypeOther");
		notifyIcon_rightScreen = By.xpath("//AppiumAUT/XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[20]/XCUIElementTypeOther");
		zoomBtn = By.id("minus");
		backButton = By.xpath("//android.widget.ImageButton[contains(@content-desc,'Navigate up')]");
		overLayMenu=By.className("android.widget.ImageButton");
		selectPreferedLanguage = By.xpath("//XCUIElementTypeCell[1]");
		saveBtn = By.xpath("//XCUIElementTypeNavigationBar/XCUIElementTypeButton");
		okBtn = By.xpath("//XCUIElementTypeAlert/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton");
		doneBtn = By.id("Done");
		optimizeforChina = By.id("Optimize for China");
		


	}
	
	public static By inputLanguage(String language){

		return languageIcon = By.name(language);
	}
	public static By inputTypeLanguage(String language){

		return languageIcon = By.id(language);
	}
	public static By getDynamicLocatorByID(String locator){

		return dynamicLocator = By.id(locator);
	}

}
