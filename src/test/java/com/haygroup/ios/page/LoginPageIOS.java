package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class LoginPageIOS {


	public static By userName_mobile;
	public static By password_mobile;
	public static By loginBtn_mobile;
	public static By loginBtn_Journey;
	
	
	public static By JM_username;
	public static By JM_password;
	public static By JM_LoginBut;   
	
	//Journey
	public static By journeyUN;
	public static By journeyPW;
	
		public static By userNameJourney_mobile;
		public static By passwordJourney_mobile;
		public static By journeySignInBtn;
	
	
	public static By logOut_mobile;

	/* Page Objects Of Login Page */
	static {
		userName_mobile = By.className("XCUIElementTypeTextField");
		password_mobile 	= By.className("XCUIElementTypeSecureTextField");
		loginBtn_mobile 	= By.xpath("//XCUIElementTypeButton[4]");
		loginBtn_Journey    = By.xpath("//XCUIElementTypeButton[2]");
		JM_username = By.xpath("//input[1]");
		JM_password = By.xpath("//input[2]");
		JM_LoginBut=By.xpath("//input[3]");
		userNameJourney_mobile = By.id("com.haygroup.journey.android:id/txtUsername");
		passwordJourney_mobile = By.id("com.haygroup.journey.android:id/txtPassword");
		journeySignInBtn = By.id("com.haygroup.journey.android:id/btnSignIn");
	}
}