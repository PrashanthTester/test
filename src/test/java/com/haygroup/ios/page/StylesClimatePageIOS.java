package com.haygroup.ios.page;

import org.openqa.selenium.By;

public class StylesClimatePageIOS {

	public static By enterFeedbackmanually;
	public static By continueButton;
	public static By modalPopUpHeader;
	public static By okButton;
	public static By skipButton;
	public static By enterUniqueCodeLabel;
	public static By uniqueCodeField;
	public static By roadMapLabel;
	public static By designMyPlan;
	public static By designMyOwnPlan;
	public static By myPlanTrip;
	public static By toDoList;
	public static By continueOneOnOne;
	public static By continueDisabled;
	public static By checkBoxList;
	public static By editReview;
	public static By markCompletebBtn;
	public static By nextWeekBtn;
	public static By finalReviewtext;
	public static By checkBoxListInCalander;
	public static By clickCheckBox;
	public static By enterStyleFeedback;
	public static By enterClimateRaterFeedback;
	public static By doneButton;
	public static By numerator;
	public static By tipPopupHeaderText;
	public static By congratsPopUp;
	public static By detailsPopUp;
	public static By detailsPopUpText;
	public static By detailPopUpEditLink;
	public static By appointmentDropDown;
	public static By appointmentDropDownOptions;
	public static By detailPopUpDoneLink;
	public static By dateField;
	public static By selectNextDate;
	public static By noOfTips;
	

	/* Page Objects Of Styles and Climate */
	static {
		enterFeedbackmanually 			= By.xpath(".//a[@id='manual-feedback-button']");
		continueButton 					= By.xpath(".//*[@id='my-plan-continue']");
		modalPopUpHeader 				= By.xpath(".//*[@id='app-modal']//div[@class='modal-header']/strong");
		okButton 						= By.xpath(".//*[@id='ok-button']");
		skipButton 						= By.xpath("//a[text()='Skip']");
		enterUniqueCodeLabel 			= By.xpath("//span[@class='uppercase' and text()='ENTER UNIQUE CODE']");
		uniqueCodeField 				= By.xpath(".//*[@id='qr-section']/input");
		roadMapLabel 					= By.xpath(".//*[@id='styles-climate-road-map']//h3[@class='header-container text-center']//span");
		designMyPlan 					= By.xpath(".//button[text()='Design My Plan']");
		designMyOwnPlan 				= By.xpath(".//button[text()='Design My Own Plan']");
		myPlanTrip 						= By.xpath(".//*[@id='my-plan-tip-type']");
		continueOneOnOne 				= By.xpath("//button[@data-title='Continue']");
		continueDisabled 				= By.xpath("//button[@data-title='Continue' and @disabled='']");
		toDoList 						= By.xpath(".//*[@id='calendar']");
		checkBoxList 					= By.xpath(".//input[@type='checkbox']");
		editReview 						= By.xpath(".//*[@id='tip-review']");
		markCompletebBtn 				= By.xpath(".//*[@id='mark-complete-submit']");
		nextWeekBtn 					= By.xpath(".//button[@class='btn date-pagination right']");
		finalReviewtext 				= By.xpath(".//*[@id='plan-complete']//p");
		checkBoxListInCalander 			= By.xpath("(//input[@type='checkbox' and not(@checked)])");
		clickCheckBox					= By.xpath("(//input[@type='checkbox' and not(@checked)])[1]");
		enterStyleFeedback              = By.xpath("//*[@id='stylesTable']/table/tbody/tr[*]/td[3]/input");
		enterClimateRaterFeedback       = By.xpath("//*[@id='climateTable']/table/tbody/tr[*]/td[4]/input");
		doneButton                      = By.xpath("//*[text() = 'Done']");
		numerator                       = By.id("numerator");
		tipPopupHeaderText              = By.tagName("h3"); 
		congratsPopUp                   = By.xpath("//*[@id='tip-complete']/div/div[1]");
		detailsPopUp                    = By.id("view-edit-tip-modal");
		detailsPopUpText                = By.xpath("//h3[text()='DETAIL']");
		detailPopUpEditLink             = By.linkText("Edit");
		detailPopUpDoneLink             = By.linkText("Done");
		appointmentDropDown             = By.id("s2id_select2-roster-current-appointment");
		appointmentDropDownOptions      = By.xpath("//ul[@class = 'day-grid current-week']/li/div/ul/li/a");
	}
}
