package com.haygroup.ios.libs;

import java.util.Set;

import com.gallop.accelerators.ActionEngine;
import com.haygroup.android.page.LoginPageAndroid;
import com.haygroup.ios.page.EcoSystemPageIOS;
import com.haygroup.ios.page.HomePageIOS;
import com.haygroup.ios.page.LoginPageIOS;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;
//import com.haygroup.web.page.LoginPage;


public class HayGroupIOSCommonLib extends ActionEngine{

	/*	Login to application with User Name and Password*/



	public void loginToIOSApplication(String UserName, String Password) throws Throwable {	

		verifyLogOutBtn();
		waitTillElementToBeClickble(LoginPageIOS.userName_mobile, "entering login username");
		//appiumDriver.findElement(LoginPageIOS.userName_mobile).sendKeys("user.auto1@haygroup.com");
		type(LoginPageIOS.userName_mobile,"user.auto1@haygroup.com", "entering login username");
		//appiumDriver.hideKeyboard();
		//appiumDriver.findElement(LoginPageIOS.password_mobile).sendKeys("Activate123");
		type(LoginPageIOS.password_mobile, Password, "login password");
		//appiumDriver.hideKeyboard();SIGN IN
		//appiumDriver.findElement(LoginPageIOS.loginBtn_mobile).click();
		click(LoginPageIOS.loginBtn_mobile, "Login button");
	}

	public void loginToJourneyIOSApplication(String UserName, String Password) throws Throwable {	

		verifyLogOutBtn();
		waitTillElementToBeClickble(LoginPageIOS.userName_mobile, "entering login username");
		//appiumDriver.findElement(LoginPageIOS.userName_mobile).sendKeys("user.auto1@haygroup.com");
		type(LoginPageIOS.userName_mobile,"user.auto1@haygroup.com", "entering login username");
		//appiumDriver.hideKeyboard();
		//appiumDriver.findElement(LoginPageIOS.password_mobile).sendKeys("Activate123");
		type(LoginPageIOS.password_mobile, Password, "login password");
		//appiumDriver.hideKeyboard();SIGN IN
		//appiumDriver.findElement(LoginPageIOS.loginBtn_mobile).click();
		click(LoginPageIOS.loginBtn_Journey, "Login button");
	}

	public void optimizeforChina(String UserName, String Password){
		try {
			waitTillElementToBeClickble(LoginPageIOS.userName_mobile, "entering login username");
			//appiumDriver.findElement(LoginPageIOS.userName_mobile).sendKeys("user.auto1@haygroup.com");
			type(LoginPageIOS.userName_mobile,UserName, "entering login username");
			//appiumDriver.hideKeyboard();
			//appiumDriver.findElement(LoginPageIOS.password_mobile).sendKeys("Activate123");
			type(LoginPageIOS.password_mobile, Password, "login password");
			//appiumDriver.hideKeyboard();SIGN IN
			//appiumDriver.findElement(LoginPageIOS.loginBtn_mobile).click();
			waitTillElementToBeClickble(EcoSystemPageIOS.optimizeforChina, "optimizeforChina button");
			click(EcoSystemPageIOS.optimizeforChina, "optimizeforChina button");
			click(LoginPageIOS.loginBtn_mobile, "Login button");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void verifyLogOutBtn(){
		if(verifyElementPresent(HomePageIOS.logOut_mobile, "Logout btn")){
			try {
				waitTillElementToBeClickble(HomePageIOS.logOut_mobile, "click on logout button");
				click(HomePageIOS.logOut_mobile, "Logout button");
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}
	}

	public void loginToJourneyMobileApplication(String UserName, String Password) throws Throwable {
		//checkPageIsReady();
		//waitTillElementToBeClickble(LoginPage.userNameJourney_mobile, "entering login username");
		//click(LoginPage.journeyUN, "Login username");
		type(LoginPageAndroid.userNameJourney_mobile,UserName, "entering login username");
		type(LoginPageAndroid.passwordJourney_mobile, Password,"login password");
		click(LoginPageAndroid.journeySignInBtn, "Login button");
	}

	public void mobileSettingsPageButtonIOS() throws Throwable {
		try {
			waitTillElementToBeClickble(EcoSystemPageIOS.mobileSettingsButton, "Settings button");
			click(EcoSystemPageIOS.mobileSettingsButton, "Settings button");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void notifyIconsJourneyIOS() throws Throwable {
		try {
			//waitTillElementToBeClickble(EcoSystemPageIOS.notifyIcon_rightScreen, "notifyIcon_rightScreen button");
			
			//click(EcoSystemPageIOS.zoomBtn, "zoomBtn button");
			click(EcoSystemPageIOS.notifyIcon_rightScreen, "notifyIcon_rightScreen button");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	public void navigateBack(){
		try {
			click(EcoSystemPage.backButton, "Back Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void verifyGetContextHandles(){
		Set<String> contextNames = this.appiumDriver.getContextHandles();
		for (String contextName : contextNames) {
			System.out.println(contextName);
		}
	}
	public void openOverLayMenu(){
		try {
			waitTillElementToBeClickble(EcoSystemPage.overLayMenu,"click on menu");
			click(EcoSystemPage.overLayMenu,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void clickDoneButton(){
		try {
			waitTillElementToBeClickble(EcoSystemPageIOS.doneBtn,"Wait for Done btn to be clickable");
			click(EcoSystemPageIOS.doneBtn,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*Log out from application*/
	public void logOutFromApplication() throws Throwable{
		JSClick(HomePage.logOutButton, "Log out");
	}

	public void logOutMobileApplication() throws Throwable{
		waitTillElementToBeClickble(HomePageIOS.logOut_mobile, "Log out Mobile");
		click(HomePageIOS.logOut_mobile, "Log out Mobile");
	}

}
