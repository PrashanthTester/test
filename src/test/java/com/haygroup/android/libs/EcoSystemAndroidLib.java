package com.haygroup.android.libs;

import java.util.concurrent.TimeUnit;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.EcoSystemPageAndroid;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;


public class EcoSystemAndroidLib extends HayGroupAndroidCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/**
	 * This function performs click operation on settings button
	 * 
	 */

	public void settingspagebutton() {

		try {

			click(EcoSystemPage.settingsbutton, "click on settings");
			isApplicationReady();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * This function performs click operation on location dropdown
	 * 	 
	 * 
	 */

	public void locationchange() {

		try {

			click(EcoSystemPage.locationdropdown, "click on location dropdown");
			click(EcoSystemPage.locationchange, "change location");
			waitForVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed", 30);
			waitForInVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}


	public void homeLocation() throws Throwable {
		try {
			isApplicationReady();
			waitTillElementToBeClickble(EcoSystemPageAndroid.Homelocation, "Homelocationclicked");
			click(EcoSystemPageAndroid.Homelocation, "Homelocationclicked");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void changeLocation(String location) throws Throwable{
		try {
			//isApplicationReady();
			type(EcoSystemPageAndroid.Editlocation,location,"enter location");
			Waittime();
			Thread.sleep(5000);
			click(EcoSystemPageAndroid.clickmylocation, "click location");
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		}
	
	
		
	
	public void userPreferredCurrency(String currency)  throws Throwable
	{
		try {
			//isApplicationReady();
			waitTillElementToBeClickble(EcoSystemPageAndroid.selectPreferedCurrency, "selectPreferedCurrency");
			
			click(EcoSystemPageAndroid.selectPreferedCurrency ,"");
			//click(EcoSystemPageAndroid.userprefferedcurrencylist,"currency should selected");
			click(EcoSystemPageAndroid.inputCurrency(currency),"click on currency");
					
		} catch (Exception e) {
			// TODO: handle exception
		}	
		
	}
		public void userPreferredTimeZone(String timezone)  throws Throwable
		{
			try {
				//isApplicationReady();
			//	waitTillElementToBeClickble(EcoSystemPageAndroid.selectPreferredTimezone, "selectPreferedTimezone");
				click(EcoSystemPageAndroid.selectPreferredTimezone ,"selectedCurrency");
				//click(EcoSystemPageAndroid.userPreferredtimezonelist,"Selectpreferedtimezone");
				 click(EcoSystemPageAndroid.inputTimezone(timezone),"click ontimezone");
						
			} catch (Exception e) {
				// TODO: handle exception
			}	
		}
		
		public void userpreferencetoreceiveemailnotifications(String emailnotifications) throws Throwable
		{
			try
			{
				isApplicationReady();
				click(EcoSystemPageAndroid.userpreferencetoreceiveemailnotifications,"clickonreceiveemailnotifications");
				//click(EcoSystemPageAndroid.sendmenotificationsaswhenrequired, "clickonnotifications");
				click(EcoSystemPageAndroid.inputemailnotifications(emailnotifications), "clickemailnotifications");
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		public void userpreferenceforActionPlanreminderEmailNotifications(String ActionPlanremindernotification) throws Throwable
		
		{
			try
			{
		//	waitTillElementToBeClickble(EcoSystemPageAndroid.ActionPlanremindersByEmailnotifications, "Emailnotifications");
			click(EcoSystemPageAndroid.ActionPlanremindersByEmailnotifications,"Click on Email notifications");
			//click(EcoSystemPageAndroid.sendmenotifications,"click sendmenotifications on a dailybasis");
			click(EcoSystemPageAndroid.inputActionPlanreminder(ActionPlanremindernotification),"clickemailnotification ");
			}
			catch(Exception e)
			{
				e.printStackTrace();	
			}
			
		}
public void MyPlanPreferenceforWeekStartingDay(String WeekStaringDay) throws Throwable
		
		{
	try
	{
			//waitTillElementToBeClickble(EcoSystemPageAndroid.MyPlanPreferenceforWeekStartingDay, "");
			click(EcoSystemPageAndroid.MyPlanPreferenceforWeekStartingDay,"Click on select day");
			//click(EcoSystemPageAndroid.Preferenceweekstartingday,"Click on Monday");
			click(EcoSystemPageAndroid.inputWeekStaringDay(WeekStaringDay)," click weekstarting day");
			click(EcoSystemPageAndroid.WeekEndingDay_okbtn,"Ok button");	
	}
			
			catch(Exception e)
			{
				e.printStackTrace();	
			}
			
		}
public void MyplanpreferenceforWeekEndingDay(String WeekEndingDay) throws Throwable	
{
	try
	{
	//waitTillElementToBeClickble(EcoSystemPageAndroid.MyplanpreferenceforWeekEndingDay, "");
	click(EcoSystemPageAndroid.MyplanpreferenceforWeekEndingDay,"Click on select day");
	//click(EcoSystemPageAndroid.PreferenceweekEndingday,"Click on Friday");
	click(EcoSystemPageAndroid.inputWeekEndingDay(WeekEndingDay),"Click on Wekendingday");
	
	click(EcoSystemPageAndroid.WeekEndingDay_okbtn,"Ok button");
	}
	catch(Exception e)
	{
		e.printStackTrace();	
	}
	
}
public void MyplanpreferencesforteammeetingDay(String teammeeting) throws Throwable	
{
	try
	{
	//waitTillElementToBeClickble(EcoSystemPageAndroid.MyplanpreferencesforteammeetingDay, "");
	click(EcoSystemPageAndroid.MyplanpreferencesforteammeetingDay,"Click on select day");
	//click(EcoSystemPageAndroid.Preferenceforteammeetingday,"Click on Monday");
	click(EcoSystemPageAndroid.inputTeammeeting(teammeeting),"Click onTeammeeting");

	click(EcoSystemPageAndroid.WeekEndingDay_okbtn,"Ok button");
	
	}
	catch(Exception e)
	{
		e.printStackTrace();	
	}
}

public void Teammeetingtime() throws Throwable	
{

	try
	{
	//waitTillElementToBeClickble(EcoSystemPageAndroid.Teammeetingtime, "");
	click(EcoSystemPageAndroid.Teammeetingtime,"Click on select day");
	click(EcoSystemPageAndroid.PreferenceTeammeetingtime,"Click on okbutton");
	click(EcoSystemPageAndroid.WeekEndingDay_okbtn, "click on button");
	}
	catch(Exception e)
	{
		e.printStackTrace();	
	}

}


 public void Logout() throws Throwable
		{	
			
		try 
		{
			click(EcoSystemPageAndroid.Logout," MenuLogoutbutton");
			click(EcoSystemPageAndroid.popupLogoutbtn,"Logoutbuttonclicked");
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
								
	
	}
	
	
	

	
	public void navigateBack(){
		try {
			click(EcoSystemPage.backButton, "Back Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void openOverLayMenu(){
		try {
			waitTillElementToBeClickble(EcoSystemPage.overLayMenu,"click on menu");
			click(EcoSystemPage.overLayMenu,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*Log out from application*/
	public void logOutFromApplication() throws Throwable{
		JSClick(HomePage.logOutButton, "Log out");
	}
	
	public void contactAdministrator(String message) throws Throwable {

		waitTillElementToBeClickble(EcoSystemPageAndroid.contactadmin, "click on contact admin");
		click(EcoSystemPageAndroid.contactadmin, "click on contact admin");
		waitTillElementToBeClickble(EcoSystemPageAndroid.adminselect, "click on admin");
		click(EcoSystemPageAndroid.adminselect, "click on admin");
		waitTillElementToBeClickble(EcoSystemPageAndroid.nextbutton, "click on next button");
		click(EcoSystemPageAndroid.nextbutton, "click on admin");
		waitForVisibilityOfElement(EcoSystemPageAndroid.entertext, message, 30);
		type(EcoSystemPageAndroid.entertext, message, "enter text");
		waitTillElementToBeClickble(EcoSystemPageAndroid.sendbutton, "wait for send button");
		click(EcoSystemPageAndroid.sendbutton, "click on send");

	}
	
	public void clearhomepage() {

		try {

			click(EcoSystemPageAndroid.touch, "click on settings");
			isApplicationReady();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	
public void languagechange_mobile(String languagechange, String defaultLanguage) throws Throwable {
		
		Thread.sleep(4000);
		swipe("android:id/list", "User Preferred Language");
		swipe("android:id/list", defaultLanguage);
		waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		click(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
		click(EcoSystemPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPageAndroid.okBtn, "OK Button");
		click(EcoSystemPageAndroid.okBtn, "OK Button");
	}
	
public void languagechangeforChina_mobile(String changedLanguage, String defaultLanguage) throws Throwable {
		
		Thread.sleep(4000);
		//swipe("android:id/list", "User Preferred Language");
		swipe("android:id/list", changedLanguage);
		waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
		click(EcoSystemPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		click(EcoSystemPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPageAndroid.okBtn, "OK Button");
		click(EcoSystemPageAndroid.okBtn, "OK Button");
	}

	public void logout() throws Throwable {

		try {
			swipe("android:id/list", "Log Out");
			click(EcoSystemPageAndroid.logout, "click on logout");
			click(EcoSystemPageAndroid.logoutbutton, "click on logout button");
		} catch (Exception e) {

		}

	}

}
