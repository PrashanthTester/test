package com.haygroup.android.libs;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.StylesClimatePageAndroid;
import com.haygroup.web.page.HomePage;


public class StylesClimateAndroidLib extends HayGroupAndroidCommonLib{
	
	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	
	public void clearScreenHideOnHomePage(){
		try{
			waitForVisibilityOfElement(StylesClimatePageAndroid.homePageGridView, "Waiting for Styles&Climate Tab", 80);
			click(StylesClimatePageAndroid.homePageGridView, "Click on Styles&Climate Tab");
		}catch(Throwable e){}
	}
	
	public void openStylesAndClimate() throws Throwable{
		waitForVisibilityOfElement(StylesClimatePageAndroid.stylesAndClimateTab, "Waiting for Styles&Climate Tab", 80);
		click(StylesClimatePageAndroid.stylesAndClimateTab, "Click on Styles&Climate Tab");
	}
	
	public void removeScreenHider(By screenHider){
		try{
			waitForVisibilityOfElement(screenHider, "Waiting for Styles&Climate Tab", 80);
			click(screenHider, "Click on Styles&Climate Tab");
		}catch(Throwable e){}
	}
	
	public void verifyLevelUpPage() throws Throwable{
		waitForVisibilityOfElement(StylesClimatePageAndroid.yourRangeofStylesText, "Waiting for LevelUp Page", 80);
		assertTextMatching(StylesClimatePageAndroid.yourRangeofStylesText, "Your Range of Styles", "Verify user is on LevelUp Screen");
		click(StylesClimatePageAndroid.yourRangeofStylesText, "Click on Your Range of Styles Text");
	}
	
	public void scrollDownToElement(String locator, String text) {
		try
			{
				AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
				AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+locator+"\")).scrollIntoView(UiSelector().textContains(\""+text+"\"))");
		    }
		catch(Exception e)
		   {
				System.out.println(e);
		   }
	}
	
	public void clickCheckboxes() throws Throwable
	{
		Waittime();
		AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
		List<WebElement> elements = AndroidDriver.findElementsById("com.haygroup.activate:id/selected");
		System.out.println(elements.size());
		
		for (WebElement element : elements)
		{
			Thread.sleep(3000);
			
			element.click();
		}
	}
	
	public  int  returnDate()
	{
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year  = localDate.getYear();
		int month = localDate.getMonthValue();
		int day   = localDate.getDayOfMonth();
		
		return day;
	}
	
	public void  clickdate(int index)
	{
		try
		{
		AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
		WebElement ele =AndroidDriver.findElement(By.xpath("//android.view.View[@index='"+index+"']"));//android.widget.NumberPicker[@index='0']"))
		ele.click();
		}
		catch(Exception e)
		{
			
			System.out.println(e);
			
		}
	}
	
	public void clickScanQRCode() throws Throwable{
		waitForVisibilityOfElement(StylesClimatePageAndroid.swipToScanQRCode, "Waiting for Scan QR Code Text", 80);
		click(StylesClimatePageAndroid.swipToScanQRCode, "Click on Scan QR Code Text");
		waitForVisibilityOfElement(StylesClimatePageAndroid.scanCodeOKButton, "Click OK Button", 80);
		click(StylesClimatePageAndroid.scanCodeOKButton, "Click OK Button");
		Waittime();
		isApplicationReady();
		Thread.sleep(3000);
	}
	
	public void clickEnterFeedBackManuallyButton()throws Throwable {
		Waittime();
		isApplicationReady();
		waitForVisibilityOfElement(StylesClimatePageAndroid.clickEnterFeedBackManually, "Click Enter feedback manually Button", 80);
		waitTillElementToBeClickble(StylesClimatePageAndroid.clickEnterFeedBackManually, "Click Enter feedback manually Button");
		click(StylesClimatePageAndroid.clickEnterFeedBackManually, "Click Enter feedback manually Button");
		waitForVisibilityOfElement(StylesClimatePageAndroid.YesButton, "Click yes Button", 80);
		click(StylesClimatePageAndroid.YesButton, "Click yes Button");
	}
	
	public void enterUinqueCode(String qrcode) throws Throwable{
		isApplicationReady();
		Waittime();
		waitForVisibilityOfElement(StylesClimatePageAndroid.uniqueCodeTextBox, "unique code textbox", 80);
		Waittime();
		type(StylesClimatePageAndroid.uniqueCodeTextBox, qrcode, "Enter qr code");
		Waittime();
		waitTillElementToBeClickble(StylesClimatePageAndroid.enterQrcodeContinueBtn, "Click continue Button");
		click(StylesClimatePageAndroid.enterQrcodeContinueBtn, "Click continue Button");
		isApplicationReady();	
		Waittime();
	}
	
	public void clickOneOneContinueButton() throws Throwable{
		Waittime();
		isApplicationReady();
		waitForVisibilityOfElement(StylesClimatePageAndroid.continueBtn, "Click continue Button", 80);
		waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
		click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
		Waittime();
		isApplicationReady();
		Thread.sleep(5000);
	}
	
	public void clickContinueButton() throws Throwable{
		Waittime();
		isApplicationReady();
		waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
		click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
		Waittime();
		Thread.sleep(5000);
	}
	
	public void clickDesignMyPlanButton() throws Throwable{
		isApplicationReady();
		Waittime();
		waitForVisibilityOfElement(ActivateMobilePageAndroid.designMyPlanBtn, "Click design My Plan Button", 80);
		click(ActivateMobilePageAndroid.designMyPlanBtn, "Click design MyPlan Button");
		click(ActivateMobilePageAndroid.designMyPlanBtn, "Click design MyPlan Button");
	}
	
	public void clickCreatePlanForMe() throws Throwable{
		waitForVisibilityOfElement(StylesClimatePageAndroid.createPlanForMeBtn, "Click Create Plan For Me Button", 80);
		click(StylesClimatePageAndroid.createPlanForMeBtn, "Click design My Own Plan Button");
		Waittime();
		isApplicationReady();
		Thread.sleep(15000);
	}
	
	public void clickDesignMyOwnPlanButton() throws Throwable{
		waitForVisibilityOfElement(StylesClimatePageAndroid.designMyOwnPlanBtn, "Click design My Own Plan Button", 80);
		click(StylesClimatePageAndroid.designMyOwnPlanBtn, "Click design My Own Plan Button");
		Waittime();
		isApplicationReady();
		
	}
	
	
	public void selectFiveTips() throws Throwable{
		Waittime();
		removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefour);
		click(StylesClimatePageAndroid.vStyle,"click on visionary style");
		Waittime();
		click(StylesClimatePageAndroid.vStyle,"click on visionary style");
		clickCheckboxes();
		Waittime();
		click(StylesClimatePageAndroid.aStyle,"click on Affiliative style");
		Waittime();
		clickCheckboxes();
		click(StylesClimatePageAndroid.But_Continue,"performing click on continue button");
		Waittime();
	}

	public void selectfourtips()  throws Throwable{
		
		scrollDownToElement("com.haygroup.activate:id/carddemo_list_expand","Using the Visionary Style");
		Waittime();
		click(StylesClimatePageAndroid.vStyle,"click on visionary style");
		Waittime();
		Waittime();
		click(StylesClimatePageAndroid.vStyle,"click on visionary style");
		clickCheckboxes();
		Waittime();
		click(StylesClimatePageAndroid.vStyle,"click on visionary style");
		Waittime();
		scrollDownToElement("com.haygroup.activate:id/carddemo_list_expand","Using the Pacesetting Style");
		Waittime();
		click(StylesClimatePageAndroid.pstyle,"click on Pacesetting style");
		clickCheckboxes();	
		Waittime();
		click(StylesClimatePageAndroid.But_Continue,"performing click on continue button");
		Waittime();
	}
	
	public void selectdate() throws Throwable{
		
		click(StylesClimatePageAndroid.vStyle,"click on visionary style");
		Waittime();
		scrollDownToElement("com.haygroup.activate:id/carddemo_list_expand","Using the Participative Style");
		Waittime();
		click(StylesClimatePageAndroid.participativestyle,"click on Participative style");
		clickCheckboxes();
		Waittime();
		int d=returnDate();
		clickdate(d);
		click(StylesClimatePageAndroid.But_OK,"");
		Waittime();
		click(StylesClimatePageAndroid.But_Continue,"performing click on continue button");
		Waittime();
	}
	
	public void Myplanpage()throws Throwable{
		
		Thread.sleep(8000);
		removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefive);
		assertTextMatching(StylesClimatePageAndroid.myPlanText, "My Plan", "My Plan text is verified");
	}
	
	
}
