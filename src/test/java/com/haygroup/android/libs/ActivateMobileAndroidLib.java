package com.haygroup.android.libs;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.LoginPageAndroid;
import com.haygroup.android.page.StylesClimatePageAndroid;

import io.appium.java_client.android.AndroidDriver;

public class ActivateMobileAndroidLib extends HayGroupAndroidCommonLib{
	ChromeDriver chdriver = null;
	
	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	
	public void loginToMobileChinaServer(String UserName, String Password){
	try {
		waitTillElementToBeClickble(LoginPageAndroid.userName_mobile, "entering login username");
		type(LoginPageAndroid.userName_mobile, UserName, "entering login username");
		type(LoginPageAndroid.password_mobile, Password, "login password");
		//Select the Checkbox
		click(ActivateMobilePageAndroid.chinaCheckBox_mobile, "select checkbox");
		click(LoginPageAndroid.loginBtn_mobile, "Login button");
	}catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

	}
	
	}
	
	
	public void clearScreenHideOnHomePage(){
		try{
			waitForVisibilityOfElement(ActivateMobilePageAndroid.homePageGridView, "Waiting for Styles&Climate Tab", 80);
			click(ActivateMobilePageAndroid.homePageGridView, "Click on Styles&Climate Tab");
		}catch(Throwable e){}
	}
	
	public void openStylesAndClimate() throws Throwable{
		waitForVisibilityOfElement(ActivateMobilePageAndroid.stylesAndClimateTab, "Waiting for Styles&Climate Tab", 80);
		click(ActivateMobilePageAndroid.stylesAndClimateTab, "Click on Styles&Climate Tab");
	}
	
	public void removeScreenHider(By screenHider) throws Throwable{
		Waittime();
		try{
			waitForVisibilityOfElement(screenHider, "Waiting for Styles&Climate Tab", 80);
			click(screenHider, "Click on Styles&Climate Tab");
		}catch(Throwable e){}
	}
	
	public void scrollDownToElement(String locator, String text) throws Throwable {
		try
			{
		  isApplicationReady();
			Waittime();
				AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
				AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+locator+"\")).scrollIntoView(UiSelector().textContains(\""+text+"\"))");
		    }
		catch(Exception e)
		   {
				System.out.println(e);
		   }
	}
	public void openQRcode() throws Throwable{
		Waittime();
		chdriver = new ChromeDriver();
		chdriver.get("C:\\GitLatest-Working\\kornferrytestautomation\\TestData\\PROD_QR_CODE.PNG");
	}
	
	public void clickScanQRCode() throws Throwable{
		waitForVisibilityOfElement(ActivateMobilePageAndroid.swipToScanQRCode, "Waiting for Scan QR Code Text", 80);
		click(ActivateMobilePageAndroid.swipToScanQRCode, "Click on Scan QR Code Text");
		waitForVisibilityOfElement(ActivateMobilePageAndroid.scanCodeOKButton, "Click OK Button", 80);
		click(ActivateMobilePageAndroid.scanCodeOKButton, "Click OK Button");
		Waittime();
		isApplicationReady();
		Waittime();
	}
	
	public void quitQRcode() throws Throwable{
		chdriver.quit();
		Waittime();
		Waittime();
		isApplicationReady();	
	}
	
	public void clickOneOneContinueButton() throws Throwable{
		Waittime();
		isApplicationReady();
		waitForVisibilityOfElement(StylesClimatePageAndroid.continueBtn, "Click continue Button", 80);
		waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
		click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
		Waittime();
		isApplicationReady();
		Thread.sleep(5000);
	}
	
	public void clickContinueButton() throws Throwable{
		Waittime();
		isApplicationReady();
		waitTillElementToBeClickble(StylesClimatePageAndroid.continueBtn, "continue Button");
		click(StylesClimatePageAndroid.continueBtn, "Click continue Button");
		Waittime();
		Thread.sleep(5000);
	}
	
	public void clickDesignMyPlanButton() throws Throwable{
		isApplicationReady();
		Waittime();
		waitForVisibilityOfElement(ActivateMobilePageAndroid.designMyPlanBtn, "Click design My Plan Button", 80);
		click(ActivateMobilePageAndroid.designMyPlanBtn, "Click design MyPlan Button");
		click(ActivateMobilePageAndroid.designMyPlanBtn, "Click design MyPlan Button");
	}
	
	public void clickCreatePlanForMe() throws Throwable{
		waitForVisibilityOfElement(ActivateMobilePageAndroid.createPlanForMeBtn, "Click Create Plan For Me Button", 80);
		click(ActivateMobilePageAndroid.createPlanForMeBtn, "Click design My Own Plan Button");
		Waittime();
		isApplicationReady();
		Thread.sleep(5000);
	}
	
	public void clickDesignMyOwnPlanButton() throws Throwable{
		waitForVisibilityOfElement(ActivateMobilePageAndroid.designMyOwnPlanBtn, "Click design My Own Plan Button", 80);
		click(ActivateMobilePageAndroid.designMyOwnPlanBtn, "Click design My Own Plan Button");
		Waittime();
		isApplicationReady();
	}		
	
public void Myplanpage()throws Throwable{
		
		Thread.sleep(4000);
		removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefive);
		assertTextMatching(StylesClimatePageAndroid.myPlanText, "My Plan", "My Plan text is verified");
	}

}
