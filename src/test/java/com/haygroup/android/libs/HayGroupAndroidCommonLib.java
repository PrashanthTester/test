package com.haygroup.android.libs;

import com.haygroup.android.page.HomePageAndroid;
import com.haygroup.android.page.LoginPageAndroid;
import java.util.concurrent.TimeUnit;

import com.gallop.accelerators.ActionEngine;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;

import io.appium.java_client.android.AndroidDriver;
//import com.haygroup.web.page.LoginPage;


public class HayGroupAndroidCommonLib extends ActionEngine{

	/*	Login to application with User Name and Password*/
	


	public void loginToMobileApplication(String UserName, String Password) throws Throwable {	
		waitTillElementToBeClickble(LoginPageAndroid.userName_mobile, "entering login username");
		type(LoginPageAndroid.userName_mobile, UserName, "entering login username");
		//appiumDriver.hideKeyboard();
		type(LoginPageAndroid.password_mobile, Password, "login password");
		//appiumDriver.hideKeyboard();
		
		click(LoginPageAndroid.loginBtn_mobile, "Login button");
	}
	
	public void loginToJourneyMobileApplication(String UserName, String Password) throws Throwable {
		//checkPageIsReady();
		//waitTillElementToBeClickble(LoginPage.userNameJourney_mobile, "entering login username");
		//click(LoginPage.journeyUN, "Login username");
		type(LoginPageAndroid.userNameJourney_mobile,UserName, "entering login username");
		type(LoginPageAndroid.passwordJourney_mobile, Password,"login password");
		click(LoginPageAndroid.journeySignInBtn, "Login button");
	}
	
	public void mobileSettingsPageButton() throws Throwable {
		try {
			//isApplicationReady();
			waitTillElementToBeClickble(EcoSystemPage.mobileSettingsButton, "Settings button");
			click(EcoSystemPage.mobileSettingsButton, "Settings button");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void navigateBack(){
		try {
			click(EcoSystemPage.backButton, "Back Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void openOverLayMenu(){
		try {
			waitTillElementToBeClickble(EcoSystemPage.overLayMenu,"click on menu");
			click(EcoSystemPage.overLayMenu,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*Log out from application*/
	public void logOutFromApplication() throws Throwable{
		JSClick(HomePage.logOutButton, "Log out");
	}

	public void logOutMobileApplication(){
	try {
		waitTillElementToBeClickble(HomePageAndroid.logOut_mobile, "Log out Mobile");
		click(HomePageAndroid.logOut_mobile, "Log out Mobile");
		waitTillElementToBeClickble(LoginPageAndroid.confirmLogOut_mobile, "Log out Mobile");
		click(LoginPageAndroid.confirmLogOut_mobile, "Log out Mobile");
	} catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}
	public  void swipe(String locator,String text){
	try
	{
	AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
	AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+locator+"\")).scrollIntoView(UiSelector().textContains(\""+text+"\"))");
	}
	catch(Exception e)
	{
	System.out.println(e);
	}
	}
public void tap()
{
	AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
	
	AndroidDriver.tap(1,134,526,1);
}

}
