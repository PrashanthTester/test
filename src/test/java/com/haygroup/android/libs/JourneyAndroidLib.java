package com.haygroup.android.libs;

import org.openqa.selenium.By;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.JourneyPageAndroid;
import com.haygroup.android.page.LoginPageAndroid;
import com.haygroup.web.page.EcoSystemPage;

public class JourneyAndroidLib extends HayGroupAndroidCommonLib{

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");


	public void clickSettingsButton() {

		try {
			isApplicationReady();
			click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
			click(JourneyPageAndroid.settingsBtn, "click on settings Button");
			isApplicationReady();

		}  catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
    public void loginToJourneyMobileChinaServer(String UserName, String Password) {
    try {
		
		type(LoginPageAndroid.userNameJourney_mobile,UserName, "entering login username");
		type(LoginPageAndroid.passwordJourney_mobile, Password,"login password");
		click(LoginPageAndroid.chinaServerCheckBox, "Select china server checkbox");
		click(LoginPageAndroid.journeySignInBtn, "Login button");
       }catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

	}
		
}

	public void clickGetStartedButton(){
		try{
			isApplicationReady();
			Waittime();
			waitTillElementToBeClickble(JourneyPageAndroid.getStrartedBtn,"click ok Button");
			click(JourneyPageAndroid.getStrartedBtn, "click on getStarted button");
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
	public void logOutJourney(){
		try {
		
		click(JourneyPageAndroid.logOutJourney, "Logout button");
		waitTillElementToBeClickble(LoginPageAndroid.journeySignInBtn,"click logout Button");
		
		}catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

	}
	}
		
		
		
		
		public void selectMyTitle(String name){
			try{
				isApplicationReady();
				waitTillElementToBeClickble(JourneyPageAndroid.selectMyTitle,"select my title Icon");
				click(JourneyPageAndroid.selectMyTitle, "select my title Icon");
				type(JourneyPageAndroid.myTitleTextBox, name, "Entering a Title");
				waitTillElementToBeClickble(JourneyPageAndroid.okBtn,"click ok Button");
				click(JourneyPageAndroid.okBtn, "click ok Button");
			}
			catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		}
		
		
		public void languagechange_mobile(String language) {
			try{
			waitTillElementToBeClickble(JourneyPageAndroid.selectPreferedLanguage,"click on Preffered languages");
			click(JourneyPageAndroid.selectPreferedLanguage,"click on Preffered languages");
			click(JourneyPageAndroid.inputLanguage(language),"click on Preffered languages");
			waitTillElementToBeClickble(JourneyPageAndroid.languageOkBtn,"OK Button");
			click(JourneyPageAndroid.languageOkBtn,"OK Button");
			Waittime();
			isApplicationReady();
		}
			catch (Throwable e) {

}

	}
		
		
		public void clickSettings () {

			try {
	             isApplicationReady();
	             try
	             { 
	            	 Thread.sleep(5000);
	            	 click(JourneyPageAndroid.btn_getStarted,"performing click on get started button");
	            	 Thread.sleep(5000);
	            	 click(JourneyPageAndroid.img_up,"performing click on up button");
	            	 
	            	 
	             }
	             
	             catch(Exception e )
	             {
	            	 System.out.println(e);
	             }
	             Thread.sleep(5000);
				click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
				Thread.sleep(5000);
				click(JourneyPageAndroid.settingsBtn, "click on settings Button");
			}
				 catch (Throwable e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
			}
		}
		
		public void swipeUpscreen() throws Throwable
		
		{
			try
			{
				isApplicationReady();
				//swipeup();
				swipe("android:id/list","Reset User Data");
				
			}
			catch(Exception e)
			{
				System.out.println(e);
			}

			} 
	public void swipeUpscreenFriendly() throws Throwable
		
		{
			try
			{
				isApplicationReady();
				//swipeup();
				swipe("android:id/list","Friendly Observation");
				
			}
			catch(Exception e)
			{
				System.out.println(e);
			}

			} 

	public void wipeUserdata() throws Throwable
		
		{
			try
			{
				
				click(JourneyPageAndroid.text_Unlock,"performing click on text unlock");
				click(JourneyPageAndroid.text_ResetUserdata,"performing click on text unlock");
				click(JourneyPageAndroid.btn_Ok,"performing click on OK button");
				click(JourneyPageAndroid.btn_Ok,"performing click on OK button");
				//click(JourneyPageAndroid.text_Unlock,"performing click on unlock text");
				
				//m_SwipeTop(Driver.findElement(JourneyPageAndroid.unlock));
			}
			catch(Exception e)
			{
				System.out.println(e);
			}

			} 

	public void userFriendlyData() throws Throwable

	{
		try
		{
			
			click(JourneyPageAndroid.text_friendly,"performing click on text friendly observation");
			click(JourneyPageAndroid.btn_Ok,"performing click on OK button");
			click(JourneyPageAndroid.btn_Ok,"performing click on yes button of user feedback pop up");
			click(JourneyPageAndroid.date_picker,"performing click on date");
			returnIndex("day");
			pickDate("09");
			//returnIndex("month");
			//pickDate("Jun");
			//returnIndex("year");
			//pickDate("2017");
			click(JourneyPageAndroid.But_Done,"performing click on done button");
			type(JourneyPageAndroid.Meetingname,"Test meet","entering meeting name");
			hideKeyBoard();
			type(JourneyPageAndroid.email,"knoferry@knoferry.com","entering email");
			hideKeyBoard();
			click(JourneyPageAndroid.Next,"performing click on next button");
			click(JourneyPageAndroid.btn_Done,"performing click on done button");
//			try
//			{
//			click(JourneyPageAndroid.txt_Star,"performing click on star");
//			}
//			catch(Exception e)
//			{
//				
//			}
			
			 tap();
			click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
			click(JourneyPageAndroid.settingsBtn, "click on settings Button");
			}
		catch(Exception e)
		{
			click(JourneyPageAndroid.preferencesIcon, "click on preferences Icon");
			click(JourneyPageAndroid.settingsBtn, "click on settings Button");
			System.out.println(e);
		}

		} 
		

	public void logout()
	{
		
		try {
			swipe("android:id/list","Log Out");
			click(JourneyPageAndroid.logOutJourney,"performing click on logout");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void languagechange_mobile(String languagechange, String defaultLanguage) throws Throwable {
		Waittime();
		waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		click(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
		click(JourneyPageAndroid.inputLanguage(languagechange), "click on Preffered languages");
		waitTillElementToBeClickble(JourneyPageAndroid.okBtn, "OK Button");
		click(JourneyPageAndroid.okBtn, "OK Button");
		Waittime();
		Waittime();
		}
	
		public void languagechangeforChina_mobile(String changedLanguage, String defaultLanguage) throws Throwable {
		Thread.sleep(4000);
		waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
		click(JourneyPageAndroid.inputLanguage(changedLanguage), "click on Preffered languages");
		waitTillElementToBeClickble(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		click(JourneyPageAndroid.inputLanguage(defaultLanguage), "click on Preffered languages");
		waitTillElementToBeClickble(JourneyPageAndroid.okBtn, "OK Button");
		click(JourneyPageAndroid.okBtn, "OK Button");
		Waittime();
		}

		//Journey-3 testcase
		
public void Launchstart() throws Throwable
		
		{
			try
			{
				
				click(JourneyPageAndroid.LaunchStart,"LaunchStart");
				click(JourneyPageAndroid.strengthsandweakness," click rating for strenthsandweekness");
				/*click(JourneyPageAndroid.btn_Ok,"performing click on OK button");
				click(JourneyPageAndroid.btn_Ok,"performing click on OK button");*/
				//click(JourneyPageAndroid.text_Unlock,"performing click on unlock text");
				
				//m_SwipeTop(Driver.findElement(JourneyPageAndroid.unlock));
			}
			catch(Exception e)
			{
				System.out.println(e);
			}

			} 
		
		
	}
		
		

		



