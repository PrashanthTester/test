package com.haygroup.android.page;

import org.openqa.selenium.By;

public class StylesClimatePageAndroid {
	
	public static By stylesAndClimateTab;
	public static By yourRangeofStylesText;
	public static By homePageGridView;
	public static String scanQRCodeText;
	public static By levelUpPageHideOne;
	public static By swipToScanQRCode;
	public static By levelUpPageHideTwo;
	public static By scanCodeOKButton;
	public static By clickEnterFeedBackManually;
	public static By YesButton;
	public static By uniqueCodeTextBox;
	public static By continueBtn;
	
	public static By enterQrcodeContinueBtn;
	public static By createPlanForMeBtn;
	public static By designMyPlanBtn;
	public static By designMyOwnPlanBtn;
	public static By visionaryStyleFirstCheckBox;
	public static By visionaryStyleSecondCheckBox;
	
	public static By levelUpPageHideThree;
	public static By levelUpPageHidefour;
	public static By levelUpPageHidefive;
	public static By vStyle;
	public static By aStyle;
	public static By cstyle;
	public static By pstyle;
	public static By participativestyle;
	public static By But_Continue;
	public static By myPlanText;
	public static By selectdate;
	public static By But_OK;
	
	public static By oneOnOneText;
	public static By teamText;
	public static By oneTimeText;
	public static By roadMapText;
	
	

	static
	{
		stylesAndClimateTab        = By.xpath("//android.widget.AbsListView/android.widget.LinearLayout[1]");
		yourRangeofStylesText      = By.id("com.haygroup.activate:id/yourRange_tv");
		homePageGridView           = By.id("com.haygroup.activate:id/grid_view");
		swipToScanQRCode           = By.id("com.haygroup.activate:id/btnScan");
		scanQRCodeText             = "Scan QR Code";
		levelUpPageHideOne         = By.id("com.haygroup.activate:id/llLevelUp");
		levelUpPageHideTwo         = By.className("android.widget.LinearLayout");
		levelUpPageHideThree	   = By.id("com.haygroup.activate:id/lvTasklist");
		levelUpPageHidefour		   = By.className("android.widget.LinearLayout");
		levelUpPageHidefive		   = By.className("android.widget.LinearLayout");
		scanCodeOKButton           = By.id("android:id/button1");
		clickEnterFeedBackManually = By.id("com.haygroup.activate:id/enterFeedbackManually");
		YesButton                  = By.id("android:id/button1");
		uniqueCodeTextBox          = By.id("com.haygroup.activate:id/etReason");
		 enterQrcodeContinueBtn      = By.id("com.haygroup.activate:id/btnConfirm");
		 continueBtn                = By.id("com.haygroup.activate:id/btnContinue");
		createPlanForMeBtn         = By.id("com.haygroup.activate:id/btnCreateMyPlan");
		
		
		
		designMyPlanBtn= By.id("com.haygroup.activate:id/designMyPlan");
		
		
		designMyOwnPlanBtn = By.id("com.haygroup.activate:id/btnDesignMyOwnPlan");
		vStyle=By.xpath("//android.widget.TextView[contains(@text,'Using the Visionary Style')]");
		aStyle=By.xpath("//android.widget.TextView[contains(@text,'Using the Affiliative Style')]");
		cstyle=By.xpath("//android.widget.TextView[contains(@text,'Using the Coaching Style')]");
		pstyle=By.xpath("//android.widget.TextView[contains(@text,'Using the Pacesetting Style')]");
		participativestyle=By.xpath("//android.widget.TextView[contains(@text,'Using the Participative Style')]");
		selectdate=By.xpath("//android:id/day_picker_view_pager/android:id/month_view[10]");
		But_Continue=By.id("com.haygroup.activate:id/btnContinue");
		myPlanText = By.xpath("//android.widget.TextView[contains(@text,'My Plan')]");
		But_OK=By.id("android:id/button1");
		
		oneOnOneText = By.xpath("//android.widget.TextView[@text='One-on-One (5/5)']");
		teamText = By.xpath("//android.widget.TextView[@text='Team (4/4)']");
		oneTimeText = By.xpath("//android.widget.TextView[@text='One-Time (0/0)']");
		roadMapText = By.xpath("//android.widget.TextView[@text='Road Map']");
		
		/*visionaryStyleFirstCheckBox = By.xpath("//android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.CheckBox");
		visionaryStyleSecondCheckBox = By.xpath("android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.CheckBox");
		*/
		
	}

}
