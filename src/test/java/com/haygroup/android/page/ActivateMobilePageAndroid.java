package com.haygroup.android.page;

import org.openqa.selenium.By;

public class ActivateMobilePageAndroid {

	//ActivateMobile
	
			//public static By confirmLogOut_mobile;
			public static By chinaCheckBox_mobile;
			public static By user_mobile;
			
			
			public static By stylesAndClimateTab;
			public static By yourRangeofStylesText;
			public static By homePageGridView;
			public static String scanQRCodeText;
			public static By levelUpPageHideOne;
			public static By swipToScanQRCode;
			public static By levelUpPageHideTwo;
			public static By scanCodeOKButton;
			public static By continueBtn;
			public static By createPlanForMeBtn;
			public static By designMyPlanBtn;
			public static By designMyOwnPlanBtn;
			public static By visionaryStyleFirstCheckBox;
			public static By visionaryStyleSecondCheckBox;
			public static By myPlanText;
			public static By oneOnOneText;
			public static By teamText;
			public static By oneTimeText;
			public static By roadMapText;
			
			
			static
			{
				//confirmLogOut_mobile = By.id("android:id/button1");
				chinaCheckBox_mobile = By.id("com.haygroup.activate:id/cbUseChinaServer");
				//user_mobile = By.name("User.Five@HayGroup.com");
				user_mobile = By.id("com.haygroup.activate:id/account_header_drawer_email");
				
				
				stylesAndClimateTab        = By.xpath("//android.widget.AbsListView/android.widget.LinearLayout[1]");
				yourRangeofStylesText      = By.id("com.haygroup.activate:id/yourRange_tv");
				homePageGridView           = By.id("com.haygroup.activate:id/grid_view");
				swipToScanQRCode           = By.id("com.haygroup.activate:id/btnScan");
				scanQRCodeText             = "Scan QR Code";
				levelUpPageHideOne         = By.id("com.haygroup.activate:id/llLevelUp");
				levelUpPageHideTwo         = By.className("android.widget.LinearLayout");
				scanCodeOKButton           = By.id("android:id/button1");
				continueBtn                = By.id("com.haygroup.activate:id/btnContinue");
				createPlanForMeBtn         = By.id("com.haygroup.activate:id/btnCreateMyPlan");
				designMyPlanBtn= By.id("com.haygroup.activate:id/designMyPlan");
				myPlanText = By.xpath("//android.widget.TextView[contains(@text,'My Plan')]");
				oneOnOneText = By.xpath("//android.widget.TextView[@text='One-on-One (5/5)']");
				teamText = By.xpath("//android.widget.TextView[@text='Team (4/4)']");
				oneTimeText = By.xpath("//android.widget.TextView[@text='One-Time (0/0)']");
				roadMapText = By.xpath("//android.widget.TextView[@text='Road Map']");
			}
			public static By inputUser(String user){

				return user_mobile = By.name(user);
			}
}
