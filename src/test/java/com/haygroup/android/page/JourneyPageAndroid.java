package com.haygroup.android.page;

import org.openqa.selenium.By;

public class JourneyPageAndroid {
	
	public static By preferencesIcon;
	public static By settingsBtn;
	public static By selectMyTitle;
	public static By myTitleTextBox;
	public static By okBtn;
	public static By currentTitle;
	public static By getStrartedBtn;
	public static By logOutJourney;
	public static By selectPreferedLanguage;
	public static By languageIcon;
	public static By languageOkBtn;
	public static By journeyTextInChina;
	public static By unlock;
	public static By btn_getStarted;
	public static By img_up;
	public static By text_Unlock;
	public static By text_UnlockAll; 
	public static By text_ResetUserdata; 
	public static By btn_Ok;
	public static By swipe_locator;
	public static By text_friendly;
	public static By date_picker;
	public static By But_Done;
	public static By Meetingname;
	public static By email;
	public static By Next;
	public static By btn_Done;
	public  static By txt_Star;
	
	//Journey-03
	public static By LaunchStart;
	public static By strengthsandweakness;
	public static By perceivedbyothers;
	public static By impactyourperformance;

	
	static{
		
			
		preferencesIcon = By.className("android.widget.ImageButton");
		settingsBtn = By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout");
		//selectMyTitle = By.name("My Title");
		myTitleTextBox = By.id("android:id/edit");
		okBtn = By.id("android:id/button1");
		getStrartedBtn = By.id("com.haygroup.journey.android:id/btnGetStarted");
		logOutJourney=By.xpath("//*[contains(@text,'Log Out')]");
		selectMyTitle = By.xpath("//android.widget.TextView[contains(@text,'My Title')]");
		currentTitle = By.xpath("//android.widget.TextView[@text ='My Title']/following-sibling::android.widget.TextView");
		selectPreferedLanguage = By.xpath("//android.widget.TextView[contains(@text,'User Preferred Language')]");
		languageOkBtn = By.id("android:id/button1");
		journeyTextInChina = By.id("android:id/action_bar_title");
		logOutJourney=By.xpath("//*[contains(@text,'Log Out')]");
		unlock=By.name("Unlock All Exercises");
		btn_getStarted=By.id("com.haygroup.journey.android:id/btnGetStarted");
		img_up=By.id("android:id/up");
		text_Unlock=By.xpath("//android.widget.TextView[@text='Unlock All Exercises']");
		text_ResetUserdata=By.xpath("//android.widget.TextView[@text='Reset User Data']");
		btn_Ok=By.id("android:id/button1");
		text_friendly=By.xpath("//android.widget.TextView[@text='Friendly Observation']");
		date_picker=By.id("com.haygroup.journey.android:id/btnDate");
		But_Done=By.id("android:id/button1");
		Meetingname=By.id("com.haygroup.journey.android:id/txtInput");
		email=By.id("com.haygroup.journey.android:id/txtEmail");
		Next=By.id("com.haygroup.journey.android:id/btnNext");
		btn_Done=By.id("com.haygroup.journey.android:id/btnDone");
		txt_Star=By.id("com.haygroup.journey.android:id/lblPoints");
        LaunchStart=By.xpath("//android.widget.TextView[contains(@text,'Launch Start')]");
        strengthsandweakness=By.xpath("//android.widget.LinearLayout[2]/android.widget.LinearLayout//android.widget.ImageButton[3]");
        perceivedbyothers=By.xpath("//android.widget.LinearLayout[2]/android.widget.LinearLayout[1]//android.widget.ImageButton[3]");
        impactyourperformance=By.xpath("//android.widget.LinearLayout[1]/android.widget.ImageButton[@index='3']");
        
		
		
		
		
					
	}
	
	public static By inputLanguage(String language){
		return languageIcon = By.xpath("//android.widget.TextView[@text='"+language+"']");
			
	}

}
