package com.haygroup.android.page;

import org.openqa.selenium.By;

public class EcoSystemPageAndroid {

	public static By mobileSettingsButton;
	public static By backButton;
	public static By overLayMenu;
	public static By Homelocation;
	public static By Editlocation;
	public static By selectPreferedLanguage;
	public static By selectPreferedCurrency;
	public static By okBtn;
	public static By selectPreferredTimezone;
	public static By ActionPlanremindersByEmailnotifications;
	public static By MyPlanPreferenceforWeekStartingDay;
	public static By MyplanpreferenceforWeekEndingDay;
	public static By MyplanpreferencesforteammeetingDay;
	public static By Teammeetingtime;
	public static By Logout;
	public static By popupLogoutbtn;
	public static By clickmylocation;
	public static By sendmenotifications;
	public static By Preferenceweekstartingday;
	public static By PreferenceweekEndingday;
	public static By Preferenceforteammeetingday;
	public static By PreferenceTeammeetingtime;	
	public static By btn_ok;
	
	public static By userprefferedcurrencylist;
	public static By userPreferredtimezonelist;
	public static By userpreferencetoreceiveemailnotifications;
	public static By sendmenotificationsaswhenrequired;
	public static By WeekEndingDay_okbtn;
	public static By Home_image;
	public static By currentLocation;
	
	public static By selectCurrency;
	public static By selectTimezone;
	public static By selectemailnotifications;
	public static By selectActionPlanreminders;
	public static By selectWeekStaringDay;
	public static By selectWeekEndingDay;
	
	public static By contactadmin;
	public static By adminselect;
	public static By nextbutton;
	public static By entertext;
	public static By sendbutton;
	public static By touch;
	public static By logout;
	public static By logoutbutton;
	public static By languageIcon;
	public static By homeText;
	
	


	static {

		mobileSettingsButton=By.xpath("//android.widget.RelativeLayout[6]/android.widget.LinearLayout/android.widget.TextView");
		backButton = By.xpath("//android.widget.ImageButton[contains(@content-desc,'Navigate up')]");
		overLayMenu=By.className("android.widget.ImageButton");
		Home_image=By.className("android.widget.ImageView");
		selectPreferedLanguage = By.name("User Preferred Language");
		okBtn = By.id("android:id/button1");
		//Homelocation=By.className("//android.widget.TextView[contains(@resource-id,'android:id/summary')]");
		Homelocation = By.xpath("//android.widget.RelativeLayout[1]/android.widget.TextView");
		
		//Homelocation=By.xpath("//android.widget.TextView[contains(@text,'Home')]");
		Editlocation=By.className("android.widget.EditText");
		selectPreferedCurrency=By.xpath("//android.widget.TextView[contains(@text,'User Preferred Currency')]");
		userprefferedcurrencylist=By.xpath("//android.widget.ListView/android.widget.TextView[5]");
		selectPreferredTimezone=By.xpath("//android.widget.TextView[contains(@text,'User Preferred Time Zone')]");
		userPreferredtimezonelist=By.xpath("//android.widget.ListView/android.widget.TextView[2]");
		userpreferencetoreceiveemailnotifications=By.xpath("//android.widget.TextView[contains(@text,'User preference to receive email notifications')]");
		sendmenotificationsaswhenrequired=By.xpath("//android.widget.TextView[contains(@text,'Send me notifications as and when required')]");
		ActionPlanremindersByEmailnotifications =By.xpath("//android.widget.TextView[contains(@text,'User Preference for Action Plan Reminders by Email Notifications')]");
		//ActionPlanremindersByEmailnotifications  =By.id("android:id/title");
		sendmenotifications=By.xpath("//android.widget.TextView[contains(@text,'Send me notifications on a weekly basis')]");
		MyPlanPreferenceforWeekStartingDay=By.xpath("//android.widget.TextView[contains(@text,'My Plan Preference for Week Starting Day')]");
		
		Preferenceweekstartingday=By.xpath("//android.widget.TextView[contains(@text,'Monday')]");
		
		MyplanpreferenceforWeekEndingDay=By.xpath("//android.widget.TextView[contains(@text,'My Plan Preference for Week Ending Day')]");
		
		PreferenceweekEndingday=By.xpath("//android.widget.TextView[contains(@text,'Thursday')]");
		WeekEndingDay_okbtn=By.xpath("//android.widget.Button[contains(@resource-id,'com.haygroup.activate:id/btnOk')]");
		
		MyplanpreferencesforteammeetingDay=By.xpath("//android.widget.TextView[contains(@text,'My Plan Preference for Team Meeting Day')]");
		Preferenceforteammeetingday=By.xpath("//android.widget.TextView[contains(@text,'Thursday')]");
		Teammeetingtime=By.xpath("//android.widget.TextView[contains(@text,'Team Meeting Time')]");
		PreferenceTeammeetingtime=By.id("android:id/button1");
		btn_ok=By.id("com.haygroup.activate:id/btnOk");
				
		clickmylocation=By.xpath("//android.widget.ListView/android.widget.LinearLayout/android.widget.TextView");
		Logout=By.xpath("//android.widget.TextView[contains(@text,'Log Out')]");
		popupLogoutbtn=By.xpath("//android.widget.Button[contains(@text,'Log Out')]");
		currentLocation = By.xpath("//android.widget.ListView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView");

		
		contactadmin=By.xpath("//android.widget.LinearLayout[7]/android.widget.RelativeLayout/android.widget.TextView");
		adminselect=By.xpath("//android.widget.CheckedTextView[contains(@text,'Support')]");
		nextbutton=By.xpath("//android.widget.Button[contains(@text,'NEXT')]");
		entertext=By.id("com.haygroup.activate:id/adminMessage");
		sendbutton=By.xpath("//android.widget.Button[contains(@text,'SEND')]");
		touch=By.id("com.haygroup.activate:id/rowIcon");
		logout=By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[contains(@text,'Log Out')]");
		logoutbutton=By.xpath("//android.widget.Button[contains(@text,'LOG OUT')]");
		homeText = By.xpath("//android.widget.TextView[1]");
	}


	public static By inputCurrency(String currency){
		return selectCurrency = By.xpath("//android.widget.TextView[@text='"+currency+"']");
		}
	public static By inputTimezone(String timezone){
		return selectTimezone = By.xpath("//android.widget.TextView[@text='"+timezone+"']");
		}
	public static By inputemailnotifications (String emailnotifications)
	{
		return selectemailnotifications=By.xpath("//android.widget.TextView[@text='"+emailnotifications+"']");
		
	}
	
	public static By inputActionPlanreminder (String emailnotifications)
	{
		return selectActionPlanreminders=By.xpath("//android.widget.TextView[@text='"+emailnotifications+"']");
		
	}
	public static By inputWeekStaringDay (String WeekStaringDay)
	{
		return selectWeekStaringDay=By.xpath("//android.widget.TextView[@text='"+WeekStaringDay+"']");
		
	}
	public static By inputWeekEndingDay (String WeekEndingDay)
	{
		return selectWeekEndingDay=By.xpath("//android.widget.TextView[@text='"+WeekEndingDay+"']");
		
	}

	public static By inputTeammeeting (String Teammeeting)
	{
		return selectWeekEndingDay=By.xpath("//android.widget.TextView[@text='"+Teammeeting+"']");
		
	}
	
	public static By inputLanguage(String languagechange){

		return languageIcon = By.xpath("//android.widget.TextView[@text='"+languagechange+"']");
	}
	
}
