package com.haygroup.android.page;

import org.openqa.selenium.By;

public class LoginPageAndroid {


	public static By userName_mobile;
	public static By password_mobile;
	public static By loginBtn_mobile;
	
	public static By JM_username;
	public static By JM_password;
	public static By JM_LoginBut;   
	
	//Journey
	public static By journeyUN;
	public static By journeyPW;
	
		public static By userNameJourney_mobile;
		public static By passwordJourney_mobile;
		public static By journeySignInBtn;
		
	//Styles&Climate
		public static By confirmLogOut_mobile;
		public static By chinaServerCheckBox;
	
	
	
	public static By logOut_mobile;

	/* Page Objects Of Login Page */
	static {
		userName_mobile = By.id("com.haygroup.activate:id/txtUsername");
		password_mobile 	= By.id("com.haygroup.activate:id/txtPassword");
		loginBtn_mobile 	= By.id("com.haygroup.activate:id/btnSignIn");
		JM_username = By.xpath("//input[1]");
		JM_password = By.xpath("//input[2]");
		JM_LoginBut=By.xpath("//input[3]");
		userNameJourney_mobile = By.id("com.haygroup.journey.android:id/txtUsername");
		passwordJourney_mobile = By.id("com.haygroup.journey.android:id/txtPassword");
		journeySignInBtn = By.id("com.haygroup.journey.android:id/btnSignIn");
		confirmLogOut_mobile = By.id("android:id/button1");
		chinaServerCheckBox=By.id("com.haygroup.journey.android:id/cbUseChinaServer");
	}
}