package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.EcoSystemAndroidLib;
import com.haygroup.android.page.EcoSystemPageAndroid;
import com.haygroup.web.page.EcoSystemPage;

public class TC_Android_EcoSystem_ECO_001 extends EcoSystemAndroidLib {

	@DataProvider
	public Object[][] getTestDataFor_settingspage() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_settingspage")
	public void mobileSettingsPage002Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.settingspage");	
				//launchApplication("Dashboard",data.get("AppURL"));
				/* Login to the application */
				loginToMobileApplication(data.get("UserNameMobile"), data.get("PasswordMobile"));
				
				clearhomepage();

				/*Open OverLay Menu*/
				openOverLayMenu();

				mobileSettingsPageButton();
				swipe("android:id/list", "User Preferred Language");

				/*click on settings*/
				languagechange_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());

				loginToMobileApplication(data.get("UserNameMobile"), data.get("PasswordMobile"));
			
				assertTextMatching(EcoSystemPageAndroid.homeText, data.get("HomePage").trim(), "language");
				
				
				openOverLayMenu();

				mobileSettingsPageButton();
				//swipe("android:id/list", "User Preferred Language");
				languagechangeforChina_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());
				
				
				
				
				//assertTextMatching(EcoSystemPageAndroid.inputLanguage(data.get("LanguageMobile")), data.get("LanguageMobile"), "Language Verification");
				
				
				/*navigateBack();
				
				openOverLayMenu();*/
				//Need to Update Preferred language locator with xpath
				/*mobileSettingsPageButton();
				languagechange_mobile(data.get("DefaultLanguage"));*/
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		//logOutMobileApplication();

		//captureBrowserConsoleLogs(Driver);
	}
}


