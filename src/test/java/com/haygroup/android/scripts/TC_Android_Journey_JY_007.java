package com.haygroup.android.scripts;

import java.util.Hashtable;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.JourneyAndroidLib;
import com.haygroup.android.page.JourneyPageAndroid;


public class TC_Android_Journey_JY_007 extends JourneyAndroidLib{
	
	@DataProvider
	public Object[][] getTestDataFor_journeypreferences() {
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}
	
	@Test(dataProvider = "getTestDataFor_journeypreferences")
	public void JourneyPreferences007Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.JourneyPreferences");	
				loginToJourneyMobileApplication(data.get("UserName").trim(), data.get("Password").trim());
				clickGetStartedButton();
				selectMyTitle(data.get("MyTitle").trim());			
				assertTextMatching(JourneyPageAndroid.currentTitle,data.get("MyTitle").trim(), "title");
			
				languagechange_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());
				loginToJourneyMobileApplication(data.get("UserName").trim(), data.get("Password").trim());
				assertTextMatching(JourneyPageAndroid.journeyTextInChina,data.get("JourneyTitle").trim(), "Language Verification");
				clickSettingsButton();
				languagechangeforChina_mobile(data.get("LanguageMobile").trim(),data.get("DefaultLanguage").trim());
			}
		}					
			catch (Exception e) {
					e.printStackTrace();
					//throw new RuntimeException(e);
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@AfterMethod
			public void logoutApplication() throws Throwable {
				//Logout from the application 
				//logOutMobileApplication();

				//captureBrowserConsoleLogs(Driver);
			}
		}
		


