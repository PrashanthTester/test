package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.EcoSystemAndroidLib;


public class TC_Android_EcoSystem_ECO_002 extends EcoSystemAndroidLib {

	@DataProvider
	public Object[][] getTestDataFor_settingspage() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_settingspage")
	public void EcoSystemlocationchange(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.settingspage");	
				//launchApplication("Dashboard",data.get("AppURL"));
				/* Login to the application */
				loginToMobileApplication(data.get("UserNameMobile"), data.get("PasswordMobile"));
				isApplicationReady();
				Waittime();
				
				/*Open OverLay Menu*/
				openOverLayMenu();

				/*click on settings*/
				mobileSettingsPageButton();
				homeLocation();
				//changeLocation("Hyderabad");
				changeLocation(data.get("Location"));
				//assertTextMatching(EcoSystemPageAndroid.currentLocation, "Bangalore", "Location changed");
				
				swipe("android:id/list","User Preferred Currency");
				userPreferredCurrency(data.get("Currency").trim());
				//userPreferredCurrency(data.get("Currency"));
				
				userPreferredTimeZone(data.get("Timezone").trim());
				//userPreferredTimeZone(data.get("Timezone"));
				userpreferencetoreceiveemailnotifications(data.get("Emailnotification").trim());
				
				//swipe("android:id/list","User Preference for Action Plan Reminders by Email Notifications");
				userpreferenceforActionPlanreminderEmailNotifications(data.get("Actionreminernoficiation").trim());
				MyplanpreferenceforWeekEndingDay(data.get("WeekStartingDay").trim());
				swipe("android:id/list","Team Meeting Time");
				MyplanpreferenceforWeekEndingDay(data.get("WeekEndingDay").trim());
				MyplanpreferencesforteammeetingDay(data.get("MeetingDay").trim());
				Teammeetingtime();
				Logout();		
				
				
				
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutApplication() throws Throwable {
		//Logout from the application 
		//logOutMobileApplication();

		//captureBrowserConsoleLogs(Driver);
	}
}


