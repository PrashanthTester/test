package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.JourneyAndroidLib;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.libs.ActivateMobileAndroidLib;

public class TC_Android_Journey_JY_008 extends JourneyAndroidLib{
	
	
	@DataProvider
	public Object[][] getTestDataFor_Journey() {
		
		return TestUtil.getData("Create_Journey", TestData, "Journey");
	}
	
	@Test(dataProvider = "getTestDataFor_Journey")
	public void logintoChinaServer(Hashtable<String, String> data){
	try {
			if (data.get("RunMode").equals("Y")) {
			this.reporter.initTestCaseDescription("TC.Journey");	
			
			/* Login to the application */
			loginToJourneyMobileChinaServer(data.get("UserName"), data.get("Password"));
			
			clickGetStartedButton();
			
			swipe("android:id/list","Log Out");

        }
			
		}
	
		catch (Exception e) {
		e.printStackTrace();
		//throw new RuntimeException(e);
		} catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
    
		
}
	@AfterMethod
	public void  logOutFunc()throws Throwable {
		
		//Logout from the application
		logOutJourney();
		
		}
	
	

}
