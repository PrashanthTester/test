package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.StylesClimateAndroidLib;
import com.haygroup.android.page.StylesClimatePageAndroid;
import com.haygroup.web.page.EcoSystemPage;

public class TC_Android_StylesClimate_002 extends StylesClimateAndroidLib{

	@DataProvider
	public Object[][] getTestDataFor_StylesAndClimate() {
		return TestUtil.getData("Create_Styles_And_Climate", TestData, "StylesAndClimate");
	}

	@Test(dataProvider = "getTestDataFor_StylesAndClimate")
	public void StylesClimateCreation(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.settingspage");	
	
				//Login to the application
				loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());
				
				//Remove hiding screen
				clearScreenHideOnHomePage();
				
				//Click on Styles&Climate Tab
				openStylesAndClimate();

				//Remove hiding screen
				removeScreenHider(StylesClimatePageAndroid.levelUpPageHideOne);
				
				//Verify user is on LevelUp Screen 
				//verifyLevelUpPage();
				
				//Click on Scan QR code button
				//scrollDownToElement("com.haygroup.activate:id/scrollView", "OR CREATE AN ACTION PLAN");
				scrollDownToElement("com.haygroup.activate:id/scrollView", "Scan QR Code");
				
				//Remove hiding screen
				removeScreenHider(StylesClimatePageAndroid.levelUpPageHideTwo);
				
				//Click Scan QR Code
				clickScanQRCode();
				
				clickEnterFeedBackManuallyButton();
				
				enterUinqueCode("QSCT4C");
				
				scrollDownToElement("com.haygroup.activate:id/scrollView","Continue");
				
				clickContinueButton();
				
				//removeScreenHider(StylesClimatePageAndroid.levelUpPageHideThree);
				
				clickDesignMyPlanButton();
				//clickCreatePlanForMe();
				clickDesignMyOwnPlanButton();
				
				selectFiveTips();
				
				selectfourtips();
				
				selectdate();
				
				Myplanpage();
				
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutFromApplication() throws Throwable {
		//Logout from the application 
		openOverLayMenu();
		logOutMobileApplication();
	}
}
