package com.haygroup.android.scripts;

import java.util.Hashtable;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.android.page.StylesClimatePageAndroid;
import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.ActivateMobileAndroidLib;

public class TC_Android_ActivateMobile_001 extends ActivateMobileAndroidLib{

	@DataProvider
	public Object[][] getTestDataFor_StylesAndClimate() {
		return TestUtil.getData("Create_Styles_And_Climate", TestData, "StylesAndClimate");
	}

	@Test(dataProvider = "getTestDataFor_StylesAndClimate")
	public void activateMobile001Test(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.settingspage");	
	
				//Login to the application
				//loginToMobileApplication(data.get("UserNameMobile").trim(), data.get("PasswordMobile").trim());
			    loginToMobileApplication("user.auto1@haygroup.com", "Activate123");
				
				//Remove hiding screen
				clearScreenHideOnHomePage();
			 			
				//Click on Styles&Climate Tab
				openStylesAndClimate();

				//Remove hiding screen
				removeScreenHider(ActivateMobilePageAndroid.levelUpPageHideOne);
							
			    //Verify user is on LevelUp Screen 	
				assertTextMatching(ActivateMobilePageAndroid.yourRangeofStylesText, "Your Range of Styles", "Verify user is on LevelUp Screen");
				
				//Click on Scan QR code button
				scrollDownToElement("com.haygroup.activate:id/scrollView", "Scan QR Code");
				
				//Remove hiding screen
				removeScreenHider(ActivateMobilePageAndroid.levelUpPageHideTwo);
					
				
				//Click Scan QR Code
				clickScanQRCode();	
				openQRcode();
				Thread.sleep(5000);
				quitQRcode();
				
				scrollDownToElement("com.haygroup.activate:id/scrollView","Continue");
				
				clickContinueButton();
				assertTextMatching(ActivateMobilePageAndroid.roadMapText, "Road Map", "user is taken to road map Screen");			
				clickDesignMyPlanButton();
				clickCreatePlanForMe();
				removeScreenHider(StylesClimatePageAndroid.levelUpPageHidefour);
				assertTextMatching(StylesClimatePageAndroid.oneOnOneText, "One-on-One (5/5)", "user taken to styles-climate design page - ONE ON ONE section");
				clickOneOneContinueButton();
				assertTextMatching(StylesClimatePageAndroid.teamText, "Team (4/4)", "user is taken to TEAM section");
				clickContinueButton();
				assertTextMatching(StylesClimatePageAndroid.oneTimeText,"One-Time (0/0)", "user is taken to ONE-TIME section");
				clickContinueButton();
				assertTextMatching(StylesClimatePageAndroid.myPlanText, "My Plan", "My Plan text is verified");					
				 Myplanpage();
						
			}									

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutFromApplication() throws Throwable {
		//Logout from the application 
		openOverLayMenu();
		logOutMobileApplication();
	}
}
