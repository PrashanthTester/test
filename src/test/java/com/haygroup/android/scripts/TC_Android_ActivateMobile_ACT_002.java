package com.haygroup.android.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.android.libs.ActivateMobileAndroidLib;
import com.haygroup.android.libs.EcoSystemAndroidLib;
import com.haygroup.android.page.ActivateMobilePageAndroid;
import com.haygroup.web.page.EcoSystemPage;

public class TC_Android_ActivateMobile_ACT_002 extends ActivateMobileAndroidLib
{
	@DataProvider
	public Object[][] getTestDataFor_ActivateMobile() {
		return TestUtil.getData("ActivateMobile", TestData, "ActivateMobile");
	}
	
	

    @Test(dataProvider = "getTestDataFor_ActivateMobile")
	public void logintoChinaServer(Hashtable<String, String> data){
	try {
			if (data.get("RunMode").equals("Y")) {
			this.reporter.initTestCaseDescription("TC.ActivateMobile");	
			
			/* Login to the application */
			loginToMobileChinaServer(data.get("UserNameMobile"), data.get("PasswordMobile"));
			
			openOverLayMenu();
			Waittime();
			assertTextMatching(ActivateMobilePageAndroid.user_mobile, data.get("VerifyUser").trim(), "User name verification");
			
			}
			
		}
	
		catch (Exception e) {
		e.printStackTrace();
		//throw new RuntimeException(e);
		} catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
    
		
}
	@AfterMethod
	public void logOutApplication() throws Throwable {
		
		//Logout from the application 
		logOutMobileApplication();

		
	}
	
	

	
}
