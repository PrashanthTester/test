package com.haygroup.web.libs;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.JobMapping;

public class JobMappingLib extends HayGroupCommonLib {
	
	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	
	
	/*uploadFile file will open the pop up  to provide the path*/
	
	public void uploadFile() throws Throwable
	{
		try{

		JSClick(JobMapping.Upload, "Performing click on upload button");
		scrollToView(JobMapping.UploadFile);
		//String val=System.getProperty("user.dir")+"\\TestData\\Job_Mapping_BensTestData_6functions.xlsx";
		//type(JobMapping.UploadFile,val,"sending path");
		//JSClick(JobMapping.UploadFile, "uploading file ");
		mouseover(JobMapping.UploadFile, "");
		mouseClick(JobMapping.UploadFile, "");
		//Thread.sleep(8000);
		Waittime();
		sendPath();
		Waittime();
		Waittime();
		scrollToView(JobMapping.NextButton);
		JSClick(JobMapping.NextButton, "performing click on next button ");
		System.out.println("in the end");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		
	}
	
	
	/* sendPath method  will accept the path of  the data sheet to be uplaoded */
public void sendPath() throws Throwable {
		
		try{
		Thread.sleep(10000);
		StringSelection sel=new StringSelection(System.getProperty("user.dir")+"\\TestData\\Job_Mapping_BensTestData_6functions.xlsx");
		System.out.println("the result is"+sel.toString());
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel, null);
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
		r.keyRelease(KeyEvent.VK_V);
		Thread.sleep(10000);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	
/*  MapFunctions will perform click  to open map function screen*/
	public void MapFunctions()
	{
		checkPageIsReady();
		try {
			JSClick(JobMapping.Mapfunctions,"performing click on Mapfunctions");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*clickManageFunctions will  map the jobs*/ 
	public void clickManageFunctions() throws Throwable
	{

		try{
		
			 List<WebElement> ele=Driver.findElements(JobMapping.maplist);
		 System.out.println(ele.size());
		 for(int i=0;i<ele.size();i++)
		 {
			 ele.get(i).click();
			 
			 Driver.findElement(By.xpath("//span[contains(.,'Compile analysis of losses and reserves, evaluates product line performance, calculating financial projections and recommends financial structure "
			 		+ "policies.')]")).click();
		 }
		 JSClick(JobMapping.SaveChanges, "performing click on save changes");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	
	/* DefineIndustry will map the industry to be defined  */
	public void DefineIndustry() throws Throwable
	{
		try
		{
		isApplicationReady();
		checkPageIsReady();
		JSClick(JobMapping.DefineIndustry,"performing click Define industries");
		JSClick(JobMapping.ManageSearch,"performing click on industry search");
		JSClick(JobMapping.SelectSearch,"performing click on select search industry");
		JSClick(JobMapping.NextButton, "performing click on next button ");
		JSClick(JobMapping.Guideme, "performing click on Guide me button");
		JSClick(JobMapping.Regionalsales, "Performing click on Regional sales");
		JSClick(JobMapping.Continue, "Performing click on continue button");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	
	/*selectJob  method will   define the jobs related to the industry defined */
	public void selectJob() throws Throwable
	{
		try
		{
		isApplicationReady();
		checkPageIsReady();
		Waittime();
		dragAndDrop(JobMapping.jobManager1,JobMapping.destJobManager1 , "dragging job manager");
		JSClick(JobMapping.typeCareer, "Performing click on type of career button");
		JSClick(JobMapping.optCareer, "Performing click on type of career option");
		System.out.println("in drag n drop");
		//Thread.sleep(9000);
		Waittime();
		dragAndDrop(JobMapping.RegionalsalesManager2,JobMapping.destRegionalJobManager2 , "dragging job manager");
		JSClick(JobMapping.ContinueButton, "Performing click on continue button");
		JSClick(JobMapping.NextButton_Tooltip, "Performing click on next button of tool tip");
		JSClick(JobMapping.NextButton5_Tooltip, "Performing click on final next button of tool tip");
		JSClick(JobMapping.Careerpath_Radio, "Performing click on final next button of tool tip");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	
	/*selectJobbank is similar to select job */
	
	public void selectJobbank() throws Throwable
	{
		try
		{
		isApplicationReady();
		checkPageIsReady();
		Waittime();
		dragAndDrop(JobMapping.HeadAcc1,JobMapping.dest_HeadAcc1 , "dragging job manager");
		JSClick(JobMapping.typeCareer, "Performing click on type of career button");
		JSClick(JobMapping.optCareer, "Performing click on type of career option");
		Waittime();
		dragAndDrop(JobMapping.coordinator,JobMapping.dest_keyAccadmin2 , "dragging job manager");
		JSClick(JobMapping.ContinueButton, "Performing click on continue button");
		JSClick(JobMapping.NextButton_Tooltip, "Performing click on next button of tool tip");
		JSClick(JobMapping.NextButton5_Tooltip, "Performing click on final next button of tool tip");
		JSClick(JobMapping.Careerpath_Radio, "Performing click on final next button of tool tip");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	
	
	/* selectJobbankkeyacc  will conclude the job key accounts */
	public void selectJobbankkeyacc() throws Throwable
	{
		try
		{
		isApplicationReady();
		checkPageIsReady();
		dragAndDrop(JobMapping.keyAccountAdmin1,JobMapping.dest_keyAccountAdmin1 , "dragging job manager");
		JSClick(JobMapping.typeCareer, "Performing click on type of career button");
		JSClick(JobMapping.optCareer, "Performing click on type of career option");
		Waittime();
		dragAndDrop(JobMapping.ManagerSales2,JobMapping.dest_Managersales2 , "dragging job manager");
		JSClick(JobMapping.ContinueButton, "Performing click on continue button");
		JSClick(JobMapping.NextButton_Tooltip, "Performing click on next button of tool tip");
		JSClick(JobMapping.NextButton5_Tooltip, "Performing click on final next button of tool tip");
		JSClick(JobMapping.PeerTestContinue, "Performing click on final next button of tool tip");
		DynamicWait(JobMapping.Yes_Radio);
		elementVisibleTime(JobMapping.Yes_Radio);
		Waittime();
		click(JobMapping.Yes_Radio, "Performing click on final next button of tool tip");
		DynamicWait(JobMapping.Peers_Next);
		elementVisibleTime(JobMapping.Peers_Next);
		Waittime();
		JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
		DynamicWait(JobMapping.Yes_Radio);
		elementVisibleTime(JobMapping.Yes_Radio);
		Waittime();
		click(JobMapping.Yes_Radio, "Performing click on final next button of tool tip");
		DynamicWait(JobMapping.Peers_Next);
		elementVisibleTime(JobMapping.Peers_Next);
		Waittime();
		JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
		DynamicWait(JobMapping.Yes_Radio);
		elementVisibleTime(JobMapping.Yes_Radio);
		Waittime();
		click(JobMapping.Yes_Radio, "Performing click on final next button of tool tip");
		DynamicWait(JobMapping.Peers_Next);
		elementVisibleTime(JobMapping.Peers_Next);
		Waittime();
		JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
		DynamicWait(JobMapping.Yes_Radio);
		elementVisibleTime(JobMapping.Yes_Radio);
		Waittime();
		click(JobMapping.Yes_Radio, "Performing click on final next button of tool tip");
		DynamicWait(JobMapping.Peers_Next);
		elementVisibleTime(JobMapping.Peers_Next);
		Waittime();
		JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
		DynamicWait(JobMapping.Yes_Radio);
		elementVisibleTime(JobMapping.Yes_Radio);
		Waittime();
		click(JobMapping.Yes_Radio, "Performing click on final next button of tool tip");
		DynamicWait(JobMapping.Peers_Next);
		elementVisibleTime(JobMapping.Peers_Next);
		Waittime();
		JSClick(JobMapping.Peers_Next, "Performing click on peers next ");
		DynamicWait(JobMapping.But_Illtake);
		elementVisibleTime(JobMapping.But_Illtake);
		Waittime();
		JSClick(JobMapping.But_Illtake, "Performing click on i ill take here button ");
		JSClick(JobMapping.Tick1, "Performing click on first tick button ");
		JSClick(JobMapping.Tick2, "Performing click on second tick button ");
		Waittime();
		dragAndDrop(JobMapping.AccManager,JobMapping.destAccManager , "dragging acc manager");
		JSClick(JobMapping.AcctList, "Performing click on type of career button");
		JSClick(JobMapping.optAccList, "Performing click on type of career option");
		JSClick(JobMapping.ArrowCareerpath, "Performing click on Arrow career path");
		JSClick(JobMapping.Addcareerpath, "Performing click on add career path");
		//JSClick(JobMapping.Deletecareer, "Performing click on Arrow career path");
		//JSClick(JobMapping.Deltecareeroption, "Performing click on Arrow career path");
		JSClick(JobMapping.Button_HayGrades, "Performing click onHaygrads button");
		Waittime();
		JSClick(JobMapping.But_Haycontinue, "Performing click continue  button");
		Waittime();
		JSClick(JobMapping.But_Haycontinue, "Performing click continue  button");
		Waittime();
		click(JobMapping.Radio_AnchorJob, "Performing click on anchorjob radio");
		Waittime();
		JSClick(JobMapping.Next_Anchor, "Performing click continue  button");
		Waittime();
		click(JobMapping.Radio_AnchorJob, "Performing click on anchorjob radio");
		Waittime();
		JSClick(JobMapping.Next_Anchor, "Performing click continue  button");
		Waittime();
		click(JobMapping.Radio_AnchorJob, "Performing click on anchorjob radio");
		Waittime();
		JSClick(JobMapping.Next_Anchor, "Performing click continue  button");
		Waittime();
		JSClick(JobMapping.Button_OK_Anchor, "Performing click continue  button");
		Waittime();
		JSClick(JobMapping.Button_OK_Anchor, "Performing click continue  button");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
	
	

}
