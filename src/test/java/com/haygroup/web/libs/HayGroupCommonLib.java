package com.haygroup.web.libs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.StaleElementReferenceException;

import com.gallop.accelerators.ActionEngine;
import com.gallop.support.CustomException;
import com.gallop.support.IFrameworkConstant;
import com.haygroup.web.page.EcoSystemPage;
import com.haygroup.web.page.HomePage;
import com.haygroup.web.page.LoginPage;


public class HayGroupCommonLib extends ActionEngine{

	public void launchApplication(String SheetName, String AppURL){

		try {
			Driver.get(AppURL);
			isApplicationReady();
			Driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public void loginToApplication(String UserName, String Password) throws Throwable {	
		
		try {
			
			isApplicationReady();
			checkPageIsReady();
			try {
				waitTillElementToBeClickble(LoginPage.userName, "entering login username");
				type(LoginPage.userName, UserName, "entering login username");
				type(LoginPage.password, Password, "entering login password");
				JSClick(LoginPage.loginBtn, "Login button");
			} catch (Exception e) {
				// TODO: handle exception
				throw new CustomException("Error in finding the user name locator");
			}
			
			
		} catch (StaleElementReferenceException e) {
			// TODO: handle exception
		}
		
	}
	
	public void loginToHaygroupApplication(String UserName, String Password) throws Throwable { 
		String browserName = getBrowser();
		if(browserName.equalsIgnoreCase("chrome")){
		Waittime();
		Thread.sleep(15000);
		waitForVisibilityOfElement(LoginPage.newusername, "entering login username",180);
		type(LoginPage.newusername, UserName, "entering login username");
		type(LoginPage.newpassword, Password, "entering login password");
		click(LoginPage.newloginBtn, "Login button");
		}
		else if(browserName.equalsIgnoreCase("firefox")){
		Waittime();
		Thread.sleep(15000);
		waitForVisibilityOfElement(LoginPage.newusername, "entering login username",180);
		type(LoginPage.newusername, UserName, "entering login username");
		type(LoginPage.newpassword, Password, "entering login password");
		click(LoginPage.newloginBtn, "Login button");
		}
		else if(browserName.equalsIgnoreCase("ie")){
		Waittime();
		Thread.sleep(100000);
		waitForVisibilityOfElement(LoginPage.newusername, "entering login username",180);
		type(LoginPage.newusername, UserName, "entering login username");
		type(LoginPage.newpassword, Password, "entering login password");
		click(LoginPage.newloginBtn, "Login button");
		}
		}


	/*	Login to Jobmappingapplication with User Name and Password*/
	public void loginToJMApplication(String UserName, String Password) throws Throwable {	
		waitForVisibilityOfElement(LoginPage.JM_username, "entering login username",60);
		type(LoginPage.JM_username, UserName, "entering login username");
		type(LoginPage.JM_password, Password, "entering login password");
		JSClick(LoginPage.JM_LoginBut, "Login button");
	}

	public void loginToMobileApplication(String UserName, String Password) throws Throwable {	
		waitTillElementToBeClickble(LoginPage.userName_mobile, "entering login username");
		type(LoginPage.userName_mobile, UserName, "entering login username");
		//appiumDriver.hideKeyboard();
		type(LoginPage.password_mobile, Password, "login password");
		//appiumDriver.hideKeyboard();
		click(LoginPage.loginBtn_mobile, "Login button");
	}
	public void mobileSettingsPageButton() throws Throwable {
		try {
			//isApplicationReady();
			waitTillElementToBeClickble(EcoSystemPage.mobileSettingsButton, "Settings button");
			click(EcoSystemPage.mobileSettingsButton, "Settings button");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void navigateBack(){
		try {
			click(EcoSystemPage.backButton, "Back Button");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void openOverLayMenu(){
		try {
			waitTillElementToBeClickble(EcoSystemPage.overLayMenu,"click on menu");
			click(EcoSystemPage.overLayMenu,"click on menu");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/*Select Region*/
	public void selectRegion(String region)throws Throwable {	
		JSClick(HomePage.selectregion(region), "Select region");
	}

	/*Log out from application*/
	public void logOutFromApplication() throws Throwable{
		JSClick(HomePage.logOutButton, "Log out");
	}

	public void logOutMobileApplication() throws Throwable{
		waitTillElementToBeClickble(HomePage.logOut_mobile, "Log out Mobile");
		click(HomePage.logOut_mobile, "Log out Mobile");
	}

}
