package com.haygroup.web.libs;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;

import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.google.common.base.Function;
import com.haygroup.web.page.StylesClimatePage;

public class StylesClimateLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/* Click Enter Feedback Manually Button */
	public void clickEnterFeedBackManuallyButton() throws Throwable {
		click(StylesClimatePage.enterFeedbackmanually, "Enter Feedback Manually button");
	}

	/*To click on Overwrite popup*/
	public void clickOverWritePopup() throws Throwable {		
		click(StylesClimatePage.okButton, "Confirmation for Over writing the Previous road Map");
		isApplicationReady();
		/*click(StylesClimatePage.skipButton, "Skip Button of tool tip");*/
	}

	/* Enter Unique Code */
	public void enterUniqueCode(String uniqueCode) throws Throwable {
		type(StylesClimatePage.uniqueCodeField, uniqueCode, "Entering Unique Code");
		click(StylesClimatePage.continueButton, "Continue Button to Enter Feedback Manually");
		isApplicationReady();
	}

	/* Click Continue button On activate RoadMap */
	/*public void continueActivateRoadMap() throws Throwable {
		//WebElement element = Driver.findElement(By.xpath("//*[@id='my-plan-continue']"));
		//((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", element);
		click(StylesClimatePage.continueButton, "Continue Button to view activate Road Map");
	}
		
*/
	public void continueActivateRoadMap() throws Throwable {
		//WebElement element = Driver.findElement(By.xpath("//*[@id='my-plan-continue']"));
		//((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", StylesClimatePage.continueButton);
		scrollToView(StylesClimatePage.continueButton);
		JSClick(StylesClimatePage.continueButton, "Continue Button to view activate Road Map");
		}

	
	
	
	
	/* Design My Plan */
	public void designMyPlan() throws Throwable {
		isApplicationReady();
		click(StylesClimatePage.designMyPlan, "Design My Plan button");
	}
	
	/* Design My Own Plan */
	public void designMyOwnPlan() throws Throwable {
		isApplicationReady();
		click(StylesClimatePage.designMyOwnPlan, "Design MyOwn Plan button");
		isApplicationReady();
	}

	/* Create a plan for me */
	public void createPlanForMe() throws Throwable {
		isApplicationReady();
		click(StylesClimatePage.createPlanForMe, "Design MyOwn Plan button");
		isApplicationReady();
	}
	
	/* Select user required Style */
	public void selectStyle(String styleName) throws Throwable {
		selectStyleByText(styleName);
	}
	
	/* Select user required Style */
	public void selectTips(int tipsCount) throws Throwable {
		selectTipsByIndex(tipsCount);
	}

	/* Click Continue button */
	public void clickContinueButton() throws Throwable {
		//waitForInVisibilityOfElement(StylesClimatePage.continueDisabled, "Continue button Disabled");
		waitForVisibilityOfElement(StylesClimatePage.continueOneOnOne, "", 30);
		JSClick(StylesClimatePage.continueOneOnOne, "Continue Button");
		isApplicationReady();
	}

	/* Click on checkBox and complete review */
	public void reviewAndCompleteTasks() throws Throwable {
		waitForVisibilityOfElement(StylesClimatePage.checkBoxList, "Checkbox list", IFrameworkConstant.WAIT_TIME_MEDIUM);
		boolean value = false;
		while (!(value)) {
			value = selectcheckbox();
			if (!(value)) {
				selectNextWeek();
			}
		}
	}

	/* To Select required style */
	public void selectStyleByText(String value) {
		try {          
            String styleXPath = "//div[starts-with(text(),'" + value + "')]/ancestor::div[@class='tile-item-wrapper']/a/div[2]/div[2]";
            JSMouseHoverImitate(By.xpath(styleXPath),"Hover on"+value);
            WebDriverWait wait = new WebDriverWait(Driver, 90);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[starts-with(text(),'" + value + "')]/ancestor::div[@class='tile-item-wrapper']//div//span[@class='add-to-plan-text']")));
            JSClick(By.xpath("//div[starts-with(text(),'" + value + "')]/ancestor::div[@class='tile-item-wrapper']//div//span[@class='add-to-plan-text']"), "Add to plan");
      } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
      }
	}
	
	public void selectTipsByIndex(int tipsCount) {
		try {          
            for(int i=2;i<=tipsCount;i++){
            	JSMouseHoverImitate(By.xpath("//*[@id='tip-style-group-container']/li["+i+"]/div/a/div[2]/div[2]"),"Hover on Plan "+tipsCount);
            	WebDriverWait wait = new WebDriverWait(Driver, 90);
            	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//ul[@id='tip-style-group-container'])[1]/li["+i+"]/div/a/div[2]/div[2]/span[@class='add-to-plan-text']")));
            	JSClick(By.xpath("(//ul[@id='tip-style-group-container'])[1]/li["+i+"]/div/a/div[2]/div[2]/span[@class='add-to-plan-text']"), "Add to plan");
            	/* Verify Tips Count*/
            	Driver.findElement(By.id("numerator")).getText().equals(i-1);
            }
      } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
      }
	}
	
	public void verifyNumberOfTips(int tipsCount){
		/* Verify Tips Count*/
    	Driver.findElement(By.id("numerator")).getText().equals(tipsCount);
		
	}

	/* To Select Next Week */
	public void selectNextWeek() throws Throwable {
		click(StylesClimatePage.nextWeekBtn, "Next week");
	}

	/* To Complete Review */
	public boolean reviewPopup() throws Throwable {
		boolean progressStatus = false;
		int ratingRandomNumber = (int)(Math.random() * 5) + 1;
		waitForVisibilityOfElement(By.id("app-modal"), "Modal Popup", IFrameworkConstant.WAIT_TIME_MEDIUM);		
		isApplicationReady();
		Driver.findElement(By.cssSelector("input[type='radio'][value='"+ratingRandomNumber+"']")).click();
		type(StylesClimatePage.editReview, "Reviewed and rated "+ratingRandomNumber + " star", "Edit Review");
		JSClick(StylesClimatePage.markCompletebBtn, "Continue to move to One Time");
		isApplicationReady();
		Thread.sleep(3000);
		String percentage = Driver.findElement(By.className("color-quaternarytext")).getAttribute("innerText")
				.split(" ")[0];
		if (percentage.indexOf("100%") > -1) {
			Waittime();
			progressStatus = true;
			assertTextMatching(StylesClimatePage.finalReviewtext,
					"You have now completed all the appointments in this plan, within 24 hours we will e-mail a survey to your team asking for achievement feedback.",
					"Plan Complete");
		}
		waitForVisibilityOfElement(StylesClimatePage.continueOneOnOne, "Wait for Continue Button", IFrameworkConstant.WAIT_TIME_MEDIUM);
		click(StylesClimatePage.continueOneOnOne, "Continue to move to One Time");
		return progressStatus;
	}

	/* To Select Check Box in Calendar */
	public boolean selectcheckbox() throws Throwable {
		boolean checkedStatus = false;
		List<WebElement> CheckBox = Driver.findElements(StylesClimatePage.checkBoxListInCalander);
		for (int i = 1; i <= CheckBox.size(); i++) {
			if (checkedStatus != true)
			{
			JSClick(StylesClimatePage.clickCheckBox, "Check Box");
			checkedStatus = reviewPopup();
			}
		}
		return checkedStatus;
	}
	
	/* Enter Percentile for each Style Feedback input field */
	public void enterStyleFeedback(){
		List<WebElement> percentileTextBox = Driver.findElements(StylesClimatePage.enterStyleFeedback);
		for (int i = 1; i <= percentileTextBox.size(); i++) {
			Driver.findElement(By.xpath("//*[@id='stylesTable']/table/tbody/tr["+i+"]/td[3]/input")).clear();
			Driver.findElement(By.xpath("//*[@id='stylesTable']/table/tbody/tr["+i+"]/td[3]/input")).sendKeys("20");
		}
	}
	
	/* Enter Percentile for each Climate Rater Feedback input field */
	public void enterClimateRaterFeedback(){
		List<WebElement> percentileTextBox = Driver.findElements(StylesClimatePage.enterClimateRaterFeedback);
		for (int i = 1; i <= percentileTextBox.size(); i++) {
			Driver.findElement(By.xpath("//*[@id='climateTable']/table/tbody/tr["+i+"]/td[4]/input")).clear();
			Driver.findElement(By.xpath("//*[@id='climateTable']/table/tbody/tr["+i+"]/td[4]/input")).sendKeys("20");
		}
	}
	
	/* Click Done button */
	public void clickDoneButton() throws Throwable {
		waitForVisibilityOfElement(By.xpath("//*[text() = 'Done']"), "Done Button", IFrameworkConstant.WAIT_TIME_MEDIUM);
		JSClick(StylesClimatePage.doneButton, "Done Button");
		isApplicationReady();
	}
	
	/*Verify ' How Useful Was This Item' and 'CONGRATULATIONS!' pop-ups of a tip.*/
	public void verifyTipPopup() throws Throwable{
		JSClick(StylesClimatePage.clickCheckBox, "Check Box");
		int ratingRandomNumber = (int)(Math.random() * 5) + 1;
		waitForVisibilityOfElement(By.id("app-modal"), "Modal Popup", IFrameworkConstant.WAIT_TIME_MEDIUM);		
		isApplicationReady();
		
		/*Assert ' How Useful Was This Item' pop-ups of a tip.*/
		assertTextMatching(StylesClimatePage.tipPopupHeaderText, "How Useful Was This Item?", "Verifying Pop Up Header Text");
		Driver.findElement(By.cssSelector("input[type='radio'][value='"+ratingRandomNumber+"']")).click();
		type(StylesClimatePage.editReview, "Reviewed and rated "+ratingRandomNumber + " star", "Edit Review");
		JSClick(StylesClimatePage.markCompletebBtn, "Continue to move to One Time");
		
		/*Assert 'CONGRATULATIONS!' pop-ups of a tip.*/
		waitForVisibilityOfElement(StylesClimatePage.continueOneOnOne, "Wait for Continue Button", IFrameworkConstant.WAIT_TIME_MEDIUM);
		assertTextMatching(StylesClimatePage.congratsPopUp, "CONGRATULATIONS!", "Verifying Pop Up Header Text");
		click(StylesClimatePage.continueOneOnOne, "Continue to move to One Time");
		
		/*Assert Check Box is Selected*/
		//waitForVisibilityOfElement(StylesClimatePage.clickCheckBox, "Assert Check Box is Selected", IFrameworkConstant.WAIT_TIME_MEDIUM);
		waitForInVisibilityOfElement(By.id("app-modal"), "Modal Popup");
		assertTrue(Driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).isSelected());
		Driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).click();
	}
	
	/*Verify 'Details' pop-up and Edit Name, Date fields of a tip.*/
	public void editNameOnDetailsTipPopup() throws Throwable{
		/*Get the author name of any/first tip*/
		WebElement tipAuthorName = Driver.findElement(By.xpath("(//ul[@class = 'day-grid current-week']/li/div/ul/li/a/footer/strong)[1]"));
		String tipOldAuthorName = tipAuthorName.getText();
		
		//Open Details popup by clicking on any/first tip
		String tipText = Driver.findElement(By.xpath("(//ul[@class = 'day-grid current-week']/li/div/ul/li/a/h4)[1]")).getText();
		String tipCurrentDay = Driver.findElement(By.xpath("(//ul[@class='day-grid current-week']//h4)[1]/ancestor::li[contains(@id,'day-')]")).getAttribute("id");
		
		//Increment day count.
		String[] textOne = tipCurrentDay.split("-");
		String textTwo=textOne[1];
		int oldDayCount = Integer.parseInt(textTwo);
		int newDayCount = oldDayCount+1;
		Driver.findElement(By.xpath("(//ul[@class = 'day-grid current-week']/li/div/ul/li/a)[1]")).click();
		
		//Assert 'DETAILS' pop-up of a tip.
		waitForVisibilityOfElement(StylesClimatePage.detailsPopUp, "Wait for Details PopUp", IFrameworkConstant.WAIT_TIME_MEDIUM);
		assertTextMatching(StylesClimatePage.detailsPopUpText, "DETAIL", "Verifying Pop Up Header Text");
		click(StylesClimatePage.detailPopUpEditLink, "Click Edit to update Name");
		
		//Select different Name from The Appointment drop down
		waitForVisibilityOfElement(StylesClimatePage.appointmentDropDown, "Wait for Appointment Dropdown", IFrameworkConstant.WAIT_TIME_MEDIUM);
		click(StylesClimatePage.appointmentDropDown, "Open Appointment Dropdown");
		List<WebElement> listOptions = Driver.findElements(StylesClimatePage.appointmentDropDownOptions);
		String tipNewAuthorName = "";
		for(int i=0; i<=listOptions.size(); i++){
			if(!(listOptions.get(i).getText()).equals(tipOldAuthorName)){
				tipNewAuthorName = listOptions.get(i).getText();
				System.out.println("NEW AUTHER NAME IS========"+tipNewAuthorName);
				int j=i+1;
				Driver.findElement(By.xpath("//*[@id='select2-drop']/ul/li["+j+"]")).click();
				break;
			}
	    }
		
		//Select different Date from Date field
		waitForVisibilityOfElement(StylesClimatePage.dateField, "Wait for Date Field", IFrameworkConstant.WAIT_TIME_MEDIUM);
		click(StylesClimatePage.dateField, "Open Date Field");
		String nextDateValue = Driver.findElement(By.xpath("//td[@class='active day']/following-sibling::td[1]")).getText();
		click(StylesClimatePage.selectNextDate, "Select Next Day from Calender");
		click(StylesClimatePage.detailPopUpDoneLink, "Click on Done to close pop up");
		waitForInVisibilityOfElement(By.id("view-edit-tip-modal"), "Modal Popup");
		isApplicationReady();
		
		//Assert tip has been moved to given date or not
		assertTrue(Driver.findElement(By.xpath("(//li[@id='day-"+newDayCount+"']//ul//h4[contains(text(),'"+tipText+"')])[1]")).isDisplayed());

		//Assert the updated Name on ToDo page
		String tipUpdatedAuthorName = Driver.findElement(By.xpath("(//li[@id='day-"+newDayCount+"']//ul//h4[contains(text(),'"+tipText+"')]/following-sibling::footer/strong)[1]")).getText();
		assertTrue(tipUpdatedAuthorName.equalsIgnoreCase(tipNewAuthorName));
   }
	
	public void removeTipFromPlan() throws Throwable{
		String tipText = Driver.findElement(By.xpath("(//ul[@class = 'day-grid current-week']/li/div/ul/li/a/h4)[2]")).getText();
		Driver.findElement(By.xpath("(//ul[@class = 'day-grid current-week']/li/div/ul/li/a)[2]")).click();
		waitForVisibilityOfElement(StylesClimatePage.detailPopUpEditLink, "Wait for Details PopUp", IFrameworkConstant.WAIT_TIME_MEDIUM);
		click(StylesClimatePage.removeTip, "Click on Remove From Plan");
		waitForVisibilityOfElement(StylesClimatePage.justThisButton, "Wait for Just This button", IFrameworkConstant.WAIT_TIME_MEDIUM);
		click(StylesClimatePage.justThisButton, "Click on Just This button");
		isApplicationReady();
		
		String AllTipsText = Driver.findElement(By.id("plan-container")).getText();
		assertFalse(AllTipsText.contains(tipText));
	}
	
	/*Verifies whether user is on LevelUp page or not, after clicking on  Styles&Climate tab.*/ 
	public void verifyLevelUpPage(){
		try {
			isApplicationReady();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String actURL = getCurrentURL();
		if(actURL.contains("roadmap"))
		  {
			 Driver.findElement(By.xpath("//button[@class='btn btn-icon btn-primary']")).click();;
			 try {
				isApplicationReady();
				selectRegion("Styles & Climate");
				isApplicationReady();
				verifyLevelUpPage();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      }
   }
}