package com.haygroup.web.libs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.CreateJobDescriptionPage;
import com.haygroup.web.page.JobPricingPage;

public class JobPricingLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	
	
		/* To Enter jobtitle*/
	public void Enterjobdescription(String Jobtext) throws Throwable {
		type(JobPricingPage.Jobtext, Jobtext, "Enter job title");
		click(JobPricingPage.Jobselect,"Jobtitle");
	}

	/* Work manager popup*/
	public void ClickWorkmanagerpopup() throws Throwable {
		click(JobPricingPage.Modalheader,"Workmanager popup is dispalyed");
		
		Thread.sleep(5000);
		//waitForVisibilityOfElement(JobPricingPage.Yesbtn, yes, 2000);
		click(JobPricingPage.Yesbtn, "Yes button");
		
		}
	
	/* Price Job  */
	public void ClickPriceJob (String Grade) throws Throwable {
		Waittime();
		isApplicationReady();
		click(JobPricingPage.selectgrade," Selectgrade");
		type(JobPricingPage.search, Grade, "Typed sucessfully");
		click(JobPricingPage.selectdrop, "selected dropvalue");
		JSClick(JobPricingPage.Jobfamily,"Jobfamily");
		JSClick(JobPricingPage.SubJobfamily,"Subfamily selected");
		JSClick(JobPricingPage.Pricejobbtn, "PriceJob button");
	
	}	
	
		
	/* Addlocation*/
	public void AddLoction(String location) throws Throwable
	{
		Waittime();
		isApplicationReady();
		JSClick(JobPricingPage.Addlocation, "Addlocaion");
		type(JobPricingPage.selectlocationtxt,location,"Typelocation");
		click(JobPricingPage.Searchtext,"Address");
	}
	
	/* RemoveAllocation*/
	public void removeLocation(String removeLocation){
		
			try {
				click(JobPricingPage.removeLocation(removeLocation), "Remove Location");
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	/* update the location details*/
	public void Clickupdate() throws Throwable
	{
		JSClick(JobPricingPage.updatebtn, "Updated the location details");
	}
	/* Save the Jobsummary details*/
	public void ClickSave() throws Throwable
	{
		JSClick(JobPricingPage.Savescenariobtn, "Saved the job summary details");
	}
	
	/* Save this pop up details*/
	String scenarioName;
	
	public String getScenarioName() {
		return scenarioName;
	}

	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	public void ClickSavethispricejob(String scenarioName) throws Throwable
	{
		Waittime();
		isApplicationReady();
		type(JobPricingPage.scenarioname,scenarioName,"Enterscenarioname");		
		click(JobPricingPage.city, "Select city name");
		click(JobPricingPage.Employeename,"Select the employeename");
		setScenarioName(scenarioName);
		
	}
	public void ClickpricejobSave() throws Throwable
	{
		Waittime();
			
		JSClick(JobPricingPage.Savebtn, "Savebutton");
		
		
	}
	
	
	public void clickClose() throws Throwable
	{
		Waittime();
		JSClick(JobPricingPage.closeButton, "Close button");
	}
	
	/*public void getPopUpText() throws Throwable{
	getText(JobPricingPage.pricingscenario, "Pricing message");
}*/
	String salSimulator;
	
	public String getSalSimulator() {
		return salSimulator;
	}

	public void setSalSimulator(String salSimulator) {
		this.salSimulator = salSimulator;
	}

	public void ClickSalarysimulator(String salscenarioname,String EmpName ) throws Throwable
	{
		isApplicationReady();
		JSClick(JobPricingPage.Salarysimulator, "Salary Simulatir");
		type(JobPricingPage.TotalRemuneration, "60000" , "Enter TotalRemuneration");
		type(JobPricingPage.TotalCashatTarget, "61000" , "Enter TotalCashatTarget");
		type(JobPricingPage.BaseSalary, "62000" , "Enter BaseSalary");
		JSClick(JobPricingPage.Calculatebutton, "Calcualtion");
		Waittime();
		JSClick(JobPricingPage.popupsavebutton, "Save button");
		type(JobPricingPage.salaryscenarioname, salscenarioname, "Enter scenario name");
		setSalSimulator(salscenarioname);
		click(JobPricingPage.Enteremployeename,"EmployeeName");
		type(JobPricingPage.searchemployeename, EmpName, "Searchemployeename");
		JSClick(JobPricingPage.salarysavebutton,"Save button");
				
	}
	public void Clickhome() throws Throwable
	{
	   isApplicationReady();
	   JSClick(JobPricingPage.Home, "Homeicon");
	   JSClick(JobPricingPage.MyPeople,"MyPeoplemodule");
	   JSClick(JobPricingPage.MyPeopleEmployeename, "Employeename");
	   Waittime();
	   JavascriptExecutor jse = (JavascriptExecutor)Driver;
	   jse.executeScript("window.scrollBy(0,250)", "");
	   
	 	   
	}
	
	public void verifyScenarioName(String scenarioName) throws InterruptedException{
		int matchCounter =0;
		List<WebElement> scenarioNames = Driver.findElements(JobPricingPage.lblScenarioNames);
		Thread.sleep(4000);
		for(int i=0;i<scenarioNames.size();i++){
		
			if(scenarioNames.get(i).getText().contains(scenarioName)){
				scenarioNames.get(i).click();
				matchCounter++;				
							
			}		
		}
		System.out.println("Match counter after loop"+matchCounter);
			if(matchCounter>=1){
				extentTestLogger.pass("Successfully verified Scenario name in the home page");
			}
			else{
				extentTestLogger.fail("Failed to find scenario name in home page");
			}
		}
		
		
	public void verifysalarySimulation(String ScenarioName1){
		int matchCounter =0;
		List<WebElement> salscenarioNames = Driver.findElements(JobPricingPage.lblsalarysimulation);
		for(int i=0;i<salscenarioNames.size();i++){
			if(salscenarioNames.get(i).getText().contains(ScenarioName1)){
				matchCounter++;
			}		
		}
			if(matchCounter>=1){
				extentTestLogger.pass("Successfully verified salary Scenario name in the home page");
			}
			else{
				extentTestLogger.fail("Failed to find salary scenario name in home page");
			}
		}
	
	}
	
	
	

	

