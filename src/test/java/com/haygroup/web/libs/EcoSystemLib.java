package com.haygroup.web.libs;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.EcoSystemPage;


public class EcoSystemLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	/**
	 * This function performs click operation on settings button
	 * 
	 */

	public void settingspagebutton() {

		try {

			Waittime();
			waitTillElementToBeClickble(EcoSystemPage.settingsbutton, "click on settings");
			click(EcoSystemPage.settingsbutton, "click on settings");
			isApplicationReady();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	
	/**
	 * This function performs language change operation
	 * 	 
	 * 
	 */

	public void verifylanguageChange(String changedLanguage){
		Select select = new  Select(Driver.findElement(EcoSystemPage.languagechange));
		WebElement option = select.getFirstSelectedOption(); 
		System.out.println("option is "+option);
		System.out.println("option text  is "+option.getAttribute("value"));
		
		if (option.getAttribute("value").equalsIgnoreCase(changedLanguage)) {
			assertTrue(true, "Passed");
			try {
				this.reporter.SuccessReport("Verify language change", "language has successfully changed to "+changedLanguage);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			try {
				this.reporter.failureReport("Verify language change", "language NOT changed  ",this.Driver);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * This function performs click operation on location dropdown
	 * 	 
	 * 
	 */

	public void locationchange() {

		try {

			click(EcoSystemPage.locationdropdown, "click on location dropdown");
			click(EcoSystemPage.locationchange, "change location");
			waitForVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed", 30);
			waitForInVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed");
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	
	public void locationchangeagain() {

		try {
			
			scrollToView(EcoSystemPage.myprofile);
			Thread.sleep(5000);
			click(EcoSystemPage.locationdropdown, "click on location dropdown");
			click(EcoSystemPage.locationchangeagain, "change location");
			waitForVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed", 30);
			waitForInVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}



	/**
	 * This function performs click operation on currency dropdown
	 * 	 
	 * 
	 */

	public void currencychange(String currencyName) {
		try {

			selectByValue(EcoSystemPage.currencydropdown, currencyName, "Currency");
			waitForVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed", 30);
			waitForInVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * This function performs click operation on timezone dropdown
	 * 	 
	 * 
	 */

	public void timezonechange(String timezonename) {

		try {

			selectByVisibleText(EcoSystemPage.timezonedropdown, timezonename, "timezone");
			waitForVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed", 30);
			waitForInVisibilityOfElement(EcoSystemPage.verifySuccessMessage(1), "Successfully Changed");

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * This function performs click operation on EMAIL NOTIFICATION PREFERENCES radio buttons
	 * 	 
	 * 
	 */

	public void emailnotification() {

		try {
			
			Thread.sleep(2000);
			scrollToView(EcoSystemPage.mailnotification);
			waitTillElementToBeClickble(EcoSystemPage.donotsendnotificationradio, "click on do not send");
			click(EcoSystemPage.donotsendnotificationradio, "click on do not send");
			waitTillElementToBeClickble(EcoSystemPage.sendnotificationradio, "click on  send");
			click(EcoSystemPage.sendnotificationradio, "click on send");
	
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}
	public void languagechange_mobile(String languagechange ) throws Throwable {
		
		waitTillElementToBeClickble(EcoSystemPage.selectPreferedLanguage,"click on Preffered languages");
		click(EcoSystemPage.selectPreferedLanguage,"click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPage.inputLanguage(languagechange),"click on Preffered languages");
		click(EcoSystemPage.inputLanguage(languagechange),"click on Preffered languages");
		waitTillElementToBeClickble(EcoSystemPage.okBtn,"OK Button");
		click(EcoSystemPage.okBtn,"OK Button");
	}

	/**
	 * This function performs click operation on gradingpreferences radio buttons
	 * 	 
	 * 
	 */


	public void gradingpreferences() {

		try {

			Waittime();
			waitTillElementToBeClickble(EcoSystemPage.Alwaysassumeidontknow,"click on Alwaysassumeidontknow");
			click(EcoSystemPage.Alwaysassumeidontknow, "click on Alwaysassumeidontknow");
			waitTillElementToBeClickble(EcoSystemPage.Alwaysassumeidontknow,"click on Alwaysassumeidontknow");
			click(EcoSystemPage.Alwaysaskthegrade, "click on Alwaysaskthegrade");
			

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * This function performs click operation on myplan radio buttons
	 * 	 
	 * 
	 */

	public void myplan() {

		try {

			Waittime();
			waitTillElementToBeClickble(EcoSystemPage.weekbasisnotification,"click on weekbasisnotification");
			click(EcoSystemPage.weekbasisnotification, "click on weekbasisnotification");
			waitTillElementToBeClickble(EcoSystemPage.dailybasisnotification,"click on dailybasisnotification");
			click(EcoSystemPage.dailybasisnotification, "click on dailybasisnotification");
			
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * This function performs click operation on startday dropdown
	 * 	 
	 * 
	 */

	public void startdaychange(String weekstartingdaydropdown) {

		try {
			Driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			selectByVisibleText(EcoSystemPage.weekstartingdaydropdown, weekstartingdaydropdown, "startday");
			Thread.sleep(2000);

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * This function performs click operation on weekend dropdown
	 * 	 
	 * 
	 */

	public void enddaychange(String weekenddaydropdown) {

		try {

		Waittime();
		scrollToView(EcoSystemPage.Myplanpreferencetext);
		selectByValue(EcoSystemPage.weekenddaydropdown, weekenddaydropdown, "endday");
		Waittime();
		} catch (Exception ex) {
		ex.printStackTrace();
		} catch (Throwable ex) {
		// TODO Auto-generated catch block
		ex.printStackTrace();
		}

		}


	/**
	 * This function performs click operation on teammeetingdaychange dropdown
	 * 	 
	 * 
	 */

	public void teammeetingdaychange(String teammeetingdaydropdown) {

		try {
			selectByVisibleText(EcoSystemPage.teammeetingday, teammeetingdaydropdown, "teammeetingday");
			Waittime();

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}
	

	/**
	 * This function performs click operation on teammeetingtimechange dropdown
	 * 	 
	 * 
	 */

	public void teammeetingtimechange(String teammeetingtimedropdown) {

		try {

			
			selectByValue(EcoSystemPage.teammeetingtime, teammeetingtimedropdown, "teammeetingtime");
			Waittime();

		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	/**
	 * This function performs click operation on languagechange dropdown
	 * 	 
	 * 
	 */


	public void languagechange(String languagechange) {

		try {

			selectByValue(EcoSystemPage.languagechange, languagechange, "languagechange");
			checkPageIsReady();
			
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	
	public void languagechangecheck(String verifytext) {
		
		
		try {

			isElementPresent(EcoSystemPage.verifylanguageChange, verifytext, true);
			Thread.sleep(2000);
					
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}


	/**
	 * This function performs click operation on contact button
	 * 	 
	 * 
	 */


	public void contact(String message) throws Throwable {

		try {

			click(EcoSystemPage.contactbutton, "click on contact button");
			isApplicationReady();
			waitForVisibilityOfElement(EcoSystemPage.checkbox, "verify check box", 5000);
			click(EcoSystemPage.checkbox, "click on checkbox");
			isApplicationReady();
			type(EcoSystemPage.message, message, "enter message");
			click(EcoSystemPage.sendbutton, "click on send");
			Thread.sleep(2000);

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}
	
	public void scroll()  throws Throwable {
		
		try {
	
			scrollToView(EcoSystemPage.closebutton);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();

		}
	}

	/**
	 * This function performs verification of text
	 * 	 
	 * 
	 */

	public String getSuccessMsg() throws Throwable {
		String successmsg = getText(EcoSystemPage.alert, "successMsg").substring(1).trim();
		return successmsg;
	}
}
