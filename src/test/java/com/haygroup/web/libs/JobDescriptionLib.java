package com.haygroup.web.libs;

import org.openqa.selenium.By;

import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.CreateJobDescriptionPage;

public class JobDescriptionLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	
	/* To click on create new button */
	public void createNewButton() throws Throwable {
		JSClick(CreateJobDescriptionPage.createNew, "Create new button");
	}

	/* Create new job description */
	public void createNewJobDescription(String jobCategory, String jobRole) throws Throwable {
		click(CreateJobDescriptionPage.selectJobCategory(jobCategory), "Job Category from list");
		type(CreateJobDescriptionPage.searchJobRoleTextBox, jobRole, "Search Job role");
		click(CreateJobDescriptionPage.selectJobRole(jobRole), "Job Role from list");
	}

	/* Handle modal Pop up */
	public void sliderModalDialogPopUp(String firstHeaderName, Boolean clickDone) throws Throwable {
		isElementPresent(CreateJobDescriptionPage.sliderPopUp, "Slider Popup", true);
		slider(CreateJobDescriptionPage.sliderToObtainStartingPoint(firstHeaderName), "Slide" + firstHeaderName, 10,
				30);
		if (clickDone == true) {
			click(CreateJobDescriptionPage.doneButton, "Done button on model Popup");
		}
	}

	/* Update Responsibilities And Work Characteristics levels */
	public void updateResponsibilitiesAndWorkCharacteristics(String externalResponsibility) throws Throwable {
		Waittime();
		isApplicationReady();
		JSClick(CreateJobDescriptionPage.responsibilityLevelUp(externalResponsibility),
				"Select Responsibilities And Work Characteristics Level");
		isApplicationReady();
	}

	/* Edit Job Name */
	public void editJobName(String jobName) throws Throwable {
		isApplicationReady();
		isElementPresent(CreateJobDescriptionPage.jobDesignFeedBack, "Job design Feedback", true);
		JSClick(CreateJobDescriptionPage.editJobName, "Edit the Job Name");
		type(CreateJobDescriptionPage.jobNameTextBox, jobName, "Update the Job name");
		JSClick(CreateJobDescriptionPage.saveJobName, "Save Job Name");
		isApplicationReady();
		waitForVisibilityOfElement(CreateJobDescriptionPage.getupdatedJobName(jobName), "Updated Job Name", IFrameworkConstant.WAIT_TIME_MEDIUM);
	}

	/* Update about the company */
	public void editAboutTheCompany(String headerName, String updateDescription) throws Throwable {
		click(CreateJobDescriptionPage.clickResponsibility(headerName), "About The Company Header");
		type(CreateJobDescriptionPage.aboutTheCompanyTextBox, updateDescription, "About company textbox");
		click(CreateJobDescriptionPage.aboutSaveButton, "Save button on About comapny");
	}

	/* Update job purpose */
	public void editJobPurpose(String headerName, String updatePurpose) throws Throwable {
		click(CreateJobDescriptionPage.clickResponsibility(headerName), "Job Purpose");
		type(CreateJobDescriptionPage.jobPurposeTextBox, updatePurpose, "Job purpose textbox");
		click(CreateJobDescriptionPage.jobPurposeSaveButton, "Save button on Job purpose");
	}

	/* Click Save as draft */
	public void clickSaveAsDraft() throws Throwable {
		click(CreateJobDescriptionPage.selectButtonFromHeader("Save As Draft"), "Save as Draft button");
		isElementPresent(CreateJobDescriptionPage.confirmationModelPopUp, "Confirmation model popup", true);
		click(CreateJobDescriptionPage.yesButtonOnModelPopup, "Yes button on model popup");
		waitForVisibilityOfElement(CreateJobDescriptionPage.savedOnLabel, "Saved On Label", IFrameworkConstant.WAIT_TIME_MEDIUM);
	}

	/* Publish the Job description */
	public void clickPublishButton(String publishedJob) throws Throwable {
		JSClick(CreateJobDescriptionPage.selectButtonFromHeader("Publish"), "Publish button");
		waitForVisibilityOfElement(CreateJobDescriptionPage.publishedJobName(publishedJob), "Published Job title",IFrameworkConstant.WAIT_TIME_MEDIUM);
	}

	/* Search and delete the job Description */
	public void searchAndDeleteJob(String jobName, String actionOption) throws Throwable {
		Waittime();
		isApplicationReady();
		type(CreateJobDescriptionPage.searchJobDescriptionTextBox, jobName, "Search Job role");
		Waittime();
		isApplicationReady();
		JSClick(By.xpath(".//h3[contains(text(),'" + jobName + "')]/..//button"), "Menu of publishedJob");
		JSClick(CreateJobDescriptionPage.selectJobMenuOptions(actionOption), "Delete Job role");
		JSClick(CreateJobDescriptionPage.yesButtonOnModelPopup, "Yes button on model popup");
		waitForInVisibilityOfElement(CreateJobDescriptionPage.publishedJobName(jobName), "Search Job role");
	}
}
