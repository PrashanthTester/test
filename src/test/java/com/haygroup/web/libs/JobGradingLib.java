package com.haygroup.web.libs;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.page.CreateJobDescriptionPage;
import com.haygroup.web.page.JobGradingPage;

public class JobGradingLib extends HayGroupCommonLib {

	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");

	String jobTitle;
	
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public void selectName(String name) {
		try{
		type(JobGradingPage.selectNameTextBox, name, "Entering a Name");
		click(JobGradingPage.newEntry, " 'new' drop down entry");
		click(JobGradingPage.continueButton, "click Continue button");
		isApplicationReady();
		setJobTitle(name);
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void selectJobFamilies() {
		try{
		click(JobGradingPage.selectJobFamily, "Job Family");
		click(JobGradingPage.selectSubFamily, "Sub Family");
		click(JobGradingPage.selectJobType, "Job Type");
		click(JobGradingPage.continueButton, "click Continue button");
		isApplicationReady();
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void clickGradeThisJobButton()  {
		try{
		click(JobGradingPage.continueButton, " Continue button");
		isApplicationReady();
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickApplyGradeButton() {
		try{
		click(JobGradingPage.applyGradeButton, "Apply Grade button");
		isApplicationReady();
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void selectApplyGradeCheckbox() {
		try{
		click(JobGradingPage.applyGradeCheckBox, "select check box");
		click(JobGradingPage.applyGradeButtonInResultsPage, "click Apply Grade button");
		System.out.println(JobGradingPage.applyGradeSuccessMsg);
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void closeButton() {
		try{
		click(JobGradingPage.closeButton, "click close button");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void saveGradingScenarioButton() {
		try{
		click(JobGradingPage.saveGradingScenarioBtn, "click Save Grading Scenario");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void saveGradeCheckBox() {
		try{
		click(JobGradingPage.saveGradeCheckBox, "click Save Grading Scenario");
		click(JobGradingPage.saveGradingScenarioButton, "click Save Grading Scenario");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickHome() {
		try{
		click(JobGradingPage.homeButton, "click home button");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickEmployeeName() {
		try{
		click(JobGradingPage.employeeNameInMyPeople, "click my people");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getSuccessMsg() throws Throwable {
		String successGrade = getText(JobGradingPage.applyGradeSuccessMsg, "successMsg").substring(1).trim();
		return successGrade;
	}

	public String getJobGradeInResultsPage()throws Throwable {
	
		String jobGrade = getText(JobGradingPage.jobGradeInResultsPage, "jobGrade").trim();
		return jobGrade;
		
	}

	public String getJobGradeInMyPeople()throws Throwable {
		
		String jobGradeInMyPeople = getText(JobGradingPage.jobGradeInMyPeople, "jobGrade").trim();
		return jobGradeInMyPeople;

	}

	public void sliderModalDialogPopUp(String firstHeaderName, Boolean clickContinue) {
		try{
		isApplicationReady();
		slider(JobGradingPage.sliderToObtainStartingPoint(firstHeaderName), "Slide" + firstHeaderName, 10, 30);
		if (clickContinue == true) {
			click(JobGradingPage.continueButton, "Continue button on Job factors page");
		}

	
	}catch (Throwable e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	
	public void verifyGradingScenario(String scenarioName){
        int matchCounter =0;
        List<WebElement> scenarioNames = Driver.findElements(JobGradingPage.gradingScenarios);
        for(int i=0;i<scenarioNames.size();i++){
               if(scenarioNames.get(i).getText().contains(scenarioName)){
            	   scenarioNames.get(i).click();
                     matchCounter++;
               }            
        }
               if(matchCounter>=1){
                     extentTestLogger.pass("Successfully verified Scenario name in the home page");
               }
               else{
                     extentTestLogger.fail("Failed to find scenario name in home page");
               }
        }
}
