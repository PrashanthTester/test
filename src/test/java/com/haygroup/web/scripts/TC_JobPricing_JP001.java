package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.JobPricingLib;
import com.haygroup.web.page.HomePage;
import com.haygroup.web.page.JobPricingPage;
import com.haygroup.web.page.StylesClimatePage;

/*This Test Case is For Jobtitle */

public class TC_JobPricing_JP001 extends JobPricingLib {

	@DataProvider
	public Object[][] getTestDataFor_JobPricing() {
		return TestUtil.getData("Create_JobPricing", TestData, "JobPricing");
	}
	
	
    
	@Test(dataProvider = "getTestDataFor_JobPricing")
	public void JobPricing(Hashtable<String, String> data) {
		
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Create Job description");	
				/*launchAppURL*/
				launchApplication("JobPricing",data.get("AppURL"));
				/* Login to the application */
				loginToApplication(data.get("UserName").trim(), data.get("Password").trim());
				assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), data.get("SelectRegion"),						
						"Employee Record Details Header");
				
				//Select Region as Job Pricing
				selectRegion(data.get("SelectRegion"));
				
				/*Enter Jobtilte*/
				Enterjobdescription(data.get("Jobtitle"));
				//assertTextMatching(JobPricingPage.Jobtext,data.get("Jobtitle"), "Enterjobtitle");
                  
				// Workmanager popup  handling
				ClickWorkmanagerpopup();
				
				//click on Pricjob 
				ClickPriceJob(data.get("Grade"));
				//assertTextMatching(JobPricingPage.selectgrade, data.get("Droplist"),"Select the Gradedroplist");
				System.out.println("Data ********* "+data.get("Location"));
				AddLoction(data.get("Location"));
				removeLocation(data.get("Location"));
				Clickupdate();
				ClickSave();
				//assertTextMatching
				ClickSavethispricejob(data.get("ScenarioName")+generateRandomNumber());
				String scenarioName=getScenarioName();
				//getPopUpText();
				ClickpricejobSave();
				clickClose();
				ClickSalarysimulator(data.get("ScenarioName1")+generateRandomNumber(),data.get("EmpName"));
				String salSimulator=getSalSimulator();
				Clickhome();
			    verifyScenarioName(scenarioName);
				//verifysalarySimulation(salSimulator);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@AfterMethod
	public void logoutApplication() throws Throwable {
		/*To Logout from the application*/
		logOutFromApplication();
	}
}