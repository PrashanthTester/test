package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.EcoSystemLib;

public class TC_EcoSystem_Eco001 extends EcoSystemLib{


	@DataProvider
	public Object[][] getTestDataFor_languagechange() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_languagechange")
	public void EcoSystemlanguagechange(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.languagechange");		

				/*launchAppURL*/

				launchApplication("EcoSystem",data.get("AppURL"));

				/* Login to the application */
				//loginToHaygroupApplication(data.get("UserName"), data.get("Password"));
				loginToApplication(data.get("UserName"), data.get("Password"));

				/*click on settings*/
				settingspagebutton();

			/*	click on settings*/
				languagechange(data.get("Language1"));

				checkPageIsReady();

			/*	 Login to the application */
				loginToHaygroupApplication(data.get("UserName"), data.get("Password"));

				//loginToHaygroupApplication(data.get("UserName"), data.get("Password"));


				/*click on settings*/
				settingspagebutton();
				
				languagechangecheck("verifytext");
				
				// WRITE AN ASSERT STATEMENT FOR LANGUAGE CHANGE
				verifylanguageChange(data.get("Language1"));

				/*click on settings*/
				languagechange(data.get("Language2"));

				checkPageIsReady();

				/* Login to the application */
				//loginToApplication(data.get("UserName"), data.get("Password"));


			}									
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {

		//Logout from the application 
	//	logOutFromApplication();

		//captureBrowserConsoleLogs(Driver);
	}
}
