package com.haygroup.web.scripts;

import static org.testng.Assert.assertEquals;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.JobGradingLib;
import com.haygroup.web.page.HomePage;
import com.haygroup.web.page.JobGradingPage;

public class TC_JobGrading extends JobGradingLib{
	@DataProvider
	public Object[][] getTestDataFor_CreateJobGrading() {
		return TestUtil.getData("createJobGrading", TestData, "JobGrading");
	}
		@Test(dataProvider = "getTestDataFor_CreateJobGrading")
		public void JobGradingCreation(Hashtable<String, String> data) {
			try {
				String userID = "";
				if (data.get("RunMode").equals("Y")) {
					this.reporter.initTestCaseDescription("TC.Create Job Grading");				
					// Login to the application 
					launchApplication("JobGrading",data.get("AppURL"));
					loginToApplication(data.get("UserName"), data.get("Password"));
					assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), data.get("SelectRegion"),						
							"Employee Record Details Header");
					
					
					//Select Region as Job Grading 
					selectRegion(data.get("SelectRegion"));	
					assertTextMatching(JobGradingPage.createNameLabel, data.get("selectNameTitle"), "Select or Create a Name title");	
					selectName(data.get("JobTitle")+generateRandomNumber());
					String jobTitle=getJobTitle();
					isApplicationReady();
					//assertTextMatching(JobGradingPage.specifyTheRoleLabel, data.get("SpecifyTheRoleTitle"), "specify The Role Title");
					
					selectJobFamilies();
					assertTextMatching(JobGradingPage.positionThisJobLabel, data.get("PositionThisJobTitle"), "Position This Job Title");
					 //set the sliders 
					sliderModalDialogPopUp(data.get("JobHolderPolicyDevelopment"), false);
					sliderModalDialogPopUp(data.get("JobHolderRecruiting"), false);
					sliderModalDialogPopUp(data.get("JobHolderSupervision"), true);
			
					
					assertTextMatching(JobGradingPage.resultsLabel, data.get("ResultsTitle"), "Results Title");
					String Grade = getJobGradeInResultsPage();
					clickApplyGradeButton();
					selectApplyGradeCheckbox();
					assertEquals(getSuccessMsg(), data.get("ApplyGradeSuccess").trim());
					
					closeButton();
					saveGradingScenarioButton();
					saveGradeCheckBox();
					assertEquals(getSuccessMsg(),data.get("SavedGradeSuccess").trim() );
					closeButton();
					clickHome();
					selectRegion(data.get("SelectMyPeople").trim());
					clickEmployeeName();
					getJobGradeInMyPeople();
					assertEquals(getJobGradeInMyPeople(),Grade);
					verifyGradingScenario(jobTitle);
				}
				
			}
			
				catch (Exception e) {
					e.printStackTrace();
					//throw new RuntimeException(e);
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@AfterMethod
			public void logoutApplication() throws Throwable {
				 //Logout from the application 
				logOutFromApplication();
				//captureBrowserConsoleLogs(Driver);
			}
}
		


	
		
	

