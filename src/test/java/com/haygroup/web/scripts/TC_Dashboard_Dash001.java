package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;









import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.DashboardLib;
import com.haygroup.web.libs.JobDescriptionLib;
import com.haygroup.web.page.CreateJobDescriptionPage;
import com.haygroup.web.page.DashboardPage;
import com.haygroup.web.page.HomePage;

/*This Test is To test Dashboard functionality*/
public class TC_Dashboard_Dash001 extends DashboardLib {
	public String userFullName = "";
	public String email = "";
	@DataProvider
	public Object[][] getTestDataFor_verifyDashboard() {
		return TestUtil.getData("VerifyDashboard", TestData, "Dashboard");
	}
	
	/**
	 * @author E001167
	 * @param data
	 * @Description This test will create new users, assign apps, and add/remove admin access
	 */
	@Test(dataProvider = "getTestDataFor_verifyDashboard", priority=1)
	public void verifyDashboardFunctionality(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Verify Dashboard");	
				
				launchApplication("Dashboard",data.get("AppURL"));
				/* Login to the application */
				loginToApplication(data.get("UserName"), data.get("Password"));
				
				assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), data.get("SelectRegion"),						
						"Employee Record Details Header");
				
				//Select Region as Job Description 
				selectRegion(data.get("SelectRegion"));
				//Select Company
				//selectCompany();
				//navigate to people tab
				navigateToPeopleTab(data.get("NavigationTabs_Dashboard"));
				// create user
				email = data.get("Email")+generateRandomNumber()+"@gmail.com";
				userFullName= createUserDetails(data.get("FirstName"), data.get("LastName")+generateRandomNumber(),email);
				//verify user is created successfully
				verifyUserDetails(userFullName);
				//select App Settings for the user
				selectAppSettings();
				// verify user name in the list
				By userNameXpath =  verifyUserDetails(userFullName);
				// select the user 
				selectUser(userNameXpath);
				// edit the user details
				editUserDetails(userFullName,data.get("TimeZone"));
				
				/*assertTextMatching(DashboardPage.profieHeader, userFullName, "User Full Name");*/
				//navigate to Styles and Climate Tab
				navigateToStylesAndClimateTab(data.get("StylesAndClimateTab"));
				// verify the Admin changes for Styles and climate tab
				verifyAdminChanges(userFullName);
				//navigate to Journey tab
				navigateToJourneyeTab(data.get("JourneyTab"));
				// verify admin changes for the Journey Tab
				verifyAdminChanges(userFullName);
				//navigate to Job Description Tab
				navigateToJobDescriptionTab(data.get("JDTab"));
				// verify admin changes for the Job Description Tab
				verifyAdminChanges(userFullName);
				//send user data to excel sheet
				//sendUserToExcel("Dashboard", "AutoUser", 2, email);
							
			}
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		/* Logout from the application */
		logOutFromApplication();

		//captureBrowserConsoleLogs(Driver);
	}
}
