package com.haygroup.web.scripts;

import java.util.Hashtable;




import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.JobDescriptionLib;
import com.haygroup.web.page.HomePage;
import com.haygroup.web.page.LoginPage;

/*This Test is To create a failure Job description*/
public class TC_FailureTestCase extends JobDescriptionLib {
	Boolean verifiedFlag = false;
	@DataProvider
	public Object[][] getTestDataFor_CreateJobDescription() {
		return TestUtil.getData("createJobDescription", TestData, "JobDescription");
	}
	
	@Test(dataProvider = "getTestDataFor_CreateJobDescription")
	public void failureTestCase(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.FailureTestCase");
				
				isApplicationReady();		
				/* Login to the application */

				loginToApplication(data.get("UserName"), data.get("Password"));
				/*Assertion Failure*/
				verifiedFlag = assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), "Select Job Description",
						"Employee Record Details Header");
				if(verifiedFlag){
					/* Select Region as Job Description */
					selectRegion(data.get("SelectRegion"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		/* Logout from the application */		
		//logOutFromApplication();
	}
}
