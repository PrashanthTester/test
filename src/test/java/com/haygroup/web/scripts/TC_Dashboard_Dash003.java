package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.DashboardLib;
import com.haygroup.web.libs.StylesClimateLib;
import com.haygroup.web.page.HomePage;

public class TC_Dashboard_Dash003 extends DashboardLib {
	
	
	@DataProvider
	public Object[][] getTestDataFor_VerifyDashboard() {
		return TestUtil.getData("VerifyDashboard", TestData, "Dashboard");
	}
	/**
	 * @description this test is
	 * @param data
	 */
	@Test(dataProvider = "getTestDataFor_VerifyDashboard",priority=3)
	public void Dashboard003(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Create dashboard");				
				/* Login to the application */
				launchApplication("Dashboard",data.get("AppURL"));
				loginToApplication(data.get("UserName"), data.get("Password"));
				assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), data.get("SelectRegion"),						
						"Employee Record Details Header");
				
				//Select Region as Job Description 
				selectRegion("Dashboard");
				//download company usage report
				clickCompanyDownloadUsageReprot();
				//navigate to Styles and Climate Tab
				navigateToStylesAndClimateTab(data.get("StylesAndClimateTab"));
				//download styles and climate usage reprort
				clickStylesAndClimateUsageReprot();
				//navigate to Journey tab
				navigateToJourneyeTab(data.get("JourneyTab"));	
				//download journey Hr report
				clickJourneyDownloadHrReprot();
				
			}
		}			
			catch (Exception e) {
				e.printStackTrace();
				//throw new RuntimeException(e);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@AfterMethod
		public void logoutApplication() throws Throwable {
			/*To Logout from the application*/
			logOutFromApplication();
		}
	
}
				


