package com.haygroup.web.scripts;
import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.gallop.utilities.Xls_Reader;
import com.haygroup.web.libs.DashboardLib;
import com.haygroup.web.libs.StylesClimateLib;
import com.haygroup.web.page.HomePage;

public class TC_Dashboard_Dash002 extends DashboardLib {

	
	@DataProvider
	public Object[][] getTestDataFor_VerifyDashboard() {
		return TestUtil.getData("VerifyDashboard", TestData, "Dashboard");
	}
	
	
	/**
	 * @description this test is
	 * @param data
	 */
	@Test(dataProvider = "getTestDataFor_VerifyDashboard",priority=2)
	public void verifyDashboard002(Hashtable<String, String> data) {
		try {
				if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Create dashboard");				
				/* Login to the application */
				launchApplication("Dashboard",data.get("AppURL"));
				loginToApplication(data.get("AutoUser").trim(), data.get("Password").trim());
				/*clickPrivacyPolicy();
				clickAcceptAndContinue();*/
				selectRegion(data.get("JDRegion"));
				verifyNavigation(data.get("DashboardNavigation"));
				navigateToHomePage();
				selectRegion(data.get("SCRegion"));
				verifyNavigation(data.get("SCNavigation"));
				navigateToHomePage();
				selectRegion(data.get("PeopleRegion"));
				verifyNavigation(data.get("PeopleNavigation"));
				navigateToHomePage();
				selectRegion(data.get("PlanRegion"));
				clickPopup();
				verifyNavigation(data.get("PlanNavigation"));
				navigateToHomePage();
				
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		
		@AfterMethod
		public void logoutApplication() throws Throwable {
			/*To Logout from the application*/
			logOutFromApplication();
		}
	}



