package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.JobMappingLib;

public class TC_JobMapping extends JobMappingLib{
	
	@DataProvider
	public Object[][] getTestDataFor_JobMapping() {
		return TestUtil.getData("createJobDescription", TestData, "Job Mapping");
	}
	
	@Test(dataProvider = "getTestDataFor_JobMapping")
	public void jobMapping(Hashtable<String, String> data) throws Throwable {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Create Job description");				
				/* Login to the application */
				System.out.println("User name *** "+data.get("UserName"));
				
				launchApplication("Job Mapping",data.get("AppURL"));
				loginToJMApplication(data.get("UserName"), data.get("Password"));
				uploadFile();
				MapFunctions();
				clickManageFunctions();
				DefineIndustry();
				selectJob();
				selectJobbank();
				selectJobbankkeyacc();
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
}