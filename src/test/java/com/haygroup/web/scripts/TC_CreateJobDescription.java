package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;





import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.JobDescriptionLib;
import com.haygroup.web.page.CreateJobDescriptionPage;
import com.haygroup.web.page.HomePage;

/*This Test is To create Job description*/
public class TC_CreateJobDescription extends JobDescriptionLib {

	@DataProvider
	public Object[][] getTestDataFor_CreateJobDescription() {
		return TestUtil.getData("createJobDescription", TestData, "JobDescription");
	}
	
	@Test(dataProvider = "getTestDataFor_CreateJobDescription")
	public void createJobDescription(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.Create Job description");				
				/* Login to the application */
				launchApplication("JobDescription",data.get("AppURL"));
				loginToApplication(data.get("UserName"), data.get("Password"));
				assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), data.get("SelectRegion"),						
						"Employee Record Details Header");
				
				//Select Region as Job Description 
				selectRegion(data.get("SelectRegion"));
				
				assertTextMatching(CreateJobDescriptionPage.createNew, "Create New", "Create new Button");				
				//Click on Create New 
				createNewButton();
				
				assertTextMatching(CreateJobDescriptionPage.jobDescriptionTitle, data.get("SelectRegion").toUpperCase(),									
						"Job description Page Title");
				
				 //Create New Job Description 
				createNewJobDescription(data.get("FilterFunction"), data.get("JobDescription"));
				assertTextMatching(CreateJobDescriptionPage.sliderPopUpHeader, data.get("SliderPopupTitle"),
						"Modal popup Title");
				 //set the sliders 
				sliderModalDialogPopUp(data.get("JobHolderInitiative"), false);
				sliderModalDialogPopUp(data.get("JobHolderComplexity"), false);
				sliderModalDialogPopUp(data.get("JobHolderCoordination"), true);
				assertTextMatching(CreateJobDescriptionPage.responsibilityHeader, data.get("ResponsibilityHeader"),
						"Responsibility Header");
				
				 /*Update Responsibilities and work characteristics*/ 
				updateResponsibilitiesAndWorkCharacteristics(data.get("InternalClientCustomerResponsibility"));
				updateResponsibilitiesAndWorkCharacteristics(data.get("DataCollectionandAnalysis"));
				/*Update the Job Name*/ 
				editJobName(data.get("JobName"));
				assertTextMatching(CreateJobDescriptionPage.getupdatedJobName(data.get("JobName")), data.get("JobName"),
						"Updated Job Name");
				/*Update About the Company*/
				editAboutTheCompany(data.get("AboutTheCompany"), data.get("CompanyDescription"));
				/*Update Job Purpose */
				editJobPurpose(data.get("JobPurpose"), data.get("JobPurposeDescription"));
				/*Click Save as Draft */
				clickSaveAsDraft();
				/*Click on Publish */
				clickPublishButton(data.get("JobName"));				
				assertTextMatching(CreateJobDescriptionPage.publishedJobName(data.get("JobName")), data.get("JobName"),
						"Published Job Name");
				/*Search and Delete the Job Created*/ 
				searchAndDeleteJob(data.get("JobName"), data.get("ActionToBeDone"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		/* Logout from the application */
		logOutFromApplication();

		//captureBrowserConsoleLogs(Driver);
	}
}
