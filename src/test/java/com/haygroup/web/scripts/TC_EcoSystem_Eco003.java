package com.haygroup.web.scripts;

import static org.testng.Assert.assertEquals;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.EcoSystemLib;

public class TC_EcoSystem_Eco003 extends EcoSystemLib {

	@DataProvider
	public Object[][] getTestDataFor_settingspage() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");
	}

	@Test(dataProvider = "getTestDataFor_settingspage")
	public void EcoSystemsettingspage(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.settingspage");
				
				/*launchAppURL*/
				 
			    launchApplication("EcoSystem",data.get("AppURL"));

				/* Login to the application */
				loginToApplication(data.get("UserName"), data.get("Password"));

				/* click on settings */
				settingspagebutton();

				/* click on contact button */
				contact(data.get("message"));

				/* verify the text */
				assertEquals(getSuccessMsg(), data.get("SuccessMessage").trim());
				
				scroll();
				
			

			}
		} catch (Exception e) {
			e.printStackTrace();
			// throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		// Logout from the application
		logOutFromApplication();

		// captureBrowserConsoleLogs(Driver);
	}
}
