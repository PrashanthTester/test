package com.haygroup.web.scripts;

import java.util.Hashtable;



import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.*;
import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.JobDescriptionLib;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
public class fluentWaitTest extends JobDescriptionLib{
	@DataProvider
	public Object[][] getTestDataFor_FluentTest() {
		return TestUtil.getData("createJobDescription", TestData, "JobDescription");
	}
	
	@Test(dataProvider = "getTestDataFor_FluentTest")
	public void createJobDescription(Hashtable<String, String> data) {
		try {	
			
			if (data.get("RunMode").equals("Y")) {
/*				WebDriver driver = new FirefoxDriver();
				String workingDir = System.getProperty("user.dir");*/				
				//Driver.get("H:\\fluentWaitCommandDemoPage.html");
		 				
				
				WebDriverWait wait1 = new WebDriverWait(this.Driver, 20000);
				
				
				//wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("dynamicText1")));
				
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(Driver);
				wait.pollingEvery(250, TimeUnit.MILLISECONDS);
				wait.withTimeout(2, TimeUnit.MINUTES);
				wait.ignoring(NoSuchElementException.class); // We need to ignore this
																// exception.
		 
				Function<WebDriver, Boolean> myFunction = new Function<WebDriver, Boolean>() {
					public Boolean apply(WebDriver arg0) {
						Boolean elementFound = false;
						System.out.println("Checking for the object!!");
						WebElement element = arg0.findElement(By.id("dynamicText"));
						if (element != null) {
							System.out.println("A new dynamic object is found.");
							elementFound=true;
						}
						return true;
					}
				};
				
				//wait.until(ExpectedConditions.elementToBeClickable(By.id("dynamicText")));
				//waitUntil(Driver,ExpectedConditions.elementToBeClickable(By.id("dynamicText1")), 20000, 250, true);
				//waitUntil(ExpectedConditions., waitMilliSeconds, pollingMilliSeconds, shouldThrowException)
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {
		/* Logout from the application */
		Driver.quit();

		//captureBrowserConsoleLogs(Driver);
	}

}
