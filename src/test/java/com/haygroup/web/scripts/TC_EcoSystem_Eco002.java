package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.EcoSystemLib;

public class TC_EcoSystem_Eco002 extends EcoSystemLib {

	@DataProvider
	public Object[][] getTestDataFor_locationchange() {
		return TestUtil.getData("EcoSystem", TestData, "EcoSystem");

	}

	@Test(dataProvider = "getTestDataFor_locationchange")
	public void EcoSystemlocationchange(Hashtable<String, String> data) {
		try {
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.locationchange");

				/* launchAppURL */

				launchApplication("EcoSystem", data.get("AppURL"));

				/* Login to the application */
				loginToApplication(data.get("UserName"), data.get("Password"));

				/* click on settings */
				settingspagebutton();

				/* click on location change dropdown */
				locationchange();

				/* click on currency change dropdown */
				currencychange(data.get("Currencychange1"));
				
				/* click on timezone change dropdown */
				timezonechange(data.get("Timezonechange1"));
				
				/* click on EMAIL NOTIFICATION PREFERENCES */
				emailnotification();

				/* click on GRADING PREFERENCES */
				gradingpreferences();

				/* click on MY PLAN */
				myplan();

				/* click on start day change dropdown */
				startdaychange(data.get("Startdaychange1"));

				checkPageIsReady();

				Thread.sleep(5000);

				/* click on end day change dropdown */
				enddaychange(data.get("Enddaychange1"));

				/* click on team meeting day change dropdown */
				teammeetingdaychange(data.get("Teammeetingdaychange1"));

				/* click on team meeting time change dropdown */
				teammeetingtimechange(data.get("Teammeetingtimechange1"));
				
				//locationchangeagain();
				
				locationchangeagain();
								
				/* click on currency change dropdown */
				currencychange(data.get("Currencychange2"));

				/* click on timezone change dropdown */
				timezonechange(data.get("Timezonechange2"));

				/* click on start day change dropdown */
				startdaychange(data.get("Startdaychange2"));

				checkPageIsReady();

				Thread.sleep(5000);

				/* click on end day change dropdown */
				enddaychange(data.get("Enddaychange2"));

				/* click on team meeting day change dropdown */
				teammeetingdaychange(data.get("Teammeetingdaychange2"));

				/* click on team meeting time change dropdown */
				teammeetingtimechange(data.get("Teammeetingtimechange2"));
				// teammeetingtimechange("0230");

			}
		} catch (Exception e) {
			e.printStackTrace();
			// throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void logoutApplication() throws Throwable {

		// Logout from the application
		logOutFromApplication();

		// captureBrowserConsoleLogs(Driver);
	}
}