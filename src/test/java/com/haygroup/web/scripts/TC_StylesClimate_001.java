package com.haygroup.web.scripts;

import java.util.Hashtable;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gallop.utilities.TestUtil;
import com.haygroup.web.libs.StylesClimateLib;
import com.haygroup.web.page.HomePage;
import com.haygroup.web.page.StylesClimatePage;

/*This Test Case is For Styles And Climate */
public class TC_StylesClimate_001 extends StylesClimateLib {
	@DataProvider
	public Object[][] getTestDataFor_StylesAndClimate() {
		return TestUtil.getData("Create_Styles_And_Climate", TestData, "StylesAndClimate");
	}
    
	@Test(dataProvider = "getTestDataFor_StylesAndClimate")
	public void StylesClimateCreation(Hashtable<String, String> data) {
		try {
			String userID = "";
			if (data.get("RunMode").equals("Y")) {
				this.reporter.initTestCaseDescription("TC.creation Of Styles & Climate");
				
				/*To Login to the Application*/
				if (this.testParameters.browser.equals("chrome")){
					userID = data.get("UserNameChrome");					
				}
				else if (this.testParameters.browser.equals("firefox")){
					userID = data.get("UserNamefirefox");					
				}
				
				else{
					userID = data.get("UserName");	
				}
				launchApplication("StylesAndClimate",data.get("AppURL"));
				loginToApplication(userID, data.get("Password"));
				assertTextMatching(HomePage.selectregion(data.get("SelectRegion")), data.get("SelectRegion"),
						"Employee Record Details Header");
				/*Select Region to Select Styles and Climate*/
				selectRegion(data.get("SelectRegion"));			
				/*Verifies whether user is on LevelUp page or not, after clicking on  Styles&Climate tab.*/
				verifyLevelUpPage();
				/*Select Enter Feedback Manually*/
				clickEnterFeedBackManuallyButton();
				/*Handle overwrite existing feedback*/
				clickOverWritePopup();
				assertTextMatching(StylesClimatePage.enterUniqueCodeLabel, data.get("UniqueCodeLabel"),
						"Enter Unique code label");
				/*Enter Unique Code*/
				enterUniqueCode(data.get("UniqueCode"));
				assertTextMatching(StylesClimatePage.enterFeedbackmanually, data.get("FeedBackTitle"),
						"Enter Feedback Manually");
				/*Continue the road map activity*/
				continueActivateRoadMap();
				assertTextMatching(StylesClimatePage.roadMapLabel, data.get("RoadMapTitle"), "Road map Title");
				/*Select Design My Own Plan*/
				designMyPlan();
				/*Select Create A Plan For Me*/
				createPlanForMe();
				assertTextMatching(StylesClimatePage.myPlanTrip, data.get("OneOnOneTitle"), "One on One Title");
				//Verify number of tips for automatically created plan
				verifyNumberOfTips(5);
				//Click Continue button
				clickContinueButton();
				assertTextMatching(StylesClimatePage.myPlanTrip, data.get("TeamTitle"), "Team Title");
				//Verify number of tips for automatically created plan
				verifyNumberOfTips(4);
				//Click Continue button
				clickContinueButton();
				assertTextMatching(StylesClimatePage.myPlanTrip, data.get("OneTimeTitle"), "My plan label");
				//Verify number of tips for automatically created plan
				verifyNumberOfTips(0);
				//Navigate to the To-do-list Page
				clickContinueButton();
				assertTextMatching(StylesClimatePage.toDoList, data.get("ToDoList"), "To do list");	
			}
		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@AfterMethod
	public void logoutApplication() throws Throwable {
		/*To Logout from the application*/
		logOutFromApplication();
	}

}
