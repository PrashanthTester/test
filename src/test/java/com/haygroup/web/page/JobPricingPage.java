package com.haygroup.web.page;

import org.openqa.selenium.By;

public class JobPricingPage {

	public static By Jobpricing;
	public static By Jobtext;
	public static By Jobselect;
	public static By regionPath;
	public static By Modalheader;
	public static By Yesbtn;
	public static By selectgrade;
	public static By search;
	public static By selectdrop;
	public static By Jobfamily;
	public static By SubJobfamily;
	public static By Pricejobbtn;
	public static By Addlocation;
	public static By Addlocationpopup;
	public static By selectlocationtxt;
	public static By Searchtext;
	public static By Resultcard;
	public static By updatebtn;
	public static By Savescenariobtn;
	public static By Savethispricedjob;
	public static By scenarioname;
	public static By city;
	public static By Employeename;
	public static By Savebtn;
	public static By pricingscenario;
	public static By address;
	public static By removeicon; 
	public static By closeButton;
	public static By Salarysimulator;
	public static By TotalRemuneration;
	public static By TotalCashatTarget;
	public static By BaseSalary;
    public static By Calculatebutton;
    public static By Salarysimulatorpopup;
    public static By popupsavebutton;
    public static By Closebutton;
    public static By salaryscenarioname;
    public static By searchemployeename;
    public static By salarysavebutton;
    public static By Sucesstext;
    public static By Home;
    public static By MyPeople;
    public static By MyPeopleEmployeename; 
    public static By Employeesalarysimulations;
    public static By Enteremployeename;
    public static By lblScenarioNames;
    public static By lblsalarysimulation;
    





	/* Page Objects Of Home Page */
	static     {
		Jobpricing                =  By.cssSelector("#job-pricing");
		Jobtext                   =  By.cssSelector("#s2id_job-search input");
		Jobselect                 =  By.cssSelector("#select2-drop>ul>li:nth-child(1)>div span");
		Modalheader               =  By.xpath("//div[@class='modal hide fade in']/div[2]");
		//Yesbtn                  =  By.cssSelector("[class='modal hide fade in']>div[class='modal-footer']>a[class='btn yes']");
		Yesbtn                    =  By.xpath("//a[text()='Yes']");
		//selectgrade               =  By.xpath("//span[text()='Select Grade']  ");
		selectgrade               =   By.cssSelector("#s2id_select2-grade-pricing-job span");
		search                    =  By.xpath("//div[@class='select2-search']/input");
		selectdrop                =  By.xpath("//div[@id='select2-drop']/ul/li/div/span");
		Jobfamily                 =  By.xpath(".//*[@id='job-select-family-region']/ul/li[1]/a/p[2]");
		SubJobfamily              =  By.xpath(".//*[@id='job-select-sub-family-region']/ul/li[2]/a/p[2]");
		Pricejobbtn               =  By.id("continue-button");
		Addlocation               =  By.cssSelector("#regions-layout>div>div:nth-child(1) a");
		Addlocationpopup          =  By.cssSelector(".modal-header>h3");
		selectlocationtxt         =  By.cssSelector("#locations-modal>div:nth-child(3)>form>div:nth-child(1) input");
		Searchtext                =  By.cssSelector(".select2-results>li:nth-child(1)>div>span");

		Resultcard                =  By.cssSelector("[class='modal-body']");
		//removeicon              =  By.cssSelector("[class='locations editable-list']>div>li:nth-child(5) a");
		updatebtn                 =  By.xpath(".//*[@id='locations-modal']/div[4]/button[1]");
		Savescenariobtn           =  By.xpath(".//*[@id='save-scenario']");
		Savethispricedjob         =  By.xpath("(.//*[@id='pricing-save-modal']/div[1]/h3)[5]");
		scenarioname              =  By.cssSelector("div.modal-scrollable >div>div:nth-child(2)>input");
		city                      =  By.cssSelector("div.modal-scrollable >div>div:nth-child(2)>ul.locations>div>div:nth-child(1) input");
		Employeename              =  By.cssSelector("div.modal-scrollable >div>div:nth-child(2)>ul.roster.user-list>div>li:nth-child(2) span.checkbox>input");
		//Savebtn                   =  By.xpath("(//input[@type='submit'])[11]");
		Savebtn                   =  By.cssSelector("div.modal-scrollable > #pricing-save-modal > div.modal-footer > input.btn.btn-primary");
		pricingscenario           =  By.xpath("(//div[@id='pricing-save-modal']/div[2]/div/div)[9]");
		closeButton               =  By.cssSelector("div.modal-scrollable > #pricing-save-modal > div.modal-footer > input.btn.btn.destroy");
		Salarysimulator           =  By.xpath("(//img[@alt='Salary Simulator'])[1]");
		TotalRemuneration         =  By.xpath("(//input[@name='package'])[1]");
		TotalCashatTarget         =  By.xpath("(//input[@name='package'])[2]");
		 BaseSalary               =  By.xpath("(//input[@name='package'])[3]");
		 Calculatebutton          =  By.xpath("//button[text()='Calculate']");
		 Salarysimulatorpopup     =  By.xpath(".//*[@id='individual-scenario']/div[1]/h3");
		 popupsavebutton          =  By.xpath("//button[text()='Save']");
		 salaryscenarioname       =  By.xpath("//input[@name='scenario-name']");
		 searchemployeename       =  By.xpath(".//*[@id='select2-drop']/div/input");
		 salarysavebutton         =  By.xpath("//button[text()='Save']");
		 Closebutton              =  By.xpath(".//a[text()='Close']");
		 Home                     =  By.xpath("(//button[@class='btn btn-icon btn-primary'])[1]");
		 Sucesstext               =  By.xpath("//a[text()='Success']");
		 MyPeople                 =  By.xpath(".//*[@id='my-people']");
		 MyPeopleEmployeename     =  By.xpath(".//*[@id='sidebar']/div/ul[2]/li/ul/li[2]");
		 Enteremployeename        =  By.xpath("//span[contains(text(),'Enter employee name')]");
	     Employeesalarysimulations    =  By.xpath("//label[text()='Salary Simulations']");
	 	lblScenarioNames =   By.xpath("//div[@class='pricing-scenario-list']/ul/li"); 
	 	//lblsalarysimulation = By.xpath("//div[@class='individual-scenario-list']/ul/li");
	 	lblsalarysimulation=By.xpath("//label[contains(text(),'Salary Simulations')]/following-sibling::div/ul/li");

	}

	public static By removeLocation(String locationName){
		removeicon = By.xpath("//li[@class='location'][contains(text(),'"+locationName+"')]/a");

		return removeicon;


	}



}