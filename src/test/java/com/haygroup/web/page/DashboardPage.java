package com.haygroup.web.page;

import org.openqa.selenium.By;

public class DashboardPage {

	public static By btnSelectCompany;
	public static By headerSelectAccount;
	public static By ddlSelectACompany;
	public static By selectAccount;
	public static By navigationTabs;
	public static By headerPeopleAdmin;
	public static By ddl_SelectAnAction;
	public static By addPeopleOptions;
	public static By headerManualAdd;
	public static By userFirstName;
	public static By userLastName;
	public static By emailAddress;
	public static By btn_Save_userDetails;
	public static By btn_AppSettings;
	public static By radioBtnOn_StyleAndClimate;
	public static By radioBtnOn_Journey;
	public static By radioBtnOn_JobDescription;
	public static By radioBtnOn_JobMapping;
	public static By btn_ApplySettings;
	public static By journeyDate;
	public static By headerAppSettings;
	public static By journeyDateTextBox;
	public static By headerProfile;
	public static By userProfileName;
	public static By btn_UserEdit;
	public static By ddl_TimeZone;
	public static By text_TimeZone;
	public static By btn_Done;
	public static By btn_Admin_Add;
	public static By headerSelectEmployee;
	public static By selectEmployee;
	public static By btn_Submit;
	public static By closeAdminBtn;
	public static By btn_Yes;
	public static By headerConfirmation;
	public static By txt_EmployeeName;
	public static By todayDate;
	public static By profieHeader;
	/* Dash board 003 Test Objects */
	public static By companyDownloadReport;
	public static By stylesAndClimateDownloadReport;
	public static By journeyDownloadHrReport;
	/* Dash board 002 Test Objects */


	public static By chkboxPrivacyPolicy;
	public static By btn_AcceptandContinue;
	public static By homeicon;
	public static By btn_PopupYes;





	/* Page Objects Of Styles and Climate */
	static {
		btnSelectCompany 			= By.cssSelector("a.btn.btn-default.ng-binding");
		headerSelectAccount         = By.cssSelector("h4.modal-title.ng-binding");
		ddlSelectACompany           = By.id("select2-chosen-2");
		selectAccount               = By.cssSelector("#select2-results-2>li:nth-child(2)>div");
		headerPeopleAdmin         = By.cssSelector(".panel-heading.admin-headers.ng-binding");
		//ddl_SelectAnAction         = By.xpath("//span[@class='select2-chosen'][contains(text(),'Select an Action')]/following-sibling::span[@class='select2-arrow']/b");
		ddl_SelectAnAction         = By.cssSelector("#s2id_actionSelect>a>span.select2-arrow");
		addPeopleOptions         = By.xpath("//div[@class='select2-result-label'][contains(.,'Manual Add')]");
		headerManualAdd          = By.cssSelector(".modal-header");
		userFirstName         = By.name("firstname");
		userLastName         = By.name("lastname");
		emailAddress         = By.name("email");
		btn_Save_userDetails        = By.xpath("//button[@class='btn btn-primary ng-binding'][contains(.,'Save')]");
		//btn_AppSettings         = By.cssSelector(".column.column-app-settings.pull-right.align-right.not-bulk");
		btn_AppSettings         = By.xpath("//a[contains(text(),'App Settings')]");
		radioBtnOn_StyleAndClimate         = By.xpath("(//input[@name='stylesAndClimateOn'])[1]");
		radioBtnOn_Journey         = By.xpath("(//input[@name='App_17'])[1]");
		radioBtnOn_JobDescription        = By.xpath("(//input[@name='App_18'])[1]");
		radioBtnOn_JobMapping        = By.xpath("(//input[@name='App_19'])[1]");
		btn_ApplySettings        = By.cssSelector(".btn.btn-primary.ng-binding:nth-child(2)");
		journeyDate         = By.cssSelector(".ui-state-default.ui-state-highlight.ui-state-active.ui-state-hover");
		headerAppSettings         = By.cssSelector(".modal-header>h4");
		journeyDateTextBox         = By.xpath("//div[@class='col-sm-6 align-right ng-binding'][contains(text(),'Journey')]/following-sibling::div[@class='dates']/div[@class='col-sm-6 align-left']/input");
		todayDate =  By.cssSelector(".ui-state-default.ui-state-highlight");
		headerProfile         = By.xpath("//h2[contains(text(),'Profile')]");
		userProfileName        = By.cssSelector(".panel-body>h3.ng-binding");
		btn_UserEdit        = By.cssSelector(".user-edit>a:nth-child(1)");
		ddl_TimeZone        = By.xpath("//span[contains(text(),'Search Time Zones')]");
		/*text_TimeZone        = By.xpath("//div[@class='select2-search']/input[@id='s2id_autogen16_search']");*/
		text_TimeZone        = By.xpath("//div[contains(text(),'US/Eastern')]");
		btn_Done            =By.cssSelector(".user-edit>a:nth-child(2)");
		btn_Admin_Add            =By.xpath("//a[@ng-show='ableToAddAndDeleteAdmin']");
		headerSelectEmployee   = By.xpath("//div[@class='modal-header']/h4");
		txt_EmployeeName   = By.xpath("//li[@class='select2-search-field']/input");
		selectEmployee   = By.cssSelector(".select2-result-label>span");
		btn_Submit   = By.xpath("//button[contains(text(),'Submit')]");
		closeAdminBtn   = By.xpath("(//img[@src='i/closeButton.png'])[2]");
		btn_Yes   = By.xpath("//button[contains(text(),'Yes')]");
		headerConfirmation   = By.xpath("//div[@class='modal-header']/h4");
		profieHeader = By.xpath("//div[@class='panel-body']/h3");

		/* Dash board 003 Test Objects */
		companyDownloadReport = By.cssSelector(".changeAccount input:nth-child(1)");
		stylesAndClimateDownloadReport = By.cssSelector(".well.app-unit>div>a");
		journeyDownloadHrReport = By.cssSelector("div.content>a:nth-child(5)");
		/* Dash board 002 Test Objects */



		chkboxPrivacyPolicy = By.id("privacyPolicyCheck");
		btn_AcceptandContinue = By.id("privacyPolicyAccept");
		homeicon = By.xpath("(//h2[contains(text(),'Hay Group')]/following-sibling::div/button)[1]");
		btn_PopupYes = By.xpath("//button[text()='Yes']") ;




	}

	public static By dashBoardTabs(String tabs){


		/*String navigationTabXpath = "//div[@class='row ng-scope']/nav/ul/li/a[contains(text(),'" + tabs + "')]";*/
		String navigationTabXpath = "//a[contains(text(),'"+tabs+"')]";

		navigationTabs = By.xpath(navigationTabXpath);

		return navigationTabs;
	}
}
