package com.haygroup.web.page;

import org.openqa.selenium.By;

public class CreateJobDescriptionPage {

	public static By createNew;
	public static By jobDescriptionTitle;
	public static By listOfJobRoles;
	public static By searchJobRoleTextBox;
	public static By selectJobRole;
	public static By sliderPopUp;
	public static By sliderPopUpHeader;
	public static By sliderHandle;
	public static By doneButton;
	public static By responsibilityHeader;
	public static By UdgestLevelUp;
	public static By UdgestLevelDown;
	public static By saveChangesCheckMark;
	public static By jobName;
	public static By editJobName;
	public static By jobNameTextBox;
	public static By saveJobName;
	public static By aboutTheCompanyTextBox;
	public static By aboutSaveButton;
	public static By jobPurposeTextBox;
	public static By jobPurposeSaveButton;
	public static By confirmationModelPopUp;
	public static By yesButtonOnModelPopup;
	public static By buttonNameFromMenu;
	public static By publishedJobTitles;
	public static By editJobMenu;
	public static By selectJobOption;
	public static By searchJobDescriptionTextBox;
	public static By savedOnLabel;
	public static By publishedJobTitle;
	public static By jobDesignFeedBack;
	
	/* Page Objects Of Job description Page */
	static {
		createNew 					= By.xpath(".//button[contains(text(), 'Create New')]");
		jobDescriptionTitle 		= By.xpath("//div[contains(text(),'Job Description')]");
		searchJobRoleTextBox 		= By.xpath(".//div[contains(text(), 'Search')]/preceding::input");
		sliderPopUp 				= By.xpath(".//div[contains(@class, 'modal-content')]");
		sliderPopUpHeader 			= By.xpath(".//div[contains(@class, 'modal-content')]//h4[2]");
		doneButton 					= By.xpath(".//button[contains(text(), 'Done')]");
		responsibilityHeader 		= By.xpath(".//*[@id='changeRespString']");
		saveChangesCheckMark 		= By.xpath(".//span[contains(@title,'Save')]");
		editJobName 				= By.xpath(".//div[contains(text(), 'Back')]/parent::div//a[contains(text(), 'edit')]");
		jobNameTextBox 				= By.xpath(".//div[contains(text(), 'Back')]/parent::div//input");
		saveJobName 				= By.xpath(".//div[contains(text(), 'Back')]/parent::div//a[contains(text(), 'save')]");
		aboutTheCompanyTextBox 		= By.xpath(".//*[@id='aboutTextArea']");
		aboutSaveButton 			= By.xpath(".//*[@id='aboutTextArea']/following-sibling::span[contains(text(), 'save')]");
		jobPurposeTextBox 			= By.xpath(".//*[@id='purposeTextArea']");
		jobPurposeSaveButton 		= By.xpath(".//*[@id='purposeTextArea']/following-sibling::span[contains(text(), 'save')]");
		confirmationModelPopUp 		= By.xpath(".//div[@class ='modal-content']");
		yesButtonOnModelPopup 		= By.xpath(".//div[@class ='modal-content']//button[contains(text(),'Yes')]");
		publishedJobTitles 			= By.xpath(".//*[@id='superDashboardInfinityDiv']//h3");
		editJobMenu 				= By.xpath(".//h3[contains(text(),'Manager Public')]/..//button");
		searchJobDescriptionTextBox = By.xpath(".//input[@ng-model='searchString']");
		savedOnLabel 				= By.xpath("//span[contains(text(),'Saved on')]");
		jobDesignFeedBack 			= By.xpath(".//*[@id='includeAdjusters']//p[contains(text(),'Very good')]");
	}

	/* Select Job Category from the list */
	public static By selectJobCategory(String jobCategory) {
		String Xpath = ".//li[contains(text(),'" + jobCategory + "')]";
		listOfJobRoles = By.xpath(Xpath);
		return listOfJobRoles;
	}

	/* Select Job from the list */
	public static By selectJobRole(String jobRole) {
		String Xpath = ".//span[contains(text(),'" + jobRole + "')]";
		selectJobRole = By.xpath(Xpath);
		return selectJobRole;
	}

	/*
	 * Increase Responsibilities & Work Characteristics based on responsibility
	 * name
	 */
	public static By responsibilityLevelUp(String responsibilityName) {
		String Xpath = "(.//span[contains(text(),'" + responsibilityName
				+ "')]/ancestor::div[contains(@id,'responsibilitySetter')]//span[contains(@class,'arrow element')])[1]";
		UdgestLevelUp = By.xpath(Xpath);
		return UdgestLevelUp;
	}

	/*
	 * Decrease Responsibilities & Work Characteristics based on responsibility
	 * name
	 */
	public static By responsibilityLevelDown(String responsibilityName) {
		String Xpath = "(.//span[contains(text(),'" + responsibilityName
				+ "')]/ancestor::div[contains(@id,'responsibilitySetter')]//span[contains(@class,'arrow element')])[2]";
		UdgestLevelDown = By.xpath(Xpath);
		return UdgestLevelDown;
	}

	/* Expand Headers based on their Names */
	public static By clickResponsibility(String responsibilityName) {
		String Xpath = ".//div[contains(text(), '" + responsibilityName + "')]";
		UdgestLevelUp = By.xpath(Xpath);
		return UdgestLevelUp;
	}

	/* Slide the slider based on their names */
	public static By sliderToObtainStartingPoint(String pointerName) {
		String Xpath = ".//h4[contains(text(),'" + pointerName
				+ "')]/ancestor::div[contains(@class,'assessments')]//div[contains(@class,'slider-track')]";
		sliderHandle = By.xpath(Xpath);
		return sliderHandle;
	}

	/* Update Job Name Menu */
	public static By selectJobMenuOptions(String jobName) {
		String Xpath = ".//*[@id='superDashboardInfinityDiv']//li[contains(text(),'" + jobName + "')]";
		selectJobOption = By.xpath(Xpath);
		return selectJobOption;
	}

	public static By getupdatedJobName(String updatedJobName) {
		String Xpath = ".//div[contains(text(), 'Back')]/parent::div//span[contains(text(),'" + updatedJobName + "')]";
		jobName = By.xpath(Xpath);
		return jobName;
	}

	/* Select Button From Header (Save as Draft/Publish) */
	public static By selectButtonFromHeader(String buttonName) {
		String Xpath = ".//a[contains(text(), '" + buttonName + "')]";
		buttonNameFromMenu = By.xpath(Xpath);
		return buttonNameFromMenu;
	}

	/* Published Job Name */
	public static By publishedJobName(String jobName) {
		String Xpath = ".//*[@id='superDashboardInfinityDiv']//h3[contains(text(),'" + jobName + "')]";
		publishedJobTitle = By.xpath(Xpath);
		return publishedJobTitle;
	}
}