package com.haygroup.web.page;

import org.openqa.selenium.By;

public class LoginPage {

	public static By userName;
	public static By password;
	public static By loginBtn;

	public static By userName_mobile;
	public static By password_mobile;
	public static By loginBtn_mobile;
	
	public static By JM_username;
	public static By JM_password;
	public static By JM_LoginBut;
	
	public static By newusername;
	public static By newpassword;
	public static By newloginBtn;
	
	
	public static By logOut_mobile;

	/* Page Objects Of Login Page */
	static {
		userName 	= By.xpath("//input[@id='username']");
		password 	= By.xpath("//input[@id='password']");
		loginBtn 	= By.xpath("//button[@type='submit']");
		userName_mobile = By.id("com.haygroup.activate:id/txtUsername");
		password_mobile 	= By.id("com.haygroup.activate:id/txtPassword");
		loginBtn_mobile 	= By.id("com.haygroup.activate:id/btnSignIn");
		JM_username = By.xpath("//input[1]");
		JM_password = By.xpath("//input[2]");
		JM_LoginBut=By.xpath("//input[3]");
		newusername=By.xpath("//form/div[1]/input");
		newpassword=By.id("password");
		newloginBtn 	= By.name("submit");
		


	}
}