package com.haygroup.web.page;

import org.openqa.selenium.By;

public class HomePage {

	public static By logOutButton;
	public static By regionPath;
	public static By logOut_mobile;

	/* Page Objects Of Home Page */
	static {
		logOutButton = By.xpath("(.//button[contains(text(),'Log Out')])[1]");
		logOut_mobile=By.xpath("//android.widget.RelativeLayout[11]/android.widget.LinearLayout/android.widget.TextView");
	}

	/* Select Region from list */
	public static By selectregion(String region) {
		String Xpath = ".//*[@id='hg-apps-list-region']//h2[contains(text(),'" + region + "')]";
		regionPath = By.xpath(Xpath);
		return regionPath;
	}

}