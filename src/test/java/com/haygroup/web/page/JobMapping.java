package com.haygroup.web.page;

import org.openqa.selenium.By;

public class JobMapping {
	
	public static By Upload;
	public static By UploadFile;
	public static By NextButton;
	public static By Mapfunctions;
	public static By SaveChanges;
	public static By DefineIndustry;
	public static By ManageSearch;
	public static By SelectSearch;
	public static By saveserch;
	public static By Guideme;
	public static By Regionalsales;
	public static By Continue;
	public static By jobManager1;
	public static By destJobManager1;
	public static By RegionalsalesManager2;
	public static By destRegionalJobManager2;
	public static By typeCareer;
	public static By optCareer;
	public static By ContinueButton;
	public static By NextButton_Tooltip;
	public static By NextButton5_Tooltip;
	public static By Careerpath_Radio;
	public static By HeadAcc1;
	public static By dest_HeadAcc1;
	public static By dest_keyAccadmin2;
	public static By  coordinator;
	public static By keyAccountAdmin1;
	public static By dest_keyAccountAdmin1;
	public static By ManagerSales2;
	public static By dest_Managersales2;
	public static By PeerTestContinue;
	public static By Yes_Radio;
	public static By No_Radio;
	public static By Peers_Next;
	public static By But_Illtake;
	public static By Tick1;
	public static By Tick2;
	public static By AccManager;
	public static By destAccManager;
	public static By AcctList;
	public static By optAccList;
	public static By ArrowCareerpath;
	public static By Addcareerpath;
	public static By Deletecareer;
	public static By Deltecareeroption;
	public static By Button_HayGrades;
	public static By But_Haycontinue;
	public static By Radio_AnchorJob;
	public static By Next_Anchor;
	public static By Button_OK_Anchor;
	public static By maplist;
	
	
	static{
		Upload 	= By.xpath("//*[@id='hg-jobMapping']/body/ui-view/div/div[2]/div[1]/button");
		//Upload 	= By.xpath("//*[@id='uploadButton']");
		UploadFile=By.xpath("//*[@id='uploadButton']");
		NextButton=By.xpath("//*[@id='hg-jobMapping']/body/ui-view/div/headerbar/div/div[5]/button");
		Mapfunctions=By.xpath("//*[@id='hg-jobMapping']/body/ui-view/div/div[2]/div[3]/button[1]");
		SaveChanges=By.xpath("//button[contains(text(),'Save changes')]");
		DefineIndustry=By.xpath("//span[contains(text(),'Define industry')]");
		ManageSearch= By.xpath("//span[contains(text(),'Search')]");
		SelectSearch=By.xpath("//span[contains(text(),'Banking')]");
		saveserch=By.xpath("//button[contains(text(),'Save changes')]");
		Guideme=By.xpath("//button[contains(text(),'Guide me')]");
		Regionalsales=By.xpath("//h3[contains(text(),'Regional')]");
		Continue=By.xpath("//button[contains(text(),'Continue')]");
		jobManager1=By.xpath("//span[contains(text(),'Manager Regional Sales')]");
		destJobManager1=By.xpath("//*[@id='divContainerLevel10086']");
		typeCareer=By.xpath("//span[3]/b");
		RegionalsalesManager2=By.xpath("//span[contains(text(),'Regional Sales Manager')]");
		destRegionalJobManager2=By.xpath("//*[@id='divContainerLevel10085']");
		optCareer=By.xpath("//span[contains(text(),'Life (ACA)')]");
		ContinueButton=By.xpath("//button[contains(text(),'Continue')]");
		NextButton_Tooltip=By.xpath("//*[@id='verticalModal']/div[3]/button");
		NextButton5_Tooltip=By.xpath("//*[@id='verticalModal']/div[3]/button[2]");
		Careerpath_Radio=By.xpath("//*[@id='no']");
		HeadAcc1 = By.xpath("//span[contains(text(),'Head of Key Accounts')]");
		dest_HeadAcc1=By.xpath("//*[@id='divContainerLevel10106']");
		dest_keyAccadmin2=By.xpath("//*[@id='divContainerLevel10105']");
		coordinator=By.xpath("//span[contains(text(),'Key Account Co-ordinator')]");
		keyAccountAdmin1=By.xpath("//span[contains(text(),'Key Account Administrato')]");
		dest_keyAccountAdmin1=By.xpath("//*[@id='divContainerLevel10126']");
		ManagerSales2=By.xpath("//span[contains(text(),'Manager Sales Analysis')]");
		dest_Managersales2=By.xpath("//*[@id='divContainerLevel10125']");
		PeerTestContinue=By.xpath("//button[contains(text(),'Continue')]");
		Yes_Radio=By.xpath("//*[@id='yes' and @name='peers']");
		Peers_Next=By.xpath("//*[@id='hg-jobMapping']/body/ui-view/div/div/hg-grid-sorter-container/hg-grid-popup-peers/div/div[3]/button");
		No_Radio=By.xpath("//*[@id='no' and @name='peers']");
		But_Illtake=By.xpath("(//div[@class='modal-footer']/button[@ng-click='next()'])[2]");
		Tick1=By.xpath("//div[1]/hg-grid-popup-matrix/div[1]/div[3]/span");
		Tick2=By.xpath("//div[1]/hg-grid-popup-matrix/div[2]/div[3]/span");
		AccManager=By.xpath("//span[contains(text(),'Accounting Manager')][1]");
		destAccManager=By.xpath("//*[@id='divContainerLevel10145']");
		AcctList=By.xpath("//li[4]/ul/li/div[2]/div/a");
		optAccList=By.xpath("//*[@id='ui-select-choices-row-29-0']/div/div");
		ArrowCareerpath=By.xpath("//span[contains(text(),'Accounts')]/parent::div/following-sibling::div/button/span");
		Addcareerpath=By.xpath("//*[@id='hg-jobMapping']/body/ui-view/div/div/hg-grid-sorter-container/div[1]/div[1]/ul/li[4]/div[2]/ul/li[1]/a");
		Deletecareer=By.xpath("//*[@id='hg-jobMapping']/body/ui-view/div/div/hg-grid-sorter-container/div[1]/div[1]/ul/li[4]/ul/li[1]/span[4]/div/ul/li[5]/a");
		Deltecareeroption=By.xpath(" (//*[@class='dropdown-menu']//*[text()='Delete career path'])[4]");
		Button_HayGrades=By.xpath("//button[contains(@ng-show,'showJobMapIsComplete() && !fullyCompletedJobMapping && matrix')]");
		But_Haycontinue=By.xpath("//button[contains(.,'Continue')]");
		Radio_AnchorJob=By.xpath("//div[3]/hg-grid-popup-anchor-map/div/div[2]/ul/li[2]/label/input");
		Next_Anchor=By.xpath("//*[text()=' Next']");
		Button_OK_Anchor=By.xpath("//*[text()='OK']");
		maplist=By.xpath("//*[@class='unordered large']");
	}


}
