package com.haygroup.web.page;

import org.openqa.selenium.By;

public class JobGradingPage {

	public static By selectNameTextBox;
	public static By createNameLabel;
	public static By newEntry;
	public static By continueButton;
	public static By specifyTheRoleLabel;
	public static By selectJobFamily;
	public static By selectSubFamily;
	public static By selectJobType;
	public static By positionThisJobLabel;
	public static By resultsLabel;
	public static By applyGradeButton;
	public static By applyGradeCheckBox;
	public static By applyGradeButtonInResultsPage;
	public static By applyGradeSuccessMsg;
	public static By closeButton;
	public static By saveGradingScenarioBtn;
	public static By saveGradeCheckBox;
	public static By saveGradingScenarioButton;
	public static By homeButton;
	public static By employeeNameInMyPeople;
	public static By sliderHandle;
	public static By jobGradeInResultsPage;
	public static By jobGradeInMyPeople;
	public static By gradingScenarios;

	static {

		selectNameTextBox = By.cssSelector("#s2id_select2-job input");
		createNameLabel = By.cssSelector("#grading-header>h1:nth-child(1)");
		newEntry = By.cssSelector(".select2-result-label");
		continueButton = By.id("continue-button");
		specifyTheRoleLabel = By.cssSelector("#grading-header>h1:nth-child(2)");
		selectJobFamily = By.cssSelector("#job-select-family-region>ul>li:nth-child(2)");
		selectSubFamily = By.cssSelector("#job-select-sub-family-region>ul>li:nth-child(2)");
		selectJobType = By.cssSelector("#job-select-type-region>ul>li:nth-child(2)");
		positionThisJobLabel = By.cssSelector("#grading-header>h1:nth-child(3)");
		resultsLabel = By.cssSelector("#grading-header>h1:nth-child(4)");
		applyGradeButton = By.id("apply-button");
		applyGradeCheckBox = By.cssSelector("#apply-grade-modal-body>ul>li:nth-child(1) span.checkbox>input");
		applyGradeButtonInResultsPage = By.cssSelector(".btn.btn-large.btn-primary.btn-default.apply");
		applyGradeSuccessMsg = By.cssSelector(".alert.alert-success");
		closeButton = By.xpath("//button[text()='Close']");
		saveGradingScenarioBtn = By.id("save-button");
		saveGradeCheckBox = By.cssSelector("#save-grade-modal-body>ul>li:nth-child(1) span.checkbox>input");
		saveGradingScenarioButton = By.cssSelector(".btn.btn-large.btn-primary.btn-default.save");
		homeButton = By.cssSelector(".btn-group.pull-left>button>span.icon-home");
		employeeNameInMyPeople = By.cssSelector(".members.nav-section>li:nth-child(1)");
		jobGradeInResultsPage = By.xpath("(//div[@class='job-grade']/h1)[1]");
		jobGradeInMyPeople = By.xpath("//div[@class='job-grade']/h1");
		gradingScenarios = By.xpath("//label[text()='Grading Scenarios']/following-sibling::div/ul/li");

	}

	public static By sliderToObtainStartingPoint(String pointerName) {
		String Xpath = "//h4[contains(text(),'" + pointerName
				+ "')]/following-sibling::div//div[contains(@class,'slider-track')]";
		sliderHandle = By.xpath(Xpath);
		return sliderHandle;
	}
}
