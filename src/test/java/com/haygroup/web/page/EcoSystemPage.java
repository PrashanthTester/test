package com.haygroup.web.page;

import org.openqa.selenium.By;

public class EcoSystemPage {

	public static By settingsbutton;
	public static By languagechange;
	public static By contactbutton;
	public static By checkbox;
	public static By message;
	public static By sendbutton;
	public static By alert;
	public static By locationdropdown;
	public static By locationchange;
	public static By locationchangeagain;
	public static By successfulltext;
	public static By successfulltext2;
	public static By currencydropdown;
	public static By timezonedropdown;
	public static By sendnotificationradio;
	public static By gradingpreferencesradio;
	public static By Myplanradio;
	public static By weekstartingdaydropdown;
	public static By weekenddaydropdown;
	public static By teammeetingday;
	public static By teammeetingtime;
	public static By donotsendnotificationradio;
	public static By Alwaysaskthegrade;
	public static By Alwaysassumeidontknow;
	public static By Alwaysassumeiknow;
	public static By dailybasisnotification;
	public static By weekbasisnotification;
	public static By Donotsendnotification;
	public static By mobileSettingsButton;
	public static By backButton;
	public static By overLayMenu;
	public static By selectPreferedLanguage;
	public static By okBtn;
	public static By languageIcon;
	public static By closebutton;
	public static By myprofile;
	public static By mailnotification;
	public static By verifylanguageChange;
	public static By Myplanpreferencetext;




	static {

		settingsbutton=By.xpath("(//span[@class='icon icon-cog'])[1]");
		languagechange=By.cssSelector(".language-select>div>div>select");
		contactbutton = By.id("admin-contact");
		checkbox = By.cssSelector("#contact-admin-modal-body>ul>div:nth-child(1) input");
		message = By.id("info");
		sendbutton = By.xpath("//button[text()='Send']");
		alert = By.xpath("//div[@id='bootstrap-alert-admin-contact']");
		verifylanguageChange=By.xpath("//button[@class='btn btn-primary pull-right']");
		locationdropdown = By.cssSelector(".select2-container");
		locationchange=By.cssSelector("#select2-drop>ul>li:nth-child(3)");
		locationchangeagain=By.cssSelector("#select2-drop>ul>li:nth-child(2)");		
		successfulltext=By.cssSelector("#location-region>div:nth-child(1)");
		currencydropdown = By.cssSelector(".currency-select>div>div>select");
		timezonedropdown=By.cssSelector(".timezone-select>div>div>select");
		mailnotification=By.xpath("//h3[@class='title' and text()='Email Notification Preferences']");
		sendnotificationradio=By.cssSelector("#notification-preferences-region>div>ul>li:nth-child(1)>label>input");
		donotsendnotificationradio=By.cssSelector("#notification-preferences-region>div>ul>li:nth-child(2)>label>input");
		Alwaysaskthegrade=By.cssSelector("#grading-preferences-region>div>ul>:nth-child(1)>label>input");
		Alwaysassumeidontknow=By.cssSelector("#grading-preferences-region>div>ul>:nth-child(2)>label>input");
		Alwaysassumeiknow=By.cssSelector("#grading-preferences-region>div>ul>:nth-child(3)");
		dailybasisnotification=By.cssSelector("#my-plan-region>div>div>:nth-child(1)>ul>:nth-child(1)>label>input");
		weekbasisnotification=By.cssSelector("#my-plan-region>div>div>:nth-child(1)>ul>:nth-child(2)>label>input");
		Donotsendnotification=By.cssSelector("#my-plan-region>div>div>:nth-child(1)>ul>:nth-child(3)>label>input");
		weekstartingdaydropdown=By.cssSelector(".js-week-start-region>div>div>select");
		weekenddaydropdown=By.cssSelector(".js-week-end-region>div>div>select");
		teammeetingday=By.cssSelector(".js-team-meeting-day-region>div>div>select");
		teammeetingtime=By.cssSelector(".js-team-meeting-time-region>div>div>select");
		mobileSettingsButton=By.xpath("//android.widget.RelativeLayout[6]/android.widget.LinearLayout/android.widget.TextView");
		backButton = By.xpath("//android.widget.ImageButton[contains(@content-desc,'Navigate up')]");
		overLayMenu=By.className("android.widget.ImageButton");
		selectPreferedLanguage=By.xpath("//android:id/title[contains[@text='User Preferred Language']]");
		//selectPreferedLanguage = By.name("User Preferred Language");
		okBtn = By.id("android:id/button1");
		closebutton=By.xpath("//button[@class='close js-close-modal']");
		myprofile=By.cssSelector("#main-region>div>h3:nth-child(1)");
		Myplanpreferencetext=By.cssSelector("#my-plan-region>div>:nth-child(2)>div>h3");


	}
	public static By verifySuccessMessage(int msgCounter){

		By successElement = By.xpath("(//div[contains(text(),'Successfully Changed')])["+msgCounter+"]");
		return successElement;

	}
	public static By inputLanguage(String language){

		return languageIcon = By.name(language);
	}

}
