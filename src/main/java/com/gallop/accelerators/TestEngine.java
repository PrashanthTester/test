package com.gallop.accelerators;

import java.awt.Robot;
import java.util.Set;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.gallop.report.CReporter;
import com.gallop.report.ExtentReportBuilder;
import com.gallop.report.ReporterConstants;
import com.gallop.support.CustomException;
import com.gallop.support.ExcelReader;
import com.gallop.support.IFrameworkConstant;
import com.gallop.utilities.Xls_Reader;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.win32.W32APIOptions;

import io.appium.java_client.AppiumDriver;
//For Mobile
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

/**
 * 
 * @author in01518
 *
 */
/**
 * 
 * @author in01518
 *
 */
public class TestEngine implements WebDriverEventListener{
	private long intLoggerCurrLogLineNo = 0;
	public final Logger LOG = Logger.getLogger(TestEngine.class);
	protected AppiumDriver appiumDriver = null;
	private AppiumDriver prevAppiumDriver=null;
	protected WebDriver WebDriver = null;
	public EventFiringWebDriver Driver=null;
	private EventFiringWebDriver prevDriver=null;

	protected CReporter reporter = null;

	/*cloud platform*/
	public String localBaseUrl = null;
	public String cloudBaseUrl = null;
	public String userName = null;
	public String accessKey = null;
	public String cloudImplicitWait = null;
	public String cloudPageLoadTimeOut = null;
	public String updateJira = null;
	public String buildNumber = "";
	public String jobName = "";
	public String executedFrom = null; 
	public String executionType = null;
	public String suiteExecution = null;
	public String suiteStartTime = null;

	protected TestParameters testParameters = null;
	private TestParameters prevTestParameters=null;

	public static long startTime;
	//public Boolean isExecutingOnMobileNativeApp = null;
	public String applicationType = "";

	public static String fileName = System.getProperty("user.dir")+"/TestData/TestData.xls";
	public static ExcelReader xlsrdr= new ExcelReader(fileName);
	private LoggingPreferences logPref = new LoggingPreferences();
	private String consoleErrorsPrevious="";
	public Boolean errorsFound=false;
	private static ExtentReportBuilder extentReport = null;
	private static ExtentTest extentTestParent = null;
	public static ExtentTest extentTestLogger = null;
	//public String remoteURL=""; 
	public Xls_Reader TestData = new Xls_Reader(System.getProperty("user.dir") + "/TestData/TestData.xlsx");
	@Parameters({"executionType","suiteExecuted"})

	/*Load the data from property file*/
	@BeforeSuite(alwaysRun=true)
	public void beforeSuite(ITestContext ctx,String type,String suiteName) throws Throwable{
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT)){					
			String extentReporterResultsFolderPath=System.getProperty("user.dir") + File.separator + IFrameworkConstant.RESULTS_FOLDER + File.separator + ctx.getSuite().getName() + ".html";		
			this.extentReport = new ExtentReportBuilder();
			this.extentReport.beforeSuite(ctx.getSuite().getName(), extentReporterResultsFolderPath);
		}
		executionType=type;
		suiteExecution=suiteName;
		System.out.println("Project path is "+System.getProperty("user.dir"));
		System.out.println("Log 4j path is "+System.getProperty("user.dir")+"//Log.properties");
		//PropertyConfigurator.configure(System.getProperty("user.dir")+"\\Log.properties");
		PropertyConfigurator.configure(System.getProperty("user.dir")+"//Log.properties");
		startTime = System.currentTimeMillis();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
		String formattedDate = sdf.format(date);		
		suiteStartTime = formattedDate.replace(":","_").replace(" ","_");
		System.out.println("Suite time ==============>"+suiteStartTime);

	}

	@BeforeClass(alwaysRun=true)
	@Parameters({"automationName","applicationType","executeon","browser","version","platformName", "remoteurl", "deviceName", "android_appPackage", "android_appActivity", "mobile_app_type", "android_apkPath", "ios_UDID", "ios_BundleID", "ios_ipa_Path"})
	public void beforeClass(String automationName, String applicationType, @Optional("local")String executeon, String browser, String version,String platformName, @Optional("")String remoteurl, @Optional("")String deviceName, @Optional("")String android_appPackage, @Optional("")String android_appActivity,@Optional("")String mobile_app_type, @Optional("")String android_apkPath, @Optional("")String ios_UDID, @Optional("") String ios_BundleID, @Optional("") String ios_ipa_Path) throws Exception
	{

		System.out.println(getClass().getName());
		/*get configuration */
		this.localBaseUrl = IFrameworkConstant.APPLICATION_URL;

		this.userName = ReporterConstants.SAUCELAB_USERNAME;
		this.accessKey = ReporterConstants.SAUCELAB_ACCESSKEY;
		this.executedFrom = System.getenv("COMPUTERNAME");
		this.cloudImplicitWait = ReporterConstants.CLOUD_IMPLICIT_WAIT;
		this.cloudPageLoadTimeOut = ReporterConstants.CLOUD_PAGELOAD_TIMEOUT;
		this.updateJira = "";

		testParameters = new TestParameters();
		testParameters.applicationType = applicationType;
		testParameters.browser = browser;
		testParameters.platform = platformName;
		testParameters.deviceName=deviceName;
		testParameters.version = version;
		testParameters.executeon = executeon;
		testParameters.remoteURL=remoteurl;

		testParameters.android_appActivity = android_appActivity;
		testParameters.android_appPackage = android_appPackage;
		testParameters.android_apkPath = android_apkPath;
		testParameters.mobile_app_type = mobile_app_type;
		testParameters.iOS_UDID = ios_UDID;
		testParameters.iOS_BundleID = ios_BundleID;
		testParameters.iOS_IPA_Path = ios_ipa_Path;

		System.out.println(executeon);

		reporter = CReporter.getCReporter(browser, version, platformName, executeon, true);

		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT)){
			String currDesc="";
			currDesc = this.getClass().getName().substring(this.getClass().getName().lastIndexOf(".")+1) + "_" + platformName + "_" + browser + "_" + version + "_" + deviceName + "_" + executeon;
			extentTestParent = this.extentReport.beforeClass(currDesc);
			currDesc="";
			currDesc = "<span><b> Class : " + getClass().getName();
			currDesc = currDesc + ", Platform :" + platformName ;
			if(browser.length()>0)
			{
				currDesc = currDesc + "<br>Browser :" + browser;
			}
			currDesc = currDesc + ", Version:" + version;
			if(deviceName.length()>0)
			{				
				currDesc = currDesc + "<br>DeviceName :" + deviceName;
			}
			currDesc = currDesc + ", Executing On:" + executeon ;
			if(this.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP)==false)
			{
				currDesc = currDesc + "<br>Application :" + IFrameworkConstant.APPLICATION_URL;
			}
			if(this.testParameters.remoteURL.length()>0)
			{
				currDesc = currDesc + "<br>Remote URL:" + this.testParameters.remoteURL; 
			}
			currDesc = currDesc + "<b></span>";
			extentTestParent.info(currDesc);
		}
		else if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT))
		{
			reporter.calculateSuiteStartTime();
		}

		//testMobileChrome();
		createDriverInstance(testParameters, IFrameworkConstant.APPLICATION_URL);
	}

	@Parameters({"browser"})
	@AfterClass(alwaysRun=true)
	/*Quit Browser after test case*/
	public void afterClass(String browser) throws Exception
	{
		/*if (browser.equalsIgnoreCase("firefox")) {
			this.Driver.quit();
			LOG.info("Driver quit ::" + browser);

		} else if (browser.equalsIgnoreCase("chrome")) {
			this.Driver.quit();
			LOG.info("Driver quit ::" + browser);
		} else if (browser.equalsIgnoreCase("ie")) {
			this.Driver.quit();
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("taskkill /F /IM MicrosoftWebDriver.exe");
			LOG.info("Driver quit ::" + browser);
		} else if (browser.equalsIgnoreCase("edge")) {
			this.Driver.quit();
			Runtime.getRuntime().exec("taskkill /F /IM MicrosoftWebDriver.exe");
			LOG.info("Driver quit ::" + browser);
		} else {
			try {
				this.Driver.quit();
				LOG.info("Driver quit ::" + browser);
			} catch (Exception e) {
			}
		}*/
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
			this.reporter.calculateSuiteExecutionTime();
			this.reporter.createHtmlSummaryReport(IFrameworkConstant.APPLICATION_URL,true);
			this.reporter.closeSummaryReport();
		}
	}
	/*Initiate Reporter*/
	@BeforeMethod (alwaysRun = true)
	public void beforeMethod(Method method ) throws Exception
	{
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT)){
			this.extentTestLogger = extentReport.beforeMethod(method);			
		}
		this.reporter.initTestCase(this.getClass().getName().substring(0,this.getClass().getName().lastIndexOf(".")), method.getName(), null, true, extentTestLogger);
	}

	/*Send update status to report*/
	@AfterMethod (alwaysRun = true)
	public void afterMethod(Method method, ITestResult result) throws IOException
	{

		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT))
		{
			if (this.errorsFound==true)
			{
				this.extentTestLogger.fail("Failed");//To add your defects ''<span><a target='_blank' href='https://www.mkyong.com/unittest/testng-tutorial-2-expected-exception-test/'</a>PRASANNA Please click this</span><br>
			}
			this.extentReport.afterMethod(result);
		}		

		System.out.println("Current Method:" + method.getName());
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
			this.reporter.calculateTestCaseExecutionTime();		
			this.reporter.closeDetailedReport();	
			this.reporter.updateTestCaseStatus();
		}
		//this.Driver.close();
		if (this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS)) {
			this.appiumDriver.quit();
		}else{
			this.Driver.quit();	
		}

	}

	/*Set browser configuration for local environment*/
	private void setDriverForLocal(TestParameters curr_TestParameters) throws Exception
	{
		if (curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.DESKTOP_BROWSER) || curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MAC_BROWSER) || curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_BROWSER))
		{
			if (curr_TestParameters.browser.equalsIgnoreCase("firefox")){
				//Thread.sleep(10000);
				try{
					//Added new code
					final FirefoxProfile firefoxProfile = new FirefoxProfile();

					//System.out.println(System.getProperty("user.dir") + "\\Download");



					firefoxProfile.setPreference("browser.download.folderList", 2);

					// firefoxProfile.setPreference("browser.download.dir", System.getProperty("user.dir") + "\\Download");

					firefoxProfile.setPreference("browser.helperApps.neverAsk.openFile","application/pdf,application/download,application/octet-stream");

					firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/octet-stream");

					firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/octet-stream,"

                                                     + "application/pdf,application/download");

					//firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","text/html;charset=utf-8");

					firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);

					firefoxProfile.setPreference("pdfjs.disabled", true);



					//firefoxProfile.setPreference("xpinstall.signatures.required", false);

					DesiredCapabilities capab = DesiredCapabilities.firefox();

					capab.setCapability(FirefoxDriver.PROFILE, firefoxProfile);

					//ended new code


					String FirefoxdriverPath = System.getProperty("user.dir")+"\\Drivers\\";
					//File file = new File("Drivers\\geckodriver.exe");
					System.setProperty("webdriver.gecko.driver", FirefoxdriverPath
							+ "geckodriver.exe");
					//System.setProperty("webdriver.gecko.driver","Drivers\\geckodriver.exe");
					capab.setCapability("marionette", true);
					capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					//capab.setCapability("acceptInsecureCerts", "true");
					capab.setJavascriptEnabled(true);
					//For Setting Browser Logs to capabilities 				
					setConsoleLoggerSetting(capab);
					this.WebDriver = new FirefoxDriver(capab);
				}
				catch(Exception err)
				{
					System.out.println("Error: " + err.getMessage());
					throw err;
				}
				//Thread.sleep(5000);
			}
			else if (curr_TestParameters.browser.equalsIgnoreCase("ie")){
				//Thread.sleep(10000);
				DesiredCapabilities capab = DesiredCapabilities.internetExplorer();
				capab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
				DesiredCapabilities.internetExplorer().setCapability("ignoreProtectedModeSettings", true);
				File file = new File("Drivers\\IEDriverServer.exe");
				System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
				capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capab.setJavascriptEnabled(true);	
				capab.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				
				capab.setCapability("enablePersistentHover", false);
				//Not working for IE
				//For Setting Browser Logs to capabilities 				
				//setConsoleLoggerSetting(capab);
				this.WebDriver = new InternetExplorerDriver(capab);
				
				Thread.sleep(8000);
			}
			else if (curr_TestParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.CHROME)){
				System.out.println("Chrome Browser Launch");
				DesiredCapabilities capab = DesiredCapabilities.chrome();				
				Thread.sleep(2000);
				if(curr_TestParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.ANDROID) && curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_BROWSER) )
				{
					/*                    ChromeOptions options = new ChromeOptions();
					options.addArguments("--disable-notifications");
					options.addArguments("--disable-web-security");
				    options.addArguments("--no-proxy-server");
				    Map<String, Object> prefs = new HashMap<String, Object>();
				    prefs.put("credentials_enable_service", false);
				    prefs.put("profile.password_manager_enabled", false);
				    options.setExperimentalOption("prefs", prefs);
				    capab.setCapability(ChromeOptions.CAPABILITY, options);*/
					/*
                    objChromeOptions.addArguments("androidPackage", "com.android.chrome");
                    capab.setCapability(ChromeOptions.CAPABILITY, objChromeOptions);
                    capab.setCapability("device", "Android");
                    capab.setCapability("browserName", "chrome");
                    capab.setCapability("deviceName", "Samsung");
                    capab.setCapability("platformName", "Android");
                    capab.setCapability("platformVersion", "6.0.1");
                    System.setProperty("webdriver.chrome.driver", "H:\\KornFerry\\HayGroup\\Drivers\\chromedriver.exe");*/
				}
				else
				{					
					System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");					
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--disable-notifications");
					options.addArguments("--disable-web-security");
					options.addArguments("--dns-prefetch-disable"); //Disable ads
					//Do not delete below commented code
					//capab.setCapability("chrome.switches", Arrays.asList("--load-extension=H:\\adblock\\1.13.2_0.crx")); //Blocks Ads in the browser					
					//options.addExtensions(new File("H:\\adblock\\1.13.2_0.crx")); //Blocks Ads in the browser
					options.addArguments("--no-proxy-server");
					Map<String, Object> prefs = new HashMap<String, Object>();
					prefs.put("credentials_enable_service", false);
					prefs.put("profile.password_manager_enabled", false);
					prefs.put("profile.default_content_setting_values.automatic_downloads", 1);
					//Turns off download prompt
					prefs.put("download.prompt_for_download", false);
					options.setExperimentalOption("prefs", prefs);

					options.addArguments("test-type");
					options.addArguments("chrome.switches","--disable-extensions");
					capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					capab.setCapability(ChromeOptions.CAPABILITY, options);

					//For Capturing Browser Console logs 				
					setConsoleLoggerSetting(capab);
				}
				this.WebDriver = new ChromeDriver(capab);
				Thread.sleep(5000);
			}
			else if (curr_TestParameters.browser.equalsIgnoreCase("Safari")){

				for(int i=1;i<=10;i++){
					try{

						DesiredCapabilities capab = DesiredCapabilities.safari();
						//For Capturing Browser Console logs					
						setConsoleLoggerSetting(capab);
						this.WebDriver=new SafariDriver(capab);					
					}catch(Exception e1){
						Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
						Thread.sleep(3000);
						Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
						Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe"); 

						continue;   
					}
				}
			}
		}
	}
	/*Set Browser environment for Grid*/
	private void setDriverForRemote(TestParameters curr_TestParameters) throws Exception
	{
		DesiredCapabilities capab = null;

		if (curr_TestParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.ANDROID))
		{
			capab = new DesiredCapabilities().android();
			if(curr_TestParameters.browser.length()>0)
			{
				capab.setCapability(MobileCapabilityType.BROWSER_NAME, curr_TestParameters.browser);
			}

			if(curr_TestParameters.android_appPackage.length()>0)
			{
				capab.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, curr_TestParameters.android_appPackage);
			}
			if(curr_TestParameters.android_appActivity.length()>0)
			{
				capab.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, curr_TestParameters.android_appActivity);
			}
			if(curr_TestParameters.android_apkPath.length()>0)
			{
				//File classpathRoot = new File(System.getProperty("user.dir")) ;
				File app = new File("APKs\\" + File.separatorChar + curr_TestParameters.android_apkPath);				
				capab.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());	
			}

			if(curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_BROWSER))
			{
				File file = new File("Drivers\\chromedriver.exe");
				capab.setCapability("chromedriverExecutable", file.getAbsolutePath());

				//capab.setCapability(MobileCapabilityType.LAUNCH_TIMEOUT,"300000");
				capab.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,"5000");
			}
			else
			{
				capab.setCapability("unicodekeyboard", true);
				capab.setCapability("resetkeyboard", true);
			}
			//

			//
			capab.setCapability(MobileCapabilityType.PLATFORM,"ANDROID");//Platform.ANDROID);
			capab.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
			capab.setCapability(MobileCapabilityType.PLATFORM_VERSION, curr_TestParameters.version);
			//Newly added for executing on grid
			capab.setCapability(MobileCapabilityType.VERSION, curr_TestParameters.version);
			capab.setCapability(MobileCapabilityType.DEVICE_NAME,curr_TestParameters.deviceName);

			if(curr_TestParameters.iOS_UDID.length() > 0) //This will be used only for real devices
			{
				capab.setCapability(MobileCapabilityType.UDID, curr_TestParameters.iOS_UDID);
			}
			//this.WebDriver = new RemoteWebDriver(new URL(this.testParameters.remoteURL),capab);
			this.WebDriver = new AndroidDriver<AndroidElement>(new URL(this.testParameters.remoteURL),capab);
			Thread.sleep(5000);
			this.appiumDriver = (AndroidDriver<AndroidElement>) this.WebDriver;
			if(this.testParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP))
			{
				try
				{
					this.appiumDriver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
				}catch(Exception ex)
				{
					System.out.println("Unable to set android device to Error: " + ex.getMessage());
				}
			}

			/*				this.WebDriver.findElement(By.xpath("//android.widget.Button[@text='9']")).click();
				this.WebDriver.findElement(By.xpath("//android.widget.Button[@text='8']")).click();
			 */								
			//System.out.println(this.appiumDriver.findElement(By.id("com.android.calculator2:id/formula")).getText());
			//this.appiumDriver.findElement(By.xpath("//android.support.v4.view.ViewPager[@index='2']/android.widget.LinearLayout[@index='0']/android.widget.LinearLayout[@index='0']/android.widget.Button[@index='1']")).click();

			//ad.test(appiumDriver);
		}
		else if(curr_TestParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS))
		{
			capab = new DesiredCapabilities();
			//File appDir = new File(System.getProperty("user.dir"), "apps");

			//File app = new File(appDir, "TestApp.app");
			//capab.setCapability(MobileCapabilityType.BROWSER_NAME, browsername);	
			//capab.setCapability(MobileCapabilityType.APP_PACKAGE, appPackage);
			//capab.setCapability(MobileCapabilityType.APP_ACTIVITY, "");
			if(curr_TestParameters.iOS_IPA_Path.length()>0)
			{
				capab.setCapability(MobileCapabilityType.APP, curr_TestParameters.iOS_IPA_Path);
			}
			if(curr_TestParameters.iOS_UDID.length() > 0) //This will be used only for real devices
			{
				capab.setCapability(MobileCapabilityType.UDID, curr_TestParameters.iOS_UDID);
			}
			capab.setCapability(MobileCapabilityType.PLATFORM_NAME, curr_TestParameters.platform);
			capab.setCapability(MobileCapabilityType.PLATFORM_VERSION, curr_TestParameters.version);
			capab.setCapability(MobileCapabilityType.DEVICE_NAME, curr_TestParameters.deviceName);
			capab.setCapability("unicodekeyboard", true);
			capab.setCapability("resetkeyboard", true);
			capab.setCapability("noReset", true);
			capab.setCapability("fullReset", false);
			appiumDriver = new IOSDriver(new URL(this.testParameters.remoteURL),capab);
			appiumDriver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
			//reporter = CReporter.getCReporter(deviceName, platformName,platformVersion, false);

		}
		else if (curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.DESKTOP_BROWSER) || curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MAC_BROWSER) || curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_BROWSER))
		{
			if (curr_TestParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.FIREFOX))
			{
				Thread.sleep(10000);
				try{
					capab = DesiredCapabilities.firefox();
					File file = new File("Drivers\\geckodriver.exe");
					System.setProperty("webdriver.gecko.driver",file.getAbsolutePath());
					capab.setCapability("marionette", false);
					capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					capab.setCapability("acceptInsecureCerts", "true");
					capab.setJavascriptEnabled(true);
					this.WebDriver = new RemoteWebDriver(new URL(this.testParameters.remoteURL), capab);
				}
				catch(Exception ex)
				{
					System.out.println("Error: " + ex.getMessage());					
					throw ex;
				}
				Thread.sleep(5000);
			}
			else if (curr_TestParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.IE)){
				Thread.sleep(10000);
				capab = DesiredCapabilities.internetExplorer();
				capab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
				DesiredCapabilities.internetExplorer().setCapability("ignoreProtectedModeSettings", true);
				File file = new File("Drivers\\IE64Bit\\IEDriverServer.exe");
				System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
				capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capab.setJavascriptEnabled(true);
				capab.setCapability("enablePersistentHover", false);
				this.WebDriver = new RemoteWebDriver(new URL(this.testParameters.remoteURL), capab);
				Thread.sleep(8000);
			}
			else if (curr_TestParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.CHROME)){
				Thread.sleep(2000);
				System.out.println("Chrome Browser Launch");
				File file = new File("Drivers\\chromedriver.exe");
				System.setProperty("webdriver.chrome.driver",file.getAbsolutePath());
				capab = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
				options.addArguments("chrome.switches","--disable-extensions");
				capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capab.setCapability(ChromeOptions.CAPABILITY, options);
				//capabilities.setCapability("browser_version", "56.0.2924.87");
				try
				{

					this.WebDriver = new RemoteWebDriver(new URL(this.testParameters.remoteURL), capab);
				}catch (Exception ex)
				{
					ex.printStackTrace();
					throw ex;
				}
				Thread.sleep(5000);
			}
			else if (curr_TestParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.SAFARI)){
				for(int i=1;i<=10;i++){

					try{
						this.WebDriver=new SafariDriver();					
					}catch(Exception e1){
						Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
						Thread.sleep(3000);
						Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
						Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe"); 
						continue;   
					}
				}
			}
		}
	}

	/*Set browser environment for sauce labs*/
	private void setRemoteWebDriverForCloudSauceLabs(TestParameters curr_TestParameters) throws Exception
	{
		DesiredCapabilities capa = null;
		if (curr_TestParameters.browser.equalsIgnoreCase("Safari")) {
			capa = new DesiredCapabilities();
			capa.setCapability(CapabilityType.BROWSER_NAME, curr_TestParameters.browser);
			capa.setCapability(CapabilityType.VERSION, curr_TestParameters.version);
			capa.setCapability(CapabilityType.PLATFORM, curr_TestParameters.platform);
			capa.setCapability("username", this.userName);
			capa.setCapability("accessKey", this.accessKey);
			capa.setCapability("name", this.executedFrom + " - " /*+ this.jobName + " - " + this.buildNumber*/+ curr_TestParameters.platform +" - " + curr_TestParameters.browser);
			URL commandExecutorUri = new URL("http://ondemand.saucelabs.com:80/wd/hub");
			for(int i=1;i<=10;i++){
				try{
					this.WebDriver = new RemoteWebDriver(commandExecutorUri, capa);
					break;
				}catch(Exception e1){
					Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
					Thread.sleep(3000);
					Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
					Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe"); 
					continue;   
				}
			}
		}
		else{
			capa = new DesiredCapabilities();
			capa.setCapability(CapabilityType.BROWSER_NAME, curr_TestParameters.browser);
			capa.setCapability(CapabilityType.VERSION, curr_TestParameters.version);
			capa.setCapability(CapabilityType.PLATFORM, curr_TestParameters.platform);
			capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capa.setCapability("username", this.userName);
			capa.setCapability("accessKey", this.accessKey);
			capa.setCapability("accessKey", this.accessKey);
			capa.setCapability("name", this.executedFrom + " - " /*+ this.jobName + " - " + this.buildNumber*/+ curr_TestParameters.platform +" - " + curr_TestParameters.browser);
			capa.setCapability("idleTimeout", 1000);
			capa.setCapability("commandTimeout", 600);

			URL commandExecutorUri = new URL("http://ondemand.saucelabs.com:80/wd/hub");
			this.WebDriver = new RemoteWebDriver(commandExecutorUri, capa);
		}
	}

	/*Set browser environment for Cloud Sauce Labs Jenkins*/
	private void updateConfigurationForCloudSauceLabsJenkins(TestParameters curr_TestParameters) throws Exception
	{
		curr_TestParameters.browser = System.getenv("SELENIUM_BROWSER");
		curr_TestParameters.version = System.getenv("SELENIUM_VERSION");
		curr_TestParameters.platform = System.getenv("SELENIUM_PLATFORM");
		this.userName = System.getenv("SAUCE_USER_NAME");
		this.accessKey = System.getenv("SAUCE_API_KEY");
		this.buildNumber = System.getenv("BUILD_NUMBER");
		this.jobName = System.getenv("JOB_NAME");

		/*For Debug Purpose*/
		LOG.info("Debug: browser = " + curr_TestParameters.browser);
		LOG.info("Debug: version = " + curr_TestParameters.version);
		LOG.info("Debug: platform = " + curr_TestParameters.platform);
		LOG.info("Debug: userName = " + this.userName);
		LOG.info("Debug: accessKey = " + this.accessKey);
		LOG.info("Debug: executedFrom = " + this.executedFrom);
		LOG.info("Debug: BUILD_NUMBER = " + this.buildNumber);
		LOG.info("Debug: jobName = " + this.jobName);
	}

	/*TBD: Not Implemented For Running Using Jenkins*/
	private void updateConfigurationForCloudBrowserStackJenkins()
	{

	}

	public String getBrowser(){
		return  testParameters.browser;
	}

	/*
	 * Setting Capabilities to capture console Logs
	 */
	private void setConsoleLoggerSetting(DesiredCapabilities capab)
	{		
		try{

			this.logPref.enable(LogType.BROWSER, Level.ALL);
			this.logPref.enable(LogType.CLIENT, Level.ALL);
			this.logPref.enable(LogType.DRIVER, Level.ALL);
			this.logPref.enable(LogType.PERFORMANCE, Level.ALL);
			this.logPref.enable(LogType.PROFILER, Level.ALL);
			this.logPref.enable(LogType.SERVER, Level.ALL);
			capab.setCapability(CapabilityType.LOGGING_PREFS, this.logPref);
		}catch(Exception err)
		{
			System.out.println("Error while setting logger settings to capabilities:" + err.getMessage());
		};		
		return;

	}
	@Parameters({"executionType", "suiteExecuted"})
	@AfterSuite(alwaysRun = true)
	/*After Suite Kill all Browsers*/
	public void afterSuite(ITestContext ctx, String type, String suite)
			throws Throwable {
		startTime = System.currentTimeMillis();
		ctx.setAttribute("browser", System.getenv("Browsers"));
		LOG.info("--------------------------------------------------------------------------");
		LOG.info("------------------Suite :: " + suite
				+ "------------------------------");
		LOG.info("AfterSuite Start Time :: " + startTime);
		LOG.info("---------------------------End After Suite Details-----------------------------------");

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
		String formattedDate = sdf.format(date);
		suiteStartTime = formattedDate.replace(":", "_").replace(" ", "_");
		System.out.println("After Suite end time ==============>" + suiteStartTime);
		LOG.info("Before killing "+ testParameters.browser+" browser");
		if (!(this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS))) {
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("taskkill /F /IM MicrosoftWebDriver.exe");	
		}

		LOG.info("After killing "+ testParameters.browser+" browser");
	}

	public void beforeFindBy(By by, WebElement element, WebDriver driver){

		WebElement elem = driver.findElement(by);
		// draw a border around the found element
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor)driver).executeScript("arguments[0].style.border='4px solid yellow'", elem);
			//((JavascriptExecutor)driver).executeScript("arguments[0].setAttribute('style', arguments[1]);", elem, "color: yellow; border: 2px solid yellow;"); 
		}
	}

	public void beforeNavigateTo(String url, WebDriver driver) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateTo(String url, WebDriver driver) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateBack(WebDriver driver) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateBack(WebDriver driver) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateForward(WebDriver driver) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateForward(WebDriver driver) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateRefresh(WebDriver driver) {
		// TODO Auto-generated method stub
		try{
			if(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOGS==true && this.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP)==false){
				this.captureBrowserConsoleLogs();//driver);	
			}	
		}catch(Throwable t)
		{
			t.printStackTrace();
		}
	}

	public void afterNavigateRefresh(WebDriver driver) {

	}


	public void afterFindBy(By by, WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub

	}


	public void beforeClickOn(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub

	}


	public void afterClickOn(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		try{
			if(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOGS==true && this.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP)==false){
				this.captureBrowserConsoleLogs();//driver);	
			}	
		}catch(Throwable t)
		{
			t.printStackTrace();
		}
	}


	public void beforeChangeValueOf(WebElement element, WebDriver driver,
			CharSequence[] keysToSend) {
		// TODO Auto-generated method stub

	}


	public void afterChangeValueOf(WebElement element, WebDriver driver,
			CharSequence[] keysToSend) {

	}


	public void beforeScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub

	}


	public void afterScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub

	}


	public void onException(Throwable throwable, WebDriver driver) {
		// TODO Auto-generated method stub

	}

	/**
	 * Method to capture Browser Console Logs, use this function to capture JScript errors or any other browser console errors/logs
	 * Can be called explicitly in other classes for capturing console logs, 
	 * by default it captures logs when an object is clicked using selenium webdriver or when page
	 * 
	 *  NOTE: Currently this works only for Chrome & Firefox in WINDOWS OS only
	 *  
	 * @param driver
	 * @return
	 * @throws Throwable
	 */
	//public boolean captureBrowserConsoleLogs(WebDriver driver) throws Throwable
	public boolean captureBrowserConsoleLogs() throws Throwable
	{
		if (IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOGS==true) {
			isApplicationReady();
			String consoleErrors = "";
			long currLineNo = 0;
			int totalErrorsCnt = 0;
			Logs logs = this.Driver.manage().logs();

			LogEntries logEntries = logs.get(LogType.BROWSER);  //Use LogType.DRIVER to capture all Selenium WebDriver console logs which will be useful for script developers 
			//FileWriter fw = null;
			try {
				/*writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_FILEPATH), "utf-8"));*/
				//fw = new FileWriter(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_FILEPATH, true);
				//fw.write("Started New Error Captures \r\n");
				for (LogEntry logEntry : logEntries) {
					System.out.println("Errors Captured: " + logEntry.getMessage());
					//fw.write("ERROR:" + logEntry.getMessage() + "\r\n");

					//if (currLineNo >= this.intLoggerCurrLogLineNo){
					String currLogMessage=logEntry.getMessage();
					if(currLogMessage.length()!=0)
					{
						totalErrorsCnt=totalErrorsCnt+1;
						consoleErrors = consoleErrors +  ReporterConstants.HTML_TAG_LINE_BREAK_BEGIN + ReporterConstants.HTML_TAG_BOLD_BEGIN + "ERROR " + totalErrorsCnt + ReporterConstants.HTML_TAG_BOLD_END + " : " + currLogMessage ;
						/*writer.append(consoleErrors);*/							
						System.out.println(consoleErrors);
					}
					//}
					currLineNo++;
				}

			} catch (Exception err) {
				err.printStackTrace();
				throw err;
			} finally {
				try {
					/*writer.close();*/
					//fw.close();
				} catch (Exception err)
				{ 
					throw err;
				}
				this.intLoggerCurrLogLineNo = currLineNo;
				if(consoleErrors!="" && !consoleErrorsPrevious.equals(consoleErrors))
				{
					String currentURL = this.Driver.getCurrentUrl();
					openCloseBrowserConsole(true);					
					if(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_FAIL || !IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_WARN){
						this.reporter.failureReport("BROWSER CONSOLE ERRORS FOUND", "<B>Current URL: </B>" + currentURL + "<br>" + consoleErrors, true, true, this.Driver);
						this.errorsFound=true;
					}
					else if(IFrameworkConstant.CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_WARN)
					{
						this.reporter.warningReport("BROWSER CONSOLE ERRORS FOUND", "<B>Current URL: </B>" + currentURL + "<br>" + consoleErrors);
					}
					openCloseBrowserConsole(false);
					consoleErrorsPrevious=consoleErrors;
				}
			}
		}
		return true;
	}

	/** Opens, Closes Browser Console 
	 * 
	 * NOTE: Currently this works only for Chrome & Firefox only
	 * 
	 * @param openConsole
	 * @throws Throwable
	 */
	private void openCloseBrowserConsole(Boolean openConsole) throws Throwable
	{ 

		Robot robot=new Robot();

		if(this.testParameters.executeon.equalsIgnoreCase(IFrameworkConstant.EXECUTION_ON.LOCAL) && (this.testParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.CHROME) || testParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.FIREFOX))) //If not local we cannot take screenshot for the browser & desktop
			//TO-DO Need to write alternate code to take screenshot on grid/remote/saucelabs
		{
			User32 user32 = User32.instance;

			//BROWSER WINDOW FOCUS CODE BEGIN: Code to focus on browser window before opening console and taking screenshot
			String windowClassName = "";
			String browserTitleLastPart="";
			int keyValue = 0;
			if (this.testParameters.browser.equalsIgnoreCase("ie"))
			{							
				windowClassName = "IEFrame";
				browserTitleLastPart = "Internet Explorer";
				keyValue = KeyEvent.VK_2;				
			}
			else if (this.testParameters.browser.equalsIgnoreCase("chrome"))
			{
				keyValue = KeyEvent.VK_J;
				windowClassName = "Chrome_WidgetWin_1";
				browserTitleLastPart = "Google Chrome";
			}
			else if (this.testParameters.browser.equalsIgnoreCase("firefox"))
			{
				keyValue = KeyEvent.VK_K;
				windowClassName = "MozillaWindowClass";
				browserTitleLastPart = "Mozilla Firefox";						
			}
			/*			HWND hWnd = user32.FindWindow(windowClassName, this.Driver.getTitle().toString() + " - " + browserTitleLastPart);		
	        user32.ShowWindow(hWnd, User32.SW_SHOW);
	        user32.SetForegroundWindow(hWnd);
			 */	        //BROWSER WINDOW FOCUS CODE END:

			if(this.testParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.IE))
			{
				robot.keyPress(KeyEvent.VK_F12);
				robot.keyRelease(KeyEvent.VK_F12);				
			}
			Thread.sleep(IFrameworkConstant.WAIT_TIME_MIN*2);
			System.out.println("Current Browser : " + this.testParameters.browser);

			//Pressing Ctrl+Shift+(J/K)			
			robot.keyPress(KeyEvent.VK_CONTROL);
			if(this.testParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.CHROME) || this.testParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.FIREFOX))
			{
				robot.keyPress(KeyEvent.VK_SHIFT);
			}
			robot.keyPress(keyValue);
			//Releasing Ctrl+Shift+(J/K)
			robot.keyRelease(KeyEvent.VK_CONTROL);
			if(this.testParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.CHROME) || this.testParameters.browser.equalsIgnoreCase(IFrameworkConstant.BROWSER.FIREFOX))
			{
				robot.keyRelease(KeyEvent.VK_SHIFT);
			}
			robot.keyRelease(keyValue);
			if(openConsole==true)
			{
				System.out.println("Browser console is opened");
			}
			else
			{
				System.out.println("Browser console is closed");
			}
			//Driver.manage().window().maximize();
			Thread.sleep(IFrameworkConstant.WAIT_TIME_MIN*2);
		}
	}

	/**
	 * Method to wait for Browser/Mobile application to load Until MAX_PAGE_WAIT_TIME
	 * 'TO-DO: Yet to implement for Mobile Native application
	 * @throws Throwable
	 */
	public void isApplicationReady() throws Throwable{
		Boolean syncCompleted=false;
		if(this.testParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP))
		{
			//TO-DO: Write Sync for Mobile Native App
			return;
		}
		else
		{
			try{
				//long startTime = new Date().getTime();
				int startTime2 = 1;


				while(startTime2 < (IFrameworkConstant.WAIT_TIME_MEDIUM/100)){
					startTime2++;
					if (pageReadyState())
					{
						syncCompleted=true;
						System.out.println("Application Load - Completed");
						break;
					}
					else
					{
						Thread.sleep(500);
					}
				}
			}
			catch(Throwable e){
				System.out.println("Failed in isApplicationReady():" + e.getMessage());
				throw e;
			}
			finally{
				if(syncCompleted==false){
					throw new CustomException("Error in isApplicationReady(): Application Sync time timedout: Waited for " + IFrameworkConstant.WAIT_TIME_MEDIUM/100 + " Seconds");
				}
			}
		}
	}

	/*Verifies for Document Ready State, Active jQueryState and AngularJS Active Queries state*/
	private Boolean pageReadyState() throws Throwable {
		Boolean status = false;
		Boolean documentReadyState = false;
		Boolean jQueryDefined = false;
		Boolean jQueryActive = false;
		Boolean pageLoadCompleted=false;
		Boolean angularPageLoaded=false;
		String angularStatus = "";
		if (this.testParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP) == false){
			angularPageLoaded=true;
			JavascriptExecutor js = Driver;    	
			documentReadyState = js.executeScript("return document.readyState").toString().equals("complete");
			if (documentReadyState==false)
			{
				System.out.println("document.readState is not 'complete' - Still loading");
				//return false;
				status = false;
			}
			else{
				status = true;
			}
			jQueryDefined = (Boolean) js.executeScript("return typeof jQuery != 'undefined'");
			if(jQueryDefined==true)
			{
				jQueryActive = (Boolean) js.executeScript("return !!window.jQuery==true && window.jQuery.active == 0");
				if(jQueryActive==false) 
				{
					System.out.println("jQuery still loading");
					//return false;
					status = false;
				}
				else{
					status = true;
				}
			}
			//Get AngularJS page availability by checking the ng-app webelement ID availability 
			WebElement angularElement = (WebElement) js.executeScript("return document.getElementById('ng-app')");
			if (angularElement != null)
			{
				//Execute JSScript to get the AngularJS web element availability
				angularPageLoaded = (Boolean) js.executeScript("return (window.angular !== undefined) && (angular.element(document).injector() !== undefined) && (angular.element(document).injector().get('$http').pendingRequests.length === 0)");
				if(angularPageLoaded==false) 
				{
					System.out.println("Angular JS still loading");
					//return false;
					status = false;
				}
				else{
					status = true;
				}
			}	    	
		}
		return status;
	}

	@AfterTest
	public void afterTest() throws Throwable {
		//To fail the test case
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertTrue(!this.errorsFound);
		softAssert.assertAll();
	}

	//Need to verify if it works in other OS
	public interface User32 extends W32APIOptions {

		User32 instance = (User32) Native.loadLibrary("user32", User32.class, DEFAULT_OPTIONS);

		// interface WNDENUMPROC extends StdCallCallback {
		//      boolean callback(Pointer hWnd, Pointer arg);
		//  }

		// boolean EnumWindows(WNDENUMPROC lpEnumFunc, Pointer arg);

		boolean ShowWindow(HWND hWnd, int nCmdShow);
		boolean SetForegroundWindow(HWND hWnd);
		HWND FindWindow(String winClass, String title);

		int SW_SHOW = 1;

	}

	private String getBrowserVersion() {
		String browser_version="";
		String browsername = "";

		Capabilities cap = ((RemoteWebDriver) this.WebDriver).getCapabilities();
		browsername = cap.getBrowserName();
		// This block to find out IE Version number
		if ("internet explorer".equalsIgnoreCase(browsername)) {
			/*String uAgent = (String) ((JavascriptExecutor) this.Driver).executeScript("return navigator.userAgent;");
			System.out.println(uAgent);
			//uAgent return as "MSIE 8.0 Windows" for IE8
			if (uAgent.contains("MSIE") && uAgent.contains("Windows")) {
				browser_version = uAgent.substring(uAgent.indexOf("MSIE")+5, uAgent.indexOf("Windows")-2);
			} else if (uAgent.contains("Trident/7.0")) {
				browser_version = "11.0";
			} else {
				browser_version = "0.0";
			}*/
			browser_version = testParameters.version;
		} else
		{
			//Browser version for Firefox and Chrome
			browser_version = cap.getVersion();
		}
		//browser_version = browser_version.substring(0,browser_version.indexOf("."));
		return browser_version;
	}


	public class getCurrentBrowserDetails{
		public String OS;
		public String browserName;
		public String browserMajorVersion;
		public String browserMinorVersion;
		public Boolean mobile;
		public String flash;
		public Boolean cookies;
		public String screenSize;
		public String userAgent;
		/**
		 * <b>Description : </b> Get Current Browser Details and assigns those details as below:
		 * <br> HOST: OS - Operating System, browserName, browserMajorVersion, browserMinorVersion, mobile, flash, cookies, screenSize & userAgent
		 * <br> All the above details will be assigned to public variables which can be used with the object created  
		 * <br> 
		 * <p>
		 * <b>Example : </b>
		 *  		getCurrentBrowserDetails currBrowser = new getCurrentBrowserDetails(this.Driver);
		 * <br>	System.out.println(currBrowser.browserName);
		 * <br>	System.out.println(currBrowser.browserMajorVersion);
		 * <br>	System.out.println(currBrowser.browserMinorVersion);
		 * <br>	System.out.println(currBrowser.OS);
		 * <br>	System.out.println(currBrowser.mobile);
		 * <br>
		 * @author Gallop/Cigniti
		 * @param currDriver
		 * @throws Exception
		 */
		public getCurrentBrowserDetails(EventFiringWebDriver currDriver) throws Exception
		{
			String jsOutPut="";
			String jsCommand = "";
			try
			{

				JavascriptExecutor executor = currDriver;	
				jsCommand = "return (function (window) {    " 
						+ "{        var unknown = '-';"
						+ "var screenSize = '';"
						+ "if (screen.width){"
						+ "	width = (screen.width) ? screen.width : '';"
						+ "	height = (screen.height) ? screen.height : '';"
						+ "	screenSize += '' + width + \" x \" + height;"
						+ "}"
						+ "var nVer = navigator.appVersion;"
						+ "var nAgt = navigator.userAgent;"
						+ "var browser = navigator.appName;"
						+ "var version = '' + parseFloat(navigator.appVersion);"
						+ "var majorVersion = parseInt(navigator.appVersion, 10);"
						+ "var nameOffset, verOffset, ix;"
						+ "if ((verOffset = nAgt.indexOf('Opera')) != -1) {"
						+ "	browser = 'Opera';"
						+ "	version = nAgt.substring(verOffset + 6);"
						+ "if ((verOffset = nAgt.indexOf('Version')) != -1) {"
						+ "	version = nAgt.substring(verOffset + 8);"
						+ "}"
						+ "}"
						+ "if ((verOffset = nAgt.indexOf('OPR')) != -1) {"
						+ "browser = 'Opera';"
						+ "version = nAgt.substring(verOffset + 4);"
						+ "}"
						+ "else if ((verOffset = nAgt.indexOf('Edge')) != -1) {"
						+ "browser = 'Microsoft Edge';"
						+ "version = nAgt.substring(verOffset + 5);"
						+ "}"
						+ "else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {"
						+ ""
						+ "browser = 'Microsoft Internet Explorer';"
						+ "version = nAgt.substring(verOffset + 5);"
						+ "}"
						+ "else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {"
						+ ""
						+ "browser = 'Chrome';"
						+ "version = nAgt.substring(verOffset + 7);"
						+ "}"
						+ "else if ((verOffset = nAgt.indexOf('Safari')) != -1) {"
						+ "browser = 'Safari';"
						+ "version = nAgt.substring(verOffset + 7);"
						+ "if ((verOffset = nAgt.indexOf('Version')) != -1) {"
						+ ""
						+ "version = nAgt.substring(verOffset + 8);"
						+ "}"
						+ "}"
						+ "else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {"
						+ ""
						+ "browser = 'Firefox';"
						+ "version = nAgt.substring(verOffset + 8);"
						+ "}"
						+ "else if (nAgt.indexOf('Trident/') != -1) {"
						+ "browser = 'Microsoft Internet Explorer';"
						+ "version = nAgt.substring(nAgt.indexOf('rv:') + 3);"
						+ "}"
						+ "else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {"
						+ "browser = nAgt.substring(nameOffset, verOffset);"
						+ "version = nAgt.substring(verOffset + 1);"
						+ "if (browser.toLowerCase() == browser.toUpperCase()) {"
						+ "browser = navigator.appName;            "
						+ "}"
						+ "}"
						+ "if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);"
						+ "if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);"
						+ "if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);"
						+ "majorVersion = parseInt('' + version, 10);"
						+ "if (isNaN(majorVersion)) {"
						+ "version = '' + parseFloat(navigator.appVersion);"
						+ "majorVersion = parseInt(navigator.appVersion, 10);"
						+ "}"
						+ "var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);"
						+ "var cookieEnabled = (navigator.cookieEnabled) ? true : false;"
						+ "if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {"
						+ "document.cookie = 'testcookie';"
						+ "cookieEnabled = (document.cookie.indexOf('testcookie') != -1) ? true : false;"
						+ "}"
						+ "var os = unknown;"
						+ "var clientStrings = [{s:'Windows 10', r:/(Windows 10.0|Windows NT 10.0)/},"
						+ "{s:'Windows 8.1', r:/(Windows 8.1|Windows NT 6.3)/},"
						+ "{s:'Windows 8', r:/(Windows 8|Windows NT 6.2)/},"
						+ "{s:'Windows 7', r:/(Windows 7|Windows NT 6.1)/},"
						+ "{s:'Windows Vista', r:/Windows NT 6.0/},"
						+ "{s:'Windows Server 2003', r:/Windows NT 5.2/},"
						+ "{s:'Windows XP', r:/(Windows NT 5.1|Windows XP)/},"
						+ "{s:'Windows 2000', r:/(Windows NT 5.0|Windows 2000)/},"
						+ "{s:'Windows ME', r:/(Win 9x 4.90|Windows ME)/},"
						+ "{s:'Windows 98', r:/(Windows 98|Win98)/},"
						+ "{s:'Windows 95', r:/(Windows 95|Win95|Windows_95)/},"
						+ "{s:'Windows NT 4.0', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},"
						+ "{s:'Windows CE', r:/Windows CE/},"
						+ "{s:'Windows 3.11', r:/Win16/},"
						+ "{s:'Android', r:/Android/},"
						+ "{s:'Open BSD', r:/OpenBSD/},"
						+ "{s:'Sun OS', r:/SunOS/},"
						+ "{s:'Linux', r:/(Linux|X11)/},"
						+ "{s:'iOS', r:/(iPhone|iPad|iPod)/},"
						+ "{s:'Mac OS X', r:/Mac OS X/},"
						+ "{s:'Mac OS', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},"
						+ "{s:'QNX', r:/QNX/},"
						+ "{s:'UNIX', r:/UNIX/},"
						+ "{s:'BeOS', r:/BeOS/},"
						+ "{s:'OS/2', r:/OS\\/2/},"
						+ "{s:'Search Bot', r:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\\/Teoma|ia_archiver)/}];"
						+ "for (var id in clientStrings) {"
						+ "var cs = clientStrings[id];"
						+ "if (cs.r.test(nAgt)) {"
						+ "os = cs.s;"
						+ "break;"
						+ "}"
						+ "}"
						+ "var osVersion = unknown;"
						+ "if (/Windows/.test(os)) {"
						+ "osVersion = /Windows (.*)/.exec(os)[1];"
						+ "os = 'Windows';"
						+ "}"
						+ "switch (os) {"
						+ "case 'Mac OS X':"
						+ "osVersion = /Mac OS X (10[\\.\\_\\d]+)/.exec(nAgt)[1];"
						+ "break;"
						+ "case 'Android':"
						+ "osVersion = /Android ([\\.\\_\\d]+)/.exec(nAgt)[1];"
						+ "break;"
						+ "case 'iOS':"
						+ "osVersion = /OS (\\d+)_(\\d+)_?(\\d+)?/.exec(nVer);"
						+ "osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);"
						+ "break;"
						+ "}"
						+ "var flashVersion = 'no check';"
						+ "if (typeof swfobject != 'undefined') {"
						+ "var fv = swfobject.getFlashPlayerVersion();"
						+ "if (fv.major > 0) {"
						+ "flashVersion = fv.major + '.' + fv.minor + ' r' + fv.release;"
						+ "}"
						+ "else  {"
						+ "flashVersion = unknown;"
						+ "}"
						+ "}"
						+ "}"
						+ "window.jscd = {"
						+ "screen: screenSize,"
						+ "browser: browser,"
						+ "browserVersion: version,"
						+ "browserMajorVersion: majorVersion, mobile: mobile,"
						+ "os: os,osVersion: osVersion,cookies: cookieEnabled,"
						+ "flashVersion: flashVersion    };"
						+ "return ('OS:' + jscd.os +' '+ jscd.osVersion + '##' "
						+ "+ 'Browser: ' + jscd.browser + '##'"
						+ "+ 'BrowserMajorVersion:' + jscd.browserMajorVersion + '##'"
						+ "+ 'BrowserMinorVersion: Browser (' + jscd.browserVersion + ')##' "
						+ "+ 'Mobile: ' + jscd.mobile + '##' "
						+ "+ 'Flash: ' + jscd.flashVersion + '##' "
						+ "+ 'Cookies: ' + jscd.cookies + '##' "
						+ "+ 'Screen Size: ' + jscd.screen + '##' "
						+ "+ 'Full User Agent: ' + navigator.userAgent);}(this));";
				jsOutPut = (String) executor.executeScript(jsCommand);			
				String[] tokensValues = jsOutPut.split("##");
				this.OS = tokensValues[0].split(":")[1].trim();
				this.browserName= tokensValues[1].split(":")[1].trim();
				this.browserMajorVersion= tokensValues[2].split(":")[1].trim();
				this.browserMinorVersion= tokensValues[3].split(":")[1].trim();
				this.mobile= Boolean.valueOf(tokensValues[4].split(":")[1].trim());
				this.flash= tokensValues[5].split(":")[1].trim();
				this.cookies= Boolean.valueOf(tokensValues[6].split(":")[1].trim());
				this.screenSize= tokensValues[7].split(":")[1].trim();
				this.userAgent= tokensValues[8].split(":")[1].trim();
			}catch(Exception err)
			{
				throw err;
			}
		}

	}

	/**
	 * Method to create a new EventFiringWebDrive instance and appiumDriver instances
	 * both Driver & appiumDriver will get new driver instances
	 * <br>
	 * @param currApplicationType - should be one of the options from IFrameworkConstant.APPLICATION_TYPE
	 * @param currBrowser - for setting browser as declared in iFramework 
	 * @param currVersion - Set Version can be ""
	 * @param currPlatform - is Mandatory
	 * @param currDeviceName - Device Name
	 * @param currDeviceDetails - DeviceDetails Object should be created before for Mobile/devices  
	 * @return
	 * @throws Exception
	 * <br>
	 * <br>Example: For Desktop Web Browser
	 * <br>createDriverInstance("desktopbrowser", chrome", "", "windows 7", "",null, "http://google.com");
	 * <br>
	 * <br>Example: For MAC Web Browser
	 * <br>
	 * <br>createDriverInstance("macbrowser", "firefox", "", "windows 10", "", null, "http://google.com");
	 * <br>
	 * <br>Example: For mobile Browser
	 * <br>
	 * <br>createDriverInstance("mobilebrowser", "chrome", "54", "android", mobileDeviceName, currDeviceDetails, "http://google.com");
	 * <br>
	 * <br>Example: For mobile
	 * <br>
	 * <br>createDriverInstance("mobileapp", "", "5.0", "android", "samsung", currDeviceDetails, "");
	 * <br>
	 * @see         getPreviousWebDriver, setCurrentDriverObject, getPreviousAppiumDriver
	 *
	 */	
	public void createDriverInstance(TestParameters curr_TestParameters, String applicationURL) throws Exception
	{		
		/*		if (currBrowser.equalsIgnoreCase(IFrameworkConstant.BROWSER.IE) || currBrowser.equalsIgnoreCase(IFrameworkConstant.BROWSER.CHROME) || currBrowser.equalsIgnoreCase(IFrameworkConstant.BROWSER.FIREFOX) || currBrowser.equalsIgnoreCase(IFrameworkConstant.BROWSER.SAFARI))
		{
			this.isExecutingOnMobileNativeApp = false;
		}
		else 
		{
			this.isExecutingOnMobileNativeApp = true;
		}*/

		//Set Previous webDriver to Existing Web Driver
		this.prevDriver = this.Driver;
		this.prevAppiumDriver = this.appiumDriver;
		this.prevTestParameters = this.testParameters;
		Driver = null;
		appiumDriver=null;
		/*Set Environment as Local*/
		if(curr_TestParameters.executeon.equalsIgnoreCase(IFrameworkConstant.EXECUTION_ON.LOCAL))
		{
			this.setDriverForLocal(curr_TestParameters);
		}else if(curr_TestParameters.executeon.equalsIgnoreCase(IFrameworkConstant.EXECUTION_ON.GRID))
		{
			/*Set environment as Grid*/
			this.setDriverForRemote(curr_TestParameters);
		}else if(curr_TestParameters.executeon.equalsIgnoreCase("cloudSauceLabs"))
		{
			/*To Set Environment as Sauce labs*/
			this.setRemoteWebDriverForCloudSauceLabs(curr_TestParameters);	           
		}else if(curr_TestParameters.executeon.equalsIgnoreCase("cloudSauceLabsJenkins"))
		{
			this.updateConfigurationForCloudSauceLabsJenkins(curr_TestParameters);
			/*set remoteWebDriver for cloudsaucelabs*/        
			this.setRemoteWebDriverForCloudSauceLabs(curr_TestParameters);	           
		}else if (curr_TestParameters.executeon.equalsIgnoreCase("cloudBrowserStackJenkins"))
		{
			/*TBD: Not Implemented For Running Using Jenkins*/
			this.updateConfigurationForCloudBrowserStackJenkins();
		}
		if (!(this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS))) {
			Driver = new EventFiringWebDriver(this.WebDriver);
		}
		//
		if(curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.DESKTOP_BROWSER))
		{
			this.Driver.register(this);
		}		

		//Driver.manage().timeouts().implicitlyWait(IFrameworkConstant.WAIT_TIME_MAX_PAGE, TimeUnit.MILLISECONDS);
		//BELOW CODE IS YET TO BE UTILIZED FOR REPORTING PURPOSE - SREEKANTH
		if(curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP)==false)
		{			
			/*//MyListener myListener = new MyListener();
			Driver.register(this.myListener);
			 */			
			try{
				if(curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.DESKTOP_BROWSER)
						|| curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MAC_BROWSER))
				{
					Driver.manage().window().maximize();
				}

				/*if(applicationURL.length()>0)
				{
					//this.Driver.get(applicationURL);
				}*/
				curr_TestParameters.version = this.getBrowserVersion();
				/*if(curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.DESKTOP_BROWSER)
						|| curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MAC_BROWSER))
				{
					Driver.manage().window().maximize();
				}*/

				/*getCurrentBrowserDetails currBrowserDetails = new getCurrentBrowserDetails(this.Driver);			
				System.out.println(currBrowserDetails.browserName);
				System.out.println(currBrowserDetails.browserMajorVersion);
				System.out.println(currBrowserDetails.browserMinorVersion);
				System.out.println(currBrowserDetails.OS);
				System.out.println(currBrowserDetails.mobile);*/

			}catch (Exception e)
			{
				e.printStackTrace();
				throw e;
			}
			Thread.sleep(5000);
		}
		//If Hybdir App then set Context
		if(curr_TestParameters.applicationType.equalsIgnoreCase(IFrameworkConstant.APPLICATION_TYPE.MOBILE_APP) &&
				curr_TestParameters.mobile_app_type.equalsIgnoreCase(IFrameworkConstant.MOBILE_APP_TYPE.HYBRID_APP))
		{
			//System.out.println(this.appiumDriver.getContextHandles().getClass());
			Set<String> contextNames = this.appiumDriver.getContextHandles();
			for (String contextName : contextNames) {
				System.out.println(contextName);
			}
			this.appiumDriver.context(contextNames.toArray()[1].toString()); // set context to WEBVIEW_1
		}		
		//return this.Driver;

	}

	/**
	 * Method to set the Objects to a new this.Driver, this.appiumDriver, this.testParameters
	 * <br> this method will also send this.prevDriver, this.prevAppiumDriver, this.testParameters which can be returned using getPreviousInstance 
	 * <br> This will will set EventFiringWebDriver prevDriver with previous EventFiringWebDriver Object and 
	 * <br> set the current EventFiringWebDriver object with the new Object, before using this Object make sure that 
	 * <br> you have all the EventFiringWebDriver object instances and close/quite all the instances as needed in the scripts
	 * <br>
	 * <br>
	 * @param currWebDriver - EventFiringWebDriver should be sent as an object
	 * @param currAppiumDriver - AppiumDriver should be sent as an object
	 * @param currTestParameters - currTestParameters should be sent as TestParameters object
	 * @throws Exception
	 */
	public void setDriverInstance(EventFiringWebDriver currDriver, AppiumDriver currAppiumDriver, TestParameters currTestParameters)
	{
		this.prevDriver = this.Driver;
		this.prevAppiumDriver = this.appiumDriver;
		this.prevTestParameters = this.testParameters;

		this.Driver = currDriver;
		this.appiumDriver = currAppiumDriver;
		this.testParameters = currTestParameters;		
	}

	/**
	 * Method to get the Objects to a prevDriver, prevAppiumDriver, prevTestParameters
	 * <br> After instantiating new instance previous instance will can be used to reset back to old instance as needed
	 * <br>
	 * @param prevWebDriver - EventFiringWebDriver should be sent as an object
	 * @param prevAppiumDriver - AppiumDriver should be sent as an object
	 * @param currTestParameters - TestParameters should be sent as an object
	 * @throws Exception
	 */
	public void getDriverPreviousInstance(EventFiringWebDriver prevDriver, AppiumDriver prevAppiumDriver, TestParameters prevTestParameters)
	{
		prevDriver = this.prevDriver;
		prevAppiumDriver = this.prevAppiumDriver;
		testParameters = this.prevTestParameters;

	}
	/**
	 * Method to set the Current AppiumDriver Object to a different AppiumDriver Object
	 * <br> This will will set AppiumDriver prevDriver with previous AppiumDriver Object and 
	 * <br> set the current AppiumDriver object with the new Object, before using this Object make sure that 
	 * <br> you have all the AppiumDriver object instances and close/quite all the instances as needed in the scripts
	 * <br>
	 * <br>
	 * @param currAppiumDriver - AppiumDriver should be sent as an object
	 * @throws Exception
	 */
	public void setCurrentAppiumDriverObject(AppiumDriver currAppiumDriver)throws Exception
	{
		prevAppiumDriver = this.appiumDriver;
		this.appiumDriver = currAppiumDriver;
	}

	/**
	 * Method to return previous EventFiringWebDriver
	 * <br>After using this Driver to do the necessary actions like click etc., 
	 * <br>it is recommended to use setCurrentWebDriverObject() method
	 * @return EventFiringWebDriver 
	 * @throws Exception
	 */
	public AppiumDriver getPreviousAppiumDriverObject()throws Exception
	{
		return this.prevAppiumDriver;
	}

	//NEED TO UPDATE
	public static void startAppiumServer() throws ExecuteException,
	IOException, InterruptedException {

		CommandLine command = new CommandLine("cmd");
		command.addArgument("/c");
		command.addArgument("C:/Appium/node.exe");
		command.addArgument("C:/Appium/node_modules/appium/bin/appium.js",
				false);
		command.addArgument("--address", false);
		command.addArgument("127.0.0.1");
		command.addArgument("--port", false);
		command.addArgument("4723");
		// command.addArgument("--no-reset");
		// command.addArgument("--no-reset", false);
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		executor.execute(command, resultHandler);

		Thread.sleep(5000);
		System.out.println("Appium server started");

	}

	//NEED TO UPDATE
	public static void stopAppiumServer() throws IOException {
		CommandLine command = new CommandLine("cmd");
		command.addArgument("/c");
		command.addArgument("taskkill");
		command.addArgument("/F");
		command.addArgument("/IM");
		command.addArgument("C:/Appium/node.exe");

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);

		try {
			executor.execute(command, resultHandler);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public class TestParameters
	{
		public String applicationType = "";
		public String browser = "";
		public String platform = "";
		public String deviceName = "";
		public String version = "";
		public String executeon = "";
		public String remoteURL = "";

		//Only for android
		public String android_appActivity = "";	//eg: com.varaha.contacts.activities.DialtactsActivity
		public String android_appPackage = "";	//eg: com.esi_estech.ditto
		/*public String mobile_app_type = IFrameworkConstant.MOBILE_APP_TYPE.NATIVE_APP;*/
		public String mobile_app_type = "";
		public String android_apkPath = "";		//eg: /Users/ctl-user/Documents/mobilty-ditto-release/mobilty-ditto-release_2.0.16.apk

		//Only For iOS
		public String iOS_UDID = "";		//db683f62be2549a89f2f4a0012239a9b947da62e
		public String iOS_IPA_Path = ""	;	//Users/ctl-user/Documents/esiditto/ESIDitto2_1_1_appium.ipa
		public String iOS_BundleID = "";	//com.esi-estech.ditto
	}

	private void testMobileChrome() throws MalformedURLException
	{
		// Create object of  DesiredCapabilities class and specify android platform
		DesiredCapabilities capabilities=DesiredCapabilities.android();


		// set the capability to execute test in chrome browser
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,BrowserType.CHROME);

		// set the capability to execute our test in Android Platform
		capabilities.setCapability(MobileCapabilityType.PLATFORM,Platform.ANDROID);

		// we need to define platform name
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");

		// Set the device name as well (you can give any name)
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"SonyDevice");
		File file = new File("Drivers\\chromedriver.exe");
		capabilities.setCapability("chromedriverExecutable", file.getAbsolutePath());

		capabilities.setCapability("launchTimeout", "100000");

		// set the android version as well 
		capabilities.setCapability(MobileCapabilityType.VERSION,"4.3");

		// Create object of URL class and specify the appium server address
		URL url= new URL("http://127.0.0.1:4723/wd/hub");

		// Create object of  AndroidDriver class and pass the url and capability that we created
		WebDriver driver = new AndroidDriver(url, capabilities);

		// Open url
		driver.get("http://www.facebook.com");

		// print the title
		System.out.println("Title "+driver.getTitle());

		// enter username
		driver.findElement(By.name("email")).sendKeys("Sreekanth@gmail.com");
		driver.close();
		driver.quit();
	}


	public void afterAlertAccept(org.openqa.selenium.WebDriver arg0) {
		// TODO Auto-generated method stub

	}


	public void afterAlertDismiss(org.openqa.selenium.WebDriver arg0) {
		// TODO Auto-generated method stub

	}


	public void beforeAlertAccept(org.openqa.selenium.WebDriver arg0) {
		// TODO Auto-generated method stub

	}


	public void beforeAlertDismiss(org.openqa.selenium.WebDriver arg0) {
		// TODO Auto-generated method stub

	}
}