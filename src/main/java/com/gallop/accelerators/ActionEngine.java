package com.gallop.accelerators;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;

import java.awt.Color;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;

import com.gallop.report.ReporterConstants;
import com.gallop.support.CustomException;
import com.gallop.support.IFrameworkConstant;
import com.gallop.support.IFrameworkConstant.SCROLL_TYPE;
import com.google.common.base.Function;
import com.google.common.base.Predicate;

/* Wrapper classes for selenium web driver*/
public class ActionEngine extends TestEngine {
	private final Logger LOG = Logger.getLogger(ActionEngine.class);

	private final String msgClickSuccess = "Successfully Clicked On ";
	private final String msgClickFailure = "Unable To Click On ";
	private final String msgTypeSuccess = "Successfully Typed On ";
	private final String msgTypeFailure = "Unable To Type On ";
	private final String msgIsElementFoundSuccess = "Successfully Found Element ";
	private final String msgIsElementFoundFailure = "Unable To Found" + " Element ";
	public boolean reportIndicator = true;
	int i;
	//protected TestParameters testParameters = new TestParameters();

	/**
	 * This function performs is To Assert the element of web page
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean assertElementPresent(By by, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Assert.assertTrue(isElementPresent(by, locatorName, true));
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (!flag) {
				reporter.failureReport("AssertElementPresent :: ", locatorName + " present in the page :: ", Driver);
				return false;
			} else if (flag) {
				reporter.SuccessReport("AssertElementPresent :: ", locatorName + " is not present in the page :: ");
			}
		}
		return flag;
	}



	/**
	 * This function performs is to move mouse operation using java script
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean mouseHoverByJavaScript(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement mo = Driver.findElement(locator);
			String javaScript = "var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
					+ "arguments[0].dispatchEvent(evObj);";
			JavascriptExecutor js = (JavascriptExecutor) Driver;
			js.executeScript(javaScript, mo);
			flag = true;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (!flag) {
				reporter.failureReport("MouseOver :: ", "MouseOver action is not perform on :: " + locatorName, Driver);
			} else if (flag) {
				reporter.SuccessReport("MouseOver :: ", "MouserOver Action is Done on :: " + locatorName);
			}
		}
	}

	/*public void javaScriptsElementsToLoad(){
		WebDriverWait wait = new WebDriverWait(Driver, IFrameworkConstant.WAIT_TIME_MEDIUM);
		wait.until( new Predicate<WebDriver>() {
            public boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
            }
        }
    );
	}*/

	public void checkPageIsReady() {

		/*JavascriptExecutor js = (JavascriptExecutor)Driver;


		//Initially bellow given if condition will check ready state of page.
		if (js.executeScript("return document.readyState").toString().equals("complete")){ 
			System.out.println("Page Is loaded.");
			return; 
		} 

		//This loop will rotate for 25 times to check If page Is ready after every 1 second.
		//You can replace your value with 25 If you wants to Increase or decrease wait time.
		for (int i=0; i<25; i++){ 
			try {
				Thread.sleep(1000);
			}catch (InterruptedException e) {} 
			//To check page ready state.
			if (js.executeScript("return document.readyState").toString().equals("complete")){ 
				break; 
			}   
		}*/
	}
	/**
	 * This function wait until element to be visible
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean waitForVisibilityOfElement(By by, String locatorName,int maxWaitTimeinMilliSeconds) throws Throwable {
		int currWaitTime=IFrameworkConstant.WAIT_TIME_MEDIUM;
		if(maxWaitTimeinMilliSeconds>=0)
		{
			currWaitTime=maxWaitTimeinMilliSeconds;
		}
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(Driver, IFrameworkConstant.EXPLICIT_WAIT_TIME);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			flag = true;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				reporter.failureReport("Visible of element is false :: ",
						"Element :: " + locatorName + " is not visible", Driver);
			} else if (flag) {
				reporter.SuccessReport("Visible of element is true :: ", "Element :: " + locatorName + "  is visible");
			}
		}
	}

	/**
	 * This function performs Select drop down using by its value
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean selectByValue(By locator, String value, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByValue(value);
			flag = true;
			LOG.info("Successfully selected the value" + locatorName);
			return true;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (!flag) {
				reporter.failureReport("Select",
						"Option with value attribute : " + value + " is Not Select from the DropDown : " + locatorName,
						Driver);
			} else if (flag) {
				reporter.SuccessReport("Select",
						"Option with value attribute : " + value + " is  Selected from the DropDown : " + locatorName);
			}
		}
	}

	/**
	 * This function performs Select drop down using by its visible text
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean selectByVisibleText(By locator, String visibletext, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(Driver.findElement(locator));
			s.selectByVisibleText(visibletext.trim());
			flag = true;
			return true;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (!flag) {
				reporter.failureReport("Select", visibletext + " is Not Select from the DropDown" + locatorName,
						Driver);
			} else if (flag) {
				reporter.SuccessReport("Select", visibletext + "  is Selected from the DropDown" + locatorName);
			}
		}
	}

	/**
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean selectByIndex(By locator, int index, String locatorName) throws Throwable {
		boolean flag = false;
		try {

			Select s = new Select(Driver.findElement(locator));
			s.selectByIndex(index);
			flag = true;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (!flag) {
				if (reportIndicator == true) {
					reporter.failureReport("Select Value from the Dropdown :: " + locatorName,
							"Option at index :: " + index + " is Not Select from the DropDown :: " + locatorName,
							Driver);
				} else if (flag) {
					reporter.SuccessReport("Select Value from the Dropdown :: " + locatorName,
							"Option at index :: " + index + "is Selected from the DropDown :: " + locatorName);
				}
			}
			reportIndicator = true;
		}
	}

	/**
	 * This function performs Click operation works for both Desktop & Mobile
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean click(By locator, String locatorName) throws Throwable {
		boolean status = false;
		try {
			if (this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS)) {
				isApplicationReady();
				WebDriverWait wait = new WebDriverWait(this.appiumDriver, IFrameworkConstant.EXPLICIT_WAIT_TIME);

				wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
				//wait.until(ExpectedConditions.elementToBeClickable(locator));
				//			Assert.assertTrue(this.Driver.findElement(locator).isDisplayed());
				appiumDriver.findElement(locator).click();
				this.reporter.SuccessReport("Click : " + locatorName, msgClickSuccess + locatorName);
				status = true;
			} else{
				isApplicationReady();
				WebDriverWait wait = new WebDriverWait(this.Driver, IFrameworkConstant.EXPLICIT_WAIT_TIME);

				wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
				//wait.until(ExpectedConditions.elementToBeClickable(locator));
				//			Assert.assertTrue(this.Driver.findElement(locator).isDisplayed());
				Driver.findElement(locator).click();
				this.reporter.SuccessReport("Click : " + locatorName, msgClickSuccess + locatorName);
				status = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			status = false;
			LOG.info(e.getMessage());
			this.reporter.failureReport("Click : " + locatorName, msgClickFailure + locatorName, Driver);
			throw e;
		}
		return status;
	}

	/**
	 * This function to get Size of web elements
	 * 
	 * @return
	 * @throws Exception 
	 * @throws Throwable
	 */
	public int getElementsSize(By locator) throws Exception {
		int a = 0;
		try {
			List<WebElement> rows = Driver.findElements(locator);
			a = rows.size();
		} catch (Exception e) {
			throw e;
		}
		return a;
	}

	/**
	 * This function Sends data to input field
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean type(By locator, String testdata, String locatorName) throws Throwable {
		boolean status = false;
		try {
			if (this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS)) {
				isApplicationReady();
				appiumDriver.findElement(locator).clear();
				appiumDriver.findElement(locator).sendKeys(testdata);
				isApplicationReady();
				reporter.SuccessReport("Enter text in " + locatorName, msgTypeSuccess + locatorName);
				status = true;
			} else{
				Driver.findElement(locator).clear();
				Driver.findElement(locator).sendKeys(testdata);
				isApplicationReady();
				reporter.SuccessReport("Enter text in " + locatorName, msgTypeSuccess + locatorName);
				status = true;
			}
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			reporter.failureReport("Enter text in " + locatorName, msgTypeFailure + locatorName, Driver);
			throw e;
		}
		return status;
	}

	/**
	 * This function waits till element to be click
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean waitTillElementToBeClickble(By by, String locator) throws Throwable {
		boolean flag = false;
		WebDriverWait wait = null;
		try {
			if (this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS)) {
				wait = new WebDriverWait(appiumDriver, IFrameworkConstant.EXPLICIT_WAIT_TIME);
				isApplicationReady();
				wait.until(ExpectedConditions.elementToBeClickable(by));
				reporter.SuccessReport("Enter text in " + locator, msgTypeSuccess + locator);
				flag = true;
				return true;

			} else{
				wait = new WebDriverWait(Driver, IFrameworkConstant.EXPLICIT_WAIT_TIME);
				wait.until(ExpectedConditions.elementToBeClickable(by));
				reporter.SuccessReport("Enter text in " + locator, msgTypeSuccess + locator);
				flag = true;
				return true;
			}
		} catch (Exception e) {

			e.printStackTrace();
			return false;

		} finally {
			if (!flag) {
				reporter.failureReport("elementToBeClickble", "Element " + locator + " is not visible", Driver);

			} else if (flag) {
				reporter.SuccessReport("elementToBeClickble", "Element " + locator + "  is visible");
			}
		}
	}

	public void scrollToWebElement(By locator, String locatorName){
		WebElement element = Driver.findElement(locator);
		try {
			((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView();", element);
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This function use to verify the matching text
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean assertTextMatching(By by, String text, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			//waitForVisibilityOfElement(by,locatorName,30);
			String ActualText = getText(by, text).trim();
			if (ActualText.equalsIgnoreCase(text.trim())) {
				flag = true;
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				reporter.failureReport("Verify " + locatorName, text + " is not present in the element", Driver);
				return false;

			} else if (flag) {
				reporter.SuccessReport("Verify " + locatorName, text + " is  present in the element ");
			}
		}

	}



	/**
	 * This function use to verify the Attribute matching text
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean assertTextMatchingWithAttribute(By by, String text, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			if (this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS)) {
				String ActualText = appiumDriver.findElement(by).getAttribute("value");
				if (ActualText.contains(text.trim())) {
					flag = true;
					return true;
				} else {
					return false;
				}

			} else{
				String ActualText = Driver.findElement(by).getAttribute("value");
				if (ActualText.contains(text.trim())) {
					flag = true;
					return true;
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				reporter.failureReport("Verify : " + locatorName, text + " is not present in the element : ", Driver);
				return false;
			} else if (flag) {
				reporter.SuccessReport("Verify : " + locatorName, text + " is  present in the element : ");
			}
		}
	}

	/**
	 * Moves the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:link,menus etc..)
	 */
	public boolean mouseover(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement mo = this.Driver.findElement(locator);
			new Actions(this.Driver).moveToElement(mo).build().perform();
			flag = true;
			return true;
		} catch (Exception e) {
			// return false;
			throw new RuntimeException(e);
		} finally {
			if (flag == false) {
				this.reporter.failureReport("MouseOver :: ", "MouseOver action is not perform on ::" + locatorName,
						Driver);
			} else if (flag == true) {
				this.reporter.SuccessReport("MouseOver :: ", "MouserOver Action is Done on  :: " + locatorName);
			}
		}
	}

	/**
	 * Moves the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 *
	 * @param destinationLocator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:link,menus etc..)
	 */
	public boolean dragAndDrop(By souceLocator, By destinationLocator, String locatorName) throws Throwable {
		boolean flag = false;
		try {

			Actions builder = new Actions(this.Driver);
			WebElement souceElement = this.Driver.findElement(souceLocator);
			WebElement destinationElement = this.Driver.findElement(destinationLocator);
			org.openqa.selenium.interactions.Action dragAndDrop = builder.clickAndHold(souceElement)
					.moveToElement(destinationElement).release(destinationElement).build();
			dragAndDrop.perform();
			flag = true;
			return true;
		} catch (Exception e) {
			// return false;
			throw new RuntimeException(e);
		} finally {
			if (flag == false) {
				this.reporter.failureReport("DragDrop : ", "Drag and Drop action is not performed on : " + locatorName,
						Driver);
			} else if (flag == true) {
				this.reporter.SuccessReport("DragDrop : ", "Drag and Drop Action is Done on : " + locatorName);
			}
		}
	}

	/**
	 * This function is used to browser navigations on page
	 * 
	 * @param destinationLocator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:link,menus etc..)
	 */

	public boolean navigateTo(String Url) throws Throwable {
		boolean flag = false;
		try {
			Waittime();
			WebDriver.navigate().to(Url);
			LOG.info("Navigated URL is : " + Url);
			flag = true;
			return flag;
		} catch (Exception e) {
			flag = false;
			LOG.info(e.getMessage());
		} finally {
			if (!flag) {
				reporter.failureReport("Unable to Open : ", Url, Driver);
				return false;
			} else if (flag) {
				reporter.SuccessReport("Sucessfully Opened : ", Url);
			}
		}
		return flag;
	}

	public boolean rightClick(By locator, String LocatorName) throws Throwable {
		boolean status;
		try {
			Actions action = new Actions(Driver);
			action.contextClick(Driver.findElement(locator)).build().perform();
			Driver.findElement(locator).click();
			reporter.SuccessReport("Click : " + LocatorName, "Successfully Mouse Right Clicked On " + LocatorName);
			status = true;
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			reporter.failureReport("Click : " + LocatorName, "Unable To Right Click On " + LocatorName, Driver);
			throw new RuntimeException(e);

		}
		return status;
	}

	/**
	 * This function performs dynamic wait
	 * 
	 * @return
	 * @throws Throwable
	 */
	public void DynamicWait(By locator) throws Throwable {
		try {
			String time = ReporterConstants.CLOUD_IMPLICIT_WAIT;
			int timevalue = Integer.parseInt(time);
			WebDriverWait wait = new WebDriverWait(Driver, timevalue);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			LOG.info(locator + ":: displayed succussfully");
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * click the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:link,menus etc..)
	 */
	public boolean mouseClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement mo = this.Driver.findElement(locator);
			new Actions(this.Driver).click(mo).build().perform();
			flag = true;
			return true;
		} catch (Exception e) {
			// return false;
			throw new RuntimeException(e);
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Click :: ", "Click action is not perform on ::" + locatorName, Driver);
			} else if (flag == true) {
				this.reporter.SuccessReport(" Click :: ", " Click Action is Done on  :: " + locatorName);
			}
		}
	}

	/**
	 * This function wait until element present
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean isElementPresent(By by, String locatorName, boolean expected) throws Throwable {
		boolean status = false;
		try {
			isApplicationReady();
			if (this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS)) {
				appiumDriver.findElement(by);
			}else{
				Driver.findElement(by);	
			}

			status = true;
		} catch (Exception e) {
			status = false;
			if (expected == status) {
				reporter.SuccessReport("isElementPresent", "isElementPresent");
			} else {
				reporter.failureReport("isElementPresent", msgIsElementFoundFailure + locatorName, Driver);
			}
		}
		return status;
	}

	/**
	 * @param :
	 *            (int) Number to get year (e.g: -1,0,1 etc)
	 * @return : int
	 * @Behavior: Function to get required year e.g: 0-Current year, 1-Next
	 *            year,
	 */
	public int getYear(int number) throws Throwable {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR) + number;
		LOG.info("Year is : " + year);
		return year;
	}

	/**
	 * @return : boolean
	 * @Behavior: Function to verify date format by giving actual date
	 * @param1 : (String) actual date e.g: 21-11-2015
	 * @param2 : (String) format type e.g: dd-MM-yyyy
	 */
	public boolean dateFormatVerification(String actualDate, String formatToVerify) {
		boolean flag = false;

		if (actualDate.toLowerCase().contains("am")) {
			flag = formatVerify(actualDate, formatToVerify);
		} else if (actualDate.toLowerCase().contains("pm")) {
			flag = formatVerify(actualDate, formatToVerify);
		} else if (!actualDate.toLowerCase().contains("am") || !actualDate.toLowerCase().contains("pm")) {
			flag = formatVerify(actualDate, formatToVerify);
		}
		return flag;
	}

	/**
	 * @return : boolean
	 * @Behavior: Reusable Function to verify date format by giving actual date
	 * @param1 : (String) actual date e.g: 21-11-2015
	 * @param2 : (String) format type e.g: dd-MM-yyyy
	 */
	public boolean formatVerify(String actualDate, String formatToVerify) {
		boolean flag = false;
		try {
			String datePattern = formatToVerify;
			SimpleDateFormat sdf;
			sdf = new SimpleDateFormat(datePattern);
			Date date = sdf.parse(actualDate);
			String formattedDate = sdf.format(date);
			if (actualDate.equals(formattedDate)) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flag;
	}

	/**
	 * @return : String
	 * @throws Exception 
	 * @Behavior: Function to replace the regular expression values with client
	 *            required values
	 * @param1 : (String) Actual value
	 * @param2 : (String) regular expression of actual value
	 * @param3 : (String) value to replace the actual
	 */
	public String replaceAll(String text, String pattern, String replaceWith) throws Exception {
		String flag = null;
		try {
			flag = text.replaceAll(pattern, replaceWith);
		} catch (Exception ex) {
			throw ex;
		}
		return flag;
	}

	/**
	 * @return : String
	 * @throws Exception 
	 * @Behavior: Function to get sub string of given actual string text
	 * @param1 : (String) Actual text
	 * @param2 : (int) Start index of sub string
	 * @param3 : (int) end index of sub string
	 */
	public String subString(String text, int startIndex, int endIndex) throws Exception {
		String flag = null;
		try {
			flag = text.substring(startIndex, endIndex);
		} catch (Exception ex) {
			throw ex;
		}
		return flag;
	}

	/**
	 * @return : String
	 * @Behavior: Function to get the value of a given CSS property (e.g. width)
	 * @param1 : (By) locator
	 * @param2 : (String) CSS property
	 */
	public String getCssValue(By locator, String cssValue) {
		String result = "";
		try {
			result = this.Driver.findElement(locator).getCssValue(cssValue);
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * This function wait until element to be visible
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean scroll(By by, String locatorName) throws Throwable {
		boolean status = false;
		try {
			waitTillElementToBeClickble(by, locatorName);
			WebElement element = this.Driver.findElement(by);
			Actions actions = new Actions(this.Driver);
			actions.moveToElement(element);
			actions.build().perform();
			status = true;
			reporter.SuccessReport("Scroll: " + locatorName, "Successfully Scrolled " + locatorName);
		} catch (Exception e) {
			reporter.failureReport("Scroll: ", msgIsElementFoundFailure + locatorName, Driver);
			throw e;
		}
		return status;
	}

	/**
	 * This function wait until element to be visible
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean Waittime() throws Throwable {
		boolean status = true;

		System.out.println("Time out value is " + IFrameworkConstant.WAIT_TIME_MIN);
		Thread.sleep(IFrameworkConstant.WAIT_TIME_MIN);
		return status;
	}

	/**
	 * This function is to get Text
	 * 
	 * @return
	 * @throws Throwable
	 */

	public String getText(By locator, String locatorName) throws Throwable {
		String text = "";
		boolean flag = false;
		try {
			if (this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS)) {
				//waitForInVisibilityOfElement(locator, locatorName);
				Waittime();
				if (isElementPresent(locator, locatorName, true)) {
					text = appiumDriver.findElement(locator).getText();


				} 
			}
			else{
				waitTillElementToBeClickble(locator, locatorName);
				Waittime();
				if (isElementPresent(locator, locatorName, true)) {
					text = Driver.findElement(locator).getText();
					flag = true;
				}

			} 
		}catch (Exception e) {
			throw e;
		} finally {
			if (flag == false) {
				reporter.warningReport("GetText", "Unable to get Text from" + locatorName);
			} else if (flag == true) {
				reporter.SuccessReport("GetText", "" + locatorName + " is" + text);
			}
		}
		return text;
	}



	/**
	 * This function is to Click using java script
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean JSClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			isApplicationReady();
			waitTillElementToBeClickble(locator, locatorName);
			WebElement element = this.Driver.findElement(locator);
			JavascriptExecutor executor = this.Driver;
			executor.executeScript("arguments[0].click();", element);
			isApplicationReady();
			flag = true;
		} catch (Exception err) {
			err.printStackTrace();
			throw err;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("JSClick", "JSClick Action error" + locatorName, this.Driver);
				return flag;
			} else if (flag == true) {
				this.reporter.SuccessReport("JSClick", "Clicked " + locatorName);
				return flag;
			}
		}
		return flag;
	}

	/**
	 * This function is to perform mouse move using java script
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean JSmousehover(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			waitTillElementToBeClickble(locator, locatorName);
			WebElement HoverElement = this.Driver.findElement(locator);
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			((JavascriptExecutor) this.Driver).executeScript(mouseOverScript, HoverElement);

			flag = true;
		} catch (Exception e) {
		} finally {
			if (flag == false) {
				this.reporter.failureReport("MouseOver", "MouseOver action is not perform on" + locatorName, Driver);
				return flag;
			} else if (flag == true) {
				this.reporter.SuccessReport("MouseOver", "MouserOver Action is Done on" + locatorName);
				return flag;
			}
		}
		return flag;
	}

	/**
	 * This function is used to perform slide
	 * 
	 * @return
	 * @throws Throwable
	 */
	public boolean slider(By locator, String locatorName, int x, int y) throws Throwable {
		boolean status = false;
		try {
			waitTillElementToBeClickble(locator, locatorName);
			WebElement sliderElement = Driver.findElement(locator);
			Actions moveElement = new Actions(Driver);
			new Actions(Driver).clickAndHold(sliderElement).moveByOffset(x, y).release().perform();
			moveElement.build().perform();
			reporter.SuccessReport("Slider: " + locatorName, msgClickSuccess + locatorName);
			status = true;
			System.out.println("Slider moved");
		} catch (Exception e) {
			status = false;
			reporter.failureReport("Click : " + locatorName, msgClickFailure + locatorName, Driver);
			throw e;
		}
		return status;
	}

	/**
	 * This function is useful to wait until element to be visible
	 * 
	 * @return
	 * @throws Throwable
	 */
	public void elementVisibleTime(By locator) throws Throwable {
		boolean flag = false;
		float timeTaken = 0;
		try {

			long start = System.currentTimeMillis();
			WebDriverWait wait = new WebDriverWait(Driver, IFrameworkConstant.EXPLICIT_WAIT_TIME);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			long stop = System.currentTimeMillis();
			timeTaken = (stop - start) / 1000;
			LOG.info("Took : " + timeTaken + " secs to display the results : ");
			reporter.SuccessReport("Total time taken for element visible :: ",
					"Time taken load the element :: " + timeTaken + " seconds");
			flag = true;
		} catch (Exception e) {
			reporter.failureReport("Click : " + locator, msgClickFailure + locator, Driver);
			throw e;
		}
	}

	/**
	 * This function wait until element Not visible
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean waitForInVisibilityOfElement(By by, String locatorName) throws Throwable {
		boolean flag = false;

		WebDriverWait wait = new WebDriverWait(Driver, IFrameworkConstant.EXPLICIT_WAIT_TIME);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			flag = true;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				reporter.failureReport("InVisible of element is false :: ",
						"Element :: " + locatorName + " is not visible", Driver);
			} else if (flag) {
				reporter.SuccessReport("InVisible of element is true :: ",
						"Element :: " + locatorName + "  is visible");
			}
		}
	}

	/**
	 * This function wait until element enabled
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean VerifyElementIsEnabled(By by, String locatorName, boolean expected) throws Throwable {
		boolean status = false;
		try {
			if (this.Driver.findElement(by).isEnabled()) {
				this.reporter.SuccessReport("VerifyElementIsEnabled " + locatorName,
						this.msgIsElementFoundSuccess + locatorName);
				status = true;
			}
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			if (expected == status) {
				this.reporter.SuccessReport("isElementEnabled", "isElementEnabled");
			} else {
				this.reporter.failureReport("isElementEnabled", this.msgIsElementFoundFailure + locatorName, Driver);
			}
		}
		return status;
	}

	public boolean verifyElementPresent(By by, String locatorName){
		boolean status = false;
		try {
			if (this.Driver.findElement(by).isDisplayed()) {
				try {
					this.reporter.SuccessReport("VerifyElementIsPresent " + locatorName,
							this.msgIsElementFoundSuccess + locatorName);
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				status = true;
			}
		} catch (Exception e) {
			status = false;
			//LOG.info(e.getMessage());
			try {
				//this.reporter.failureReport("isElementPresent", this.msgIsElementFoundFailure + locatorName, Driver);
			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
		}
		return status;

	}

	/**
	 * This function imitates Mouse hover
	 * 
	 * @return
	 * @throws Throwable
	 */

	public boolean JSMouseHoverImitate(By by, String locatorName) throws Throwable {
		boolean flag = false;

		try {
			WebElement element = Driver.findElement(by);
			JavascriptExecutor js = (JavascriptExecutor) Driver;
			js.executeScript("arguments[0].setAttribute('style', 'display: block;')", element);
			Thread.sleep(3000);
			flag = true;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				reporter.failureReport("JSMouseHoverImitate of element is not found :: ",
						"Element :: " + locatorName + " is not available", Driver);
			} else if (flag) {
				reporter.SuccessReport("JSMouseHoverImitate of element is found :: ",
						"Element :: " + locatorName + "  is available");
			}
		}
	}

	/**
	 * This function useful to scroll browser up and down
	 * 
	 * @return
	 * @throws Throwable
	 */

	public static boolean scrollBrowserPage(WebDriver browser, int steps, SCROLL_TYPE scrollType) {
		boolean result = false;
		JavascriptExecutor js = (JavascriptExecutor) browser;
		String strExecution = "";
		try {
			String scrollAddOrDeduct = "";
			if (IFrameworkConstant.SCROLL_TYPE.UP.equalsIgnoreCase(scrollType.toString())) {
				scrollAddOrDeduct = "-";
			}
			strExecution = "window.scrollBy(0," + scrollAddOrDeduct + steps + ")";
			js.executeScript(strExecution, "");
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result = false;
			}
		}
		return result;
	}

	/**
	 * This function is for implicit wait
	 * 
	 * @return
	 * @throws Throwable
	 */

	public static void implicitWait(WebDriver browser, int waittime) {
		browser.manage().timeouts().implicitlyWait(waittime, TimeUnit.SECONDS);
	}

	/**
	 * This function is for explicit wait
	 * 
	 * @return
	 * @throws Throwable
	 */

	public static WebElement explicitWait(WebDriver browser, By webby, int waittime) {
		WebDriverWait wait = new WebDriverWait(browser, waittime);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webby));
		return element;
	}

	/**
	 * This function is for explicit wait using web element
	 * 
	 * @return
	 * @throws Throwable
	 */
	public static WebElement explicitWait(WebDriver browser, WebElement webElement, int waittime) {
		WebDriverWait wait = new WebDriverWait(browser, waittime);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
		return element;
	}
	public int generateRandomNumber(){
		Random rand = new Random();
		int randomNum = 1 + rand.nextInt((10000 - 1) + 1);
		return randomNum;
	}
	/*	public synchronized WebElement waitUntil(EventFiringWebDriver currDriver, ExpectedCondition<WebElement> currExpCond, long waitMilliSeconds, long pollingMilliSeconds, Boolean shouldThrowException) throws Exception
	{
		long impWait =0;
		//currDriver.manage().
		currDriver.manage().timeouts().implicitlyWait(pollingMilliSeconds, TimeUnit.MILLISECONDS);
		WebElement currElement=null;
		try{
			long startTime = new Date().getTime();
			while((new Date().getTime() - startTime) < waitMilliSeconds){
				try
				{					
					currElement=currExpCond.apply(Driver);
				}
				catch(Exception err)
				{
					//Don't catch anything as it is expected
				}

				if (currElement!=null)
				{
					System.out.println("ExpectedCondition Satisfied:" + currExpCond.toString() + " in " + (new Date().getTime() - startTime)/1000 + " Seconds");
					break;
				}
			}
		}
		catch(Throwable e){
			System.out.println("ExpectedCondition not satisfied:" + e.getMessage());
			if(shouldThrowException==true)
			{
				throw new CustomException("Error in waitUntil(): Condition not satisfied:" +  currExpCond.toString() + ", time timedout: Waited for " + waitMilliSeconds/1000 + " Seconds");
			}
		}
		finally{
			currDriver.manage().timeouts().implicitlyWait(IFrameworkConstant.WAIT_TIME_MAX_PAGE, TimeUnit.MILLISECONDS);
			if(currElement==null){
				if(shouldThrowException==true)
				{
					throw new CustomException("Error in waitUntil(): Condition not satisfied:" +  currExpCond.toString() + ", time timedout: Waited for " + waitMilliSeconds/1000 + " Seconds");
				}				
			}			
			return currElement;
		}

	}*/

	public void scrollToView(By locator)
	{
		JavascriptExecutor je = (JavascriptExecutor) Driver; 
		WebElement element = Driver.findElement(locator);


		je.executeScript("arguments[0].scrollIntoView(true);",element);
	}
	public String getCurrentURL(){

		return Driver.getCurrentUrl();

	}
	
	

	public void m_SwipeBottom(WebElement remoteelem){
		if(remoteelem != null){
			double browser_top_offset = 0.0;
			browser_top_offset = 0;
			//				RemoteWebElement remoteelem = ((RemoteWebElement)element);        	
			//				JavascriptExecutor js = (JavascriptExecutor)driver;
			Point eloc =  remoteelem.getLocation();
			double yloc = eloc.getY();
			double xloc = eloc.getX() + remoteelem.getSize().width/2;
			Double swipe_xratio = xloc;
			double elemheight = remoteelem.getSize().getHeight();
			Double yStartRatio = (yloc + elemheight + browser_top_offset)/2; 
			Double yEndRatio = (eloc.getY()+browser_top_offset);
			if(swipe_xratio < 10.0){swipe_xratio = 10.0;}
			if(yEndRatio < 50.0){yEndRatio = 50.0;}
			HashMap<String, Double> swipeObject = new HashMap<String, Double>();
			swipeObject.put("startX", swipe_xratio);
			swipeObject.put("startY", yStartRatio);
			swipeObject.put("endX", swipe_xratio);
			swipeObject.put("endY", yEndRatio);
			swipeObject.put("duration", 1.0);
			//				js.executeScript("mobile: swipe", swipeObject); 
			if(appiumDriver!=null && appiumDriver.getClass().toString().toUpperCase().contains("IOS")){
				appiumDriver.swipe(swipe_xratio.intValue(), yStartRatio.intValue(), swipe_xratio.intValue(), yEndRatio.intValue(), 1);
			}
		}

	}


	public void m_SwipeTop(WebElement element) {
		try{
			double browser_top_offset = 0.0;
			browser_top_offset = 0;
			RemoteWebElement remoteelem = ((RemoteWebElement)element);        	
			JavascriptExecutor js = (JavascriptExecutor)appiumDriver;
			Point eloc =  remoteelem.getLocation();
			double yloc = eloc.getY();
			double xloc = eloc.getX()/2 + remoteelem.getSize().width/2;
			double swipe_xratio = xloc;
			double elemheight = remoteelem.getSize().getHeight();
			double yStartRatio = (yloc + elemheight/2 + browser_top_offset)/2; 
			double yEndRatio = (eloc.getY()+browser_top_offset);
			if(swipe_xratio < 10.0){swipe_xratio = 10.0;}
			if(yEndRatio < 250.0){yEndRatio = 250.0;}
			HashMap<String, Double> swipeObject = new HashMap<String, Double>();
			swipeObject.put("startX", swipe_xratio);
			swipeObject.put("startY",yEndRatio);
			swipeObject.put("endX", swipe_xratio);
			swipeObject.put("endY",yStartRatio);
			swipeObject.put("duration", 1.0);
			js.executeScript("mobile: swipe", swipeObject); 
		}
		catch(Exception e1){
			e1.printStackTrace();
		}
	}

	// swipe left to right

	public void m_SwipeLeftToRight() throws Throwable {
		try {
			this.reporter.SuccessReport("Inside SwipeLeftToRight Method","Successfully navigated to SwipeLeftToRight Method");			
			JavascriptExecutor js = (JavascriptExecutor) this.appiumDriver;
			HashMap swipeObject = new HashMap();			
			swipeObject.put("startX", 0.5);
			swipeObject.put("startY", 0.95);
			swipeObject.put("endX", 0.05);
			swipeObject.put("endY", 0.5);
			swipeObject.put("duration", 1.8);
			js.executeScript("mobile: swipe", swipeObject);
			this.reporter.SuccessReport("successfully performed swiping",
					"Successfully navigated to Menu screen on sliding");

			/*WebElement seekBar = driver.findElement(By.id("com.cj.scrolling:id/seekbar"));
			//Get start point of seekbar.
			int startX = seekBar.getLocation().getX();
			System.out.println(startX);
			//Get end point of seekbar.
			    int endX = seekBar.getSize().getWidth();
			    System.out.println(endX);
			    //Get vertical location of seekbar.
			    int yAxis = seekBar.getLocation().getY();
			    //Set slidebar move to position.
			    // this number is calculated based on (offset + 3/4width)
			    int moveToXDirectionAt = 1000 + startX;
			    System.out.println("Moving seek bar at " + moveToXDirectionAt+" In X direction.");
			    //Moving seekbar using TouchAction class.
			    TouchAction act=new TouchAction(driver);
			    act.longPress(startX,yAxis).moveTo(moveToXDirectionAt,yAxis).release().perform();*/
		} catch (Exception e1) {

			this.reporter.failureReport("unable to perform swiping",
					"unable to  navigate to Menu screen on sliding");
		}
	}

	// swipe Right to left

	/*public void SwipeRightToLeft() {
		try {
			LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			LOG.info("Class name" + getCallerClassName() + "Method name : " + getCallerMethodName());
			appiumDriver.context("NATIVE_APP"); 
			Dimension size = appiumDriver.manage().window().getSize(); 
			int startx = (int) (size.width * 0.8); 
			int endx = (int) (size.width * 0.20); 
			int starty = size.height / 2; 
			appiumDriver.swipe(startx, starty, endx, starty, 1000);
		} catch (Exception e1) {

		}
	}*/

	public void m_swipeHorizantal() throws Throwable{
		try {
			appiumDriver.context("NATIVE_APP"); 
			//appiumDriver.findElementByAndroidUIAutomator("UiSelector().className(\"android.view.View\").instance(1)").click();
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			//AndroidDriver.findElementByAndroidUIAutomator(using)
			AndroidDriver.findElementByAndroidUIAutomator("UiSelector().className(\"android.view.View\").instance(1)").click();

			//WebElement contact =AndroidDriver.findElementByAndroidUIAutomator("UiSelector().xpath(\"android.view.View[@index='1']\")");
			Thread.sleep(3000);
			AndroidDriver.swipe(10, 30, 25, 1024, 1000);//Horizontal from right to left
			//AndroidDriver.swipe(570,600,25,1024,2000);//Horizontal from left to right
		}
		catch (Exception e1) {
			this.reporter.failureReport("unable to perform swiping",
					"unable to  navigate to Menu screen on sliding");
		}
	}


	public void m_elementScreenShot(ITestContext testContext,By by, String imgName) throws IOException  {
		File screenshot = null;
		WebElement ele = null;
		if(this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.ANDROID))
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			ele = AndroidDriver.findElement(by);
			// Get entire page screenshot
			screenshot = ((TakesScreenshot)AndroidDriver).getScreenshotAs(OutputType.FILE);
		}
		else if(this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS))
		{			 
			IOSDriver iosDriver=(IOSDriver)this.appiumDriver;
			ele = iosDriver.findElement(by);
		}		  
		BufferedImage  fullImg = ImageIO.read(screenshot);

		// Get the location of element on the page
		Point point = ele.getLocation();

		// Get width and height of the element
		int eleWidth = ele.getSize().getWidth();
		int eleHeight = ele.getSize().getHeight();

		// Crop the entire page screenshot to get only element screenshot
		BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(),
				eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);


		// Copy the element screenshot to disk
		File screenshotLocation = new File("ElemetScreenShots\\"+imgName);//+".png"
		FileUtils.copyFile(screenshot, screenshotLocation);
	}	

	public void pressTab() throws Throwable
	{
		Robot r=new Robot();
		Thread.sleep(2000);

		r.keyPress(KeyEvent.VK_ALT);
		r.keyPress(KeyEvent.VK_S);
		r.keyRelease(KeyEvent.VK_ALT);
		r.keyRelease(KeyEvent.VK_S);
	}

	public  void swipeup()
	{
		try
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\"android:id/list\")).scrollIntoView(UiSelector().textContains(\"Reset User Data\"))");

		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	public  void swipe(String locator,String text)
	{
		try
		{
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			AndroidDriver.findElementByAndroidUIAutomator("UiScrollable(UiSelector().resourceId(\""+locator+"\")).scrollIntoView(UiSelector().textContains(\""+text+"\"))");

		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	public void hideKeyBoard()
	{
		AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
		AndroidDriver.hideKeyboard();

	}
	public void pickDate(String day)
	{


		AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
		List<AndroidElement>elements=AndroidDriver.findElementsByAndroidUIAutomator("UiSelector().resourceId(\"android:id/numberpicker_input\")");
		System.out.println(elements.size());
		System.out.println(elements.get(i).getText());
		day=elements.get(i).getText();
		int x=elements.get(i).getLocation().x;
		int y=elements.get(i).getLocation().y;

		System.out.println(elements.get(i).getText());

		AndroidDriver.swipe(x, y, x, y - 196, 3000);


	}

	public void returnIndex(String picknumber)
	{
		switch(picknumber) {

		case "day":
		{ 
			i=0;
			break;
		}
		case "month":
		{ 
			i=1;
			break;
		}
		case "year":
		{ 
			i=2;
			break;
		}

		}
		System.out.println(i);

	}


}
