package com.gallop.accelerators;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;

import com.gallop.support.IFrameworkConstant;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class ActionEngineMobile extends TestEngine{
	private final String msgClickSuccess = "Successfully Clicked On ";
	private final String msgClickFailure = "Unable To Click On ";
	private final String msgTypeSuccess = "Successfully Entered Value ";
	private final String msgTypeFailure = "Unable To Type On ";
	private final String msgIsElementFoundSuccess = "Successfully Found Element ";
	private final String msgIsElementFoundFailure = "Unable To Found Element ";

	public void m_SwipeBottom(WebElement remoteelem){
		if(remoteelem != null){
				double browser_top_offset = 0.0;
					browser_top_offset = 0;
				//				RemoteWebElement remoteelem = ((RemoteWebElement)element);        	
				//				JavascriptExecutor js = (JavascriptExecutor)driver;
				Point eloc =  remoteelem.getLocation();
				double yloc = eloc.getY();
				double xloc = eloc.getX() + remoteelem.getSize().width/2;
				Double swipe_xratio = xloc;
				double elemheight = remoteelem.getSize().getHeight();
				Double yStartRatio = (yloc + elemheight + browser_top_offset)/2; 
				Double yEndRatio = (eloc.getY()+browser_top_offset);
				if(swipe_xratio < 10.0){swipe_xratio = 10.0;}
				if(yEndRatio < 50.0){yEndRatio = 50.0;}
				HashMap<String, Double> swipeObject = new HashMap<String, Double>();
				swipeObject.put("startX", swipe_xratio);
				swipeObject.put("startY", yStartRatio);
				swipeObject.put("endX", swipe_xratio);
				swipeObject.put("endY", yEndRatio);
				swipeObject.put("duration", 1.0);
				//				js.executeScript("mobile: swipe", swipeObject); 
				if(appiumDriver!=null && appiumDriver.getClass().toString().toUpperCase().contains("IOS")){
					appiumDriver.swipe(swipe_xratio.intValue(), yStartRatio.intValue(), swipe_xratio.intValue(), yEndRatio.intValue(), 1);
				}
		}

	}
	

	public void m_SwipeTop(WebElement element) {
		try{
			double browser_top_offset = 0.0;
				browser_top_offset = 0;
			RemoteWebElement remoteelem = ((RemoteWebElement)element);        	
			JavascriptExecutor js = (JavascriptExecutor)appiumDriver;
			Point eloc =  remoteelem.getLocation();
			double yloc = eloc.getY();
			double xloc = eloc.getX()/2 + remoteelem.getSize().width/2;
			double swipe_xratio = xloc;
			double elemheight = remoteelem.getSize().getHeight();
			double yStartRatio = (yloc + elemheight/2 + browser_top_offset)/2; 
			double yEndRatio = (eloc.getY()+browser_top_offset);
			if(swipe_xratio < 10.0){swipe_xratio = 10.0;}
			if(yEndRatio < 250.0){yEndRatio = 250.0;}
			HashMap<String, Double> swipeObject = new HashMap<String, Double>();
			swipeObject.put("startX", swipe_xratio);
			swipeObject.put("startY",yEndRatio);
			swipeObject.put("endX", swipe_xratio);
			swipeObject.put("endY",yStartRatio);
			swipeObject.put("duration", 1.0);
			js.executeScript("mobile: swipe", swipeObject); 
		}
		catch(Exception e1){
			e1.printStackTrace();
		}
	}
	
	// swipe left to right

	public void m_SwipeLeftToRight() throws Throwable {
		try {
			this.reporter.SuccessReport("Inside SwipeLeftToRight Method","Successfully navigated to SwipeLeftToRight Method");			
			JavascriptExecutor js = (JavascriptExecutor) this.appiumDriver;
			HashMap swipeObject = new HashMap();			
			swipeObject.put("startX", 0.5);
			swipeObject.put("startY", 0.95);
			swipeObject.put("endX", 0.05);
			swipeObject.put("endY", 0.5);
			swipeObject.put("duration", 1.8);
			js.executeScript("mobile: swipe", swipeObject);
			this.reporter.SuccessReport("successfully performed swiping",
					"Successfully navigated to Menu screen on sliding");
			
			/*WebElement seekBar = driver.findElement(By.id("com.cj.scrolling:id/seekbar"));
			//Get start point of seekbar.
			int startX = seekBar.getLocation().getX();
			System.out.println(startX);
			//Get end point of seekbar.
			    int endX = seekBar.getSize().getWidth();
			    System.out.println(endX);
			    //Get vertical location of seekbar.
			    int yAxis = seekBar.getLocation().getY();
			    //Set slidebar move to position.
			    // this number is calculated based on (offset + 3/4width)
			    int moveToXDirectionAt = 1000 + startX;
			    System.out.println("Moving seek bar at " + moveToXDirectionAt+" In X direction.");
			    //Moving seekbar using TouchAction class.
			    TouchAction act=new TouchAction(driver);
			    act.longPress(startX,yAxis).moveTo(moveToXDirectionAt,yAxis).release().perform();*/
		} catch (Exception e1) {
			
			this.reporter.failureReport("unable to perform swiping",
					"unable to  navigate to Menu screen on sliding");
		}
	}

	// swipe Right to left

	/*public void SwipeRightToLeft() {
		try {
			LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			LOG.info("Class name" + getCallerClassName() + "Method name : " + getCallerMethodName());
			appiumDriver.context("NATIVE_APP"); 
			Dimension size = appiumDriver.manage().window().getSize(); 
			int startx = (int) (size.width * 0.8); 
			int endx = (int) (size.width * 0.20); 
			int starty = size.height / 2; 
			appiumDriver.swipe(startx, starty, endx, starty, 1000);
		} catch (Exception e1) {

		}
	}*/
	
	public void m_swipeHorizantal() throws Throwable{
		try {
			appiumDriver.context("NATIVE_APP"); 
			//appiumDriver.findElementByAndroidUIAutomator("UiSelector().className(\"android.view.View\").instance(1)").click();
			AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
			//AndroidDriver.findElementByAndroidUIAutomator(using)
			AndroidDriver.findElementByAndroidUIAutomator("UiSelector().className(\"android.view.View\").instance(1)").click();
			
			//WebElement contact =AndroidDriver.findElementByAndroidUIAutomator("UiSelector().xpath(\"android.view.View[@index='1']\")");
			Thread.sleep(3000);
			AndroidDriver.swipe(10, 30, 25, 1024, 1000);//Horizontal from right to left
			//AndroidDriver.swipe(570,600,25,1024,2000);//Horizontal from left to right
	  }
		 catch (Exception e1) {
				this.reporter.failureReport("unable to perform swiping",
						"unable to  navigate to Menu screen on sliding");
			}
		}
	
		
	public void m_elementScreenShot(ITestContext testContext,By by, String imgName) throws IOException  {
		File screenshot = null;
		WebElement ele = null;
		 if(this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.ANDROID))
		 {
		    AndroidDriver AndroidDriver=(AndroidDriver)this.appiumDriver;
		    ele = AndroidDriver.findElement(by);
		    // Get entire page screenshot
		    screenshot = ((TakesScreenshot)AndroidDriver).getScreenshotAs(OutputType.FILE);
		 }
		 else if(this.testParameters.platform.equalsIgnoreCase(IFrameworkConstant.OS.IOS))
		 {			 
			 IOSDriver iosDriver=(IOSDriver)this.appiumDriver;
			 ele = iosDriver.findElement(by);
		 }		  
	    BufferedImage  fullImg = ImageIO.read(screenshot);

	    // Get the location of element on the page
	    Point point = ele.getLocation();

	    // Get width and height of the element
	    int eleWidth = ele.getSize().getWidth();
	    int eleHeight = ele.getSize().getHeight();

	    // Crop the entire page screenshot to get only element screenshot
	    BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(),
	        eleWidth, eleHeight);
	    ImageIO.write(eleScreenshot, "png", screenshot);

	
	    // Copy the element screenshot to disk
	    File screenshotLocation = new File("ElemetScreenShots\\"+imgName);//+".png"
	    FileUtils.copyFile(screenshot, screenshotLocation);
	}	
}
