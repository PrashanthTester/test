package com.gallop.report;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.Assert;
import org.testng.asserts.Assertion;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.gallop.accelerators.TestEngine;
import com.gallop.support.IFrameworkConstant;
/**
 * Class Name: CReporter. Reason for naming class CReporter is to avoid
 * ambiguity with TestNG Reporter class
 * 
 * @author in01518
 *
 */
public class CReporter {
	private static final Logger LOG = Logger.getLogger(CReporter.class);
	private BrowserContext browserContext = null;
	private static Map<BrowserContext, CReporter> mapBrowserContextReporter = new HashMap<BrowserContext, CReporter>();
	private String[] package_testname;
	private ExtentTest extentLogger=null;
	
	/**
	 * @Consturctor for CReporter
	 * @param browserName
	 * @param version
	 * @param platform
	 * @param append
	 * @throws IOException
	 */
	private CReporter(String browserName, String version, String platform, String executionon,
			boolean append) {
			this.browserContext = BrowserContext.getBrowserContext(browserName,
					version, platform, executionon);
			LOG.info("instance member browserContext was set to : ");
			LOG.info(this.browserContext);
	}
	
	/**
	 * 
	 * @return BrowserContext
	 */
	public BrowserContext getBrowserContext() {
		return this.browserContext;
	}

	public static synchronized CReporter getCReporter(String browserName,
			String version, String platform, String executionon, boolean append) {
			CReporter reporter = null;				
			BrowserContext browserContext = BrowserContext.getBrowserContext(
					browserName, version, platform, executionon);
			reporter = CReporter.mapBrowserContextReporter.get(browserContext);
			if (reporter == null) {
				reporter = new CReporter(browserName, version, platform, executionon, append);
				LOG.info("Instance Of CReporter Created");
				CReporter.mapBrowserContextReporter.put(browserContext, reporter);
				LOG.info("reporter was placed into CReporter.mapBrowserContextReporter");
			}
		return reporter;
	}

	public void initTestCase(String packageName, String testCaseName,
			String testCaseDescription, boolean appendTestCaseResult, ExtentTest extentTestLoggerObject) {
		

		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT)){
			/*Extent Report Logger - included by Sreekanth*/
			initExtentTestLogger(extentTestLoggerObject);
		}
		else if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
			TestResult.tc_name.put(this.browserContext, testCaseName + "-");
			TestResult.packageName.put(this.browserContext, packageName);
			this.testHeader(TestResult.tc_name.get(this.browserContext),
					appendTestCaseResult);
			TestResult.stepNum.put(browserContext, 0);
			TestResult.PassNum.put(browserContext, 0);
			TestResult.FailNum.put(browserContext, 0);
			TestResult.testName.put(browserContext, testCaseName);
			this.calculateTestCaseStartTime();
			if (testCaseDescription != null) 
			{
				this.initTestCaseDescription(testCaseDescription);
			}
		}
	}

	public void initTestCaseDescription(String testCaseDescription) {
		if (testCaseDescription != null) {
			Map<String, String> mapTestDescription = TestResult.testDescription
					.get(this.getBrowserContext());
			if (mapTestDescription == null) {
				mapTestDescription = new HashMap<String, String>();

			}
			mapTestDescription.put(TestResult.tc_name.get(this.browserContext),
					testCaseDescription);
			TestResult.testDescription.put(this.browserContext,
					mapTestDescription);
		}
	}

	private String getFileName(String filePath) throws Exception {
		String fileNameOnly = null;
		File file = new File(filePath);
		try {

			if (file.isFile()) {
				fileNameOnly = file.getName().toString();
			}

		} catch (Exception e) {
			LOG.error("Exception Encountered : " + e.getMessage());
			throw e;
		}
		return fileNameOnly;
	}

	/**
	 * 
	 * @return ResultDir for each browserContext
	 */
	private String filePath() {
		File resultDir=null;
		File screenShotDir = null;
		String strDirectory = "";
		String browserName = this.browserContext.getBrowserName();
		String browserVersion = this.browserContext.getBrowserVersion();
		String browserPlatform = this.browserContext.getBrowserPlatform();
		String executionon = this.browserContext.getBrowserExecutionON();
		String screenshotPath="";
		
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT))
		{
			LOG.debug("browser name = " + browserName);
			if(browserName.equalsIgnoreCase(IFrameworkConstant.BROWSER.FIREFOX))
			{
				strDirectory = "FF";
			}
			else if(browserName.equalsIgnoreCase(IFrameworkConstant.BROWSER.CHROME))
			{
				strDirectory = "CHROME";
			}
			else if(browserName.equalsIgnoreCase(IFrameworkConstant.BROWSER.IE))
			{
				strDirectory = "IE";
			}
			else{
				strDirectory = browserName;
			}

			strDirectory = strDirectory + "-" + browserPlatform + "-" + executionon ;
		}
		
		resultDir = new File(ReporterConstants.LOCATION_RESULT
				+ File.separator + strDirectory);
		LOG.info("resultDir = " + resultDir);
		if (resultDir.exists() == false) {
			try {
				resultDir.mkdirs();
			} catch (Exception e) {
				LOG.info("Exception Encountered : " + e.getMessage());
			}
		}
		screenshotPath = ReporterConstants.LOCATION_RESULT + File.separator;
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
			screenshotPath = screenshotPath + strDirectory + File.separator;
		}
		screenshotPath = screenshotPath + ReporterConstants.FOLDER_SCREENRSHOTS; 
		screenShotDir = new File( screenshotPath);

		if (screenShotDir.exists() == false) {
			try {
				screenShotDir.mkdirs();
				if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
					this.copyLogos(ReporterConstants.LOCATION_CLIENT_LOGO,
						ReporterConstants.LOCATION_COMPANY_LOGO,
						ReporterConstants.LOCATION_FAILED_LOGO,
						ReporterConstants.LOCATION_MINUS_LOGO,
						ReporterConstants.LOCATION_PASSED_LOGO,
						ReporterConstants.LOCATION_PLUS_LOGO,
						ReporterConstants.LOCATION_WARNING_LOGO,
						ReporterConstants.LOCATION_JQUERY_CSS_FOLDER,
						ReporterConstants.LOCATION_JQUERY_IMAGES_FOLDER,
						ReporterConstants.LOCATION_JQUERY_JS_FOLDER);
				}
			} catch (Exception e) {
				LOG.info("Exception Encountered : " + e.getMessage());
			}
		}

		try {
			strDirectory = resultDir.getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LOG.error("IOException Encountered : " + e.getMessage());
			e.printStackTrace();
		}
		return strDirectory;
	}

	private void copyLogos(String... logos) {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT))
		{
			File destFolder = new File(this.filePath() + File.separatorChar
					+ ReporterConstants.FOLDER_SCREENRSHOTS);
			for (String logo : logos) {
				LOG.info("Current Logo Name : " + logo);
				File logoFile = new File(logo);
				/*if folder then copy folder*/
				if (logoFile.isDirectory()) {
					try {
						FileUtils.copyDirectoryToDirectory(logoFile, destFolder);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						LOG.info(logoFile + "could not be copied to " + destFolder);
						LOG.info("IOException Encountered : " + e.getMessage());
						e.printStackTrace();
					}
				}
				/*if file then copy file*/
				if (logoFile.isDirectory() == false) {
					/* copy File if exist */
					if (logoFile.exists()) {
						try {
							FileUtils.copyFileToDirectory(logoFile, destFolder);
							LOG.info(logoFile + "copied to " + destFolder);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							LOG.info(logoFile + "could not be copied to "
									+ destFolder);
							LOG.info("IOException Encountered : " + e.getMessage());
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * deletes html detailed file if exist
	 */

	private void htmlCreateReport() {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT))
		{

			File file = new File(this.filePath() + "/" + TestResult.strTestName
					+ "_Results"
					/* + TestResult.timeStamp */+ ".html");// "Results.html"
			if (file.exists()) {
				file.delete();
			}
		}	
	}

	public void createHtmlSummaryReport(String Url, boolean append)
			throws Exception {

		if(!IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) return;

		File file = new File(this.filePath() + "/" + "SummaryResults" 
		/* + TestResult.timeStamp */+ ".html");// "SummaryReport.html"
		Writer writer = null;
		String imgSrcClientLogo = "." + File.separatorChar
				+ ReporterConstants.FOLDER_SCREENRSHOTS + File.separatorChar
				+ this.getFileName(ReporterConstants.LOCATION_CLIENT_LOGO);
		String imgSrcCompanyLogo = "." + File.separatorChar
				+ ReporterConstants.FOLDER_SCREENRSHOTS + File.separatorChar
				+ this.getFileName(ReporterConstants.LOCATION_COMPANY_LOGO);
		if (file.exists()) {
			file.delete();
		}
		writer = new FileWriter(file, append);
		try {
			writer.write("<!DOCTYPE html>" + ReporterConstants.NEW_LINE);
			writer.write("<html> " + ReporterConstants.NEW_LINE);
			writer.write("<head> " + ReporterConstants.NEW_LINE);
			writer.write("<meta charset='UTF-8'> " + ReporterConstants.NEW_LINE);
			writer.write("<title>Automation Execution Results Summary</title>" + ReporterConstants.NEW_LINE);

			// Jquery java script
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/datatable/jquery.dataTables.css'>" + ReporterConstants.NEW_LINE);
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/jquery-ui.css'>" + ReporterConstants.NEW_LINE);
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/datatable/dataTables.jqueryui.css'>" + ReporterConstants.NEW_LINE);
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/custom.css'>" + ReporterConstants.NEW_LINE);

			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/jquery-1.11.1.min.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/datatable/jquery.dataTables.min.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/datatable/dataTables.jqueryui.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/jquery-ui.min.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/custom.js'></script>" + ReporterConstants.NEW_LINE);

			writer.write("<style type='text/css'>" + ReporterConstants.NEW_LINE);
			writer.write("body {");
			writer.write("background-color: #FFFFFF; ");
			writer.write("font-family: Verdana, Geneva, sans-serif; ");
			writer.write("text-align: left; ");
			writer.write("} ");

			writer.write("small { ");
			writer.write("font-size: 0.7em; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("table { ");
			writer.write("box-shadow: 9px 9px 10px 4px #BDBDBD;");
			writer.write("border: 0px solid #4D7C7B;");
			writer.write("border-collapse: collapse; ");
			writer.write("border-spacing: 0px; ");
			writer.write("width: 1000px; ");
			writer.write("margin-left: auto; ");
			writer.write("margin-right: auto; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.heading { ");
			writer.write("background-color: #041944;");
			writer.write("color: #FFFFFF; ");
			writer.write("font-size: 0.7em; ");
			writer.write("font-weight: bold; ");
			writer.write("background:-o-linear-gradient(bottom, #999999 5%, #000000 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #999999), color-stop(1, #000000) );");
			writer.write("background:-moz-linear-gradient( left top, #999999 5%, #000000 100% );");
			writer.write("filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#999999, endColorstr=#000000);	background: -o-linear-gradient(top,#999999,000000);");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.subheading { ");
			writer.write("background-color: #6A90B6;");
			writer.write("color: #000000; ");
			writer.write("font-weight: bold; ");
			writer.write("font-size: 0.7em; ");
			writer.write("text-align: justify; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.section { ");
			writer.write("background-color: #A4A4A4; ");
			writer.write("color: #333300; ");
			writer.write("cursor: pointer; ");
			writer.write("font-weight: bold;");
			writer.write("font-size: 0.8em; ");
			writer.write("text-align: justify;");
			writer.write("background:-o-linear-gradient(bottom, #56aaff 5%, #e5e5e5 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #56aaff), color-stop(1, #e5e5e5) );");
			writer.write("background:-moz-linear-gradient( left top, #56aaff 5%, #e5e5e5 100% );");
			writer.write("filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#56aaff, endColorstr=#e5e5e5);	background: -o-linear-gradient(top,#56aaff,e5e5e5);");

			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.subsection { ");
			writer.write("cursor: pointer; ");
			writer.write("} ");

			writer.write("tr.content { ");
			writer.write("background-color: #FFFFFF; ");
			writer.write("color: #000000; ");
			writer.write("font-size: 0.7em; ");
			writer.write("display: table-row; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.content2 { ");
			writer.write("background-color:#;E1E1E1");
			writer.write("border: 1px solid #4D7C7B;");
			writer.write("color: #000000; ");
			writer.write("font-size: 0.7em; ");
			writer.write("display: table-row; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td, th { ");
			writer.write("padding: 5px; ");
			writer.write("border: 1px solid #4D7C7B; ");
			writer.write("text-align: inherit\0/; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("th.Logos { ");
			writer.write("padding: 5px; ");
			writer.write("border: 0px solid #4D7C7B; ");
			writer.write("text-align: inherit /;");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.left { ");
			writer.write("text-align: left; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.pass {");
			writer.write("font-weight: bold; ");
			writer.write("color: green; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.fail { ");
			writer.write("font-weight: bold; ");
			writer.write("color: red; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.done, td.screenshot { ");
			writer.write("font-weight: bold; ");
			writer.write("color: black; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.debug { ");
			writer.write("font-weight: bold; ");
			writer.write("color: blue; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.warning { ");
			writer.write("font-weight: bold; ");
			writer.write("color: orange; ");
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("</style> " + ReporterConstants.NEW_LINE);

			writer.write("<script> ");
			writer.write("function toggleMenu(objID) { " + ReporterConstants.NEW_LINE);
			writer.write(" if (!document.getElementById) return;" + ReporterConstants.NEW_LINE);
			writer.write(" var ob = document.getElementById(objID).style; " + ReporterConstants.NEW_LINE);
			writer.write("if(ob.display === 'none') { " + ReporterConstants.NEW_LINE);
			writer.write(" try { " + ReporterConstants.NEW_LINE);
			writer.write(" ob.display='table-row-group';" + ReporterConstants.NEW_LINE);
			writer.write("} catch(ex) { " + ReporterConstants.NEW_LINE);
			writer.write("	 ob.display='block'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("else { " + ReporterConstants.NEW_LINE);
			writer.write(" ob.display='none'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("function toggleSubMenu(objId) { " + ReporterConstants.NEW_LINE);
			writer.write("for(i=1; i<10000; i++) { " + ReporterConstants.NEW_LINE);
			writer.write("var ob = document.getElementById(objId.concat(i)); " + ReporterConstants.NEW_LINE);
			writer.write("if(ob === null) { " + ReporterConstants.NEW_LINE);
			writer.write("break; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("if(ob.style.display === 'none') { " + ReporterConstants.NEW_LINE);
			writer.write("try { " + ReporterConstants.NEW_LINE);
			writer.write(" ob.style.display='table-row'; " + ReporterConstants.NEW_LINE);
			writer.write("} catch(ex) { " + ReporterConstants.NEW_LINE);
			writer.write("ob.style.display='block'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write(" } " + ReporterConstants.NEW_LINE);
			writer.write("else { " + ReporterConstants.NEW_LINE);
			writer.write("ob.style.display='none'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write(" } " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("</script> " + ReporterConstants.NEW_LINE);
			writer.write("</head> " + ReporterConstants.NEW_LINE);

			writer.write("<body> " + ReporterConstants.NEW_LINE);
			writer.write(ReporterConstants.HTML_TAG_LINE_BREAK_END + ReporterConstants.NEW_LINE);

			writer.write("<table id='Logos' class='testData'>");
			writer.write("<colgroup>");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("</colgroup> ");
			writer.write("<thead> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='content'>" + ReporterConstants.NEW_LINE);
			writer.write("<th class ='Logos' colspan='2' >" + ReporterConstants.NEW_LINE);
			writer.write("<img align ='left' src= " + imgSrcClientLogo
					+ "></img>");
			writer.write("</th>" + ReporterConstants.NEW_LINE);
			writer.write("<th class = 'Logos' colspan='2' > ");
			writer.write("<img align ='right' src=  " + imgSrcCompanyLogo
					+ "></img>");
			writer.write("</th> " + ReporterConstants.NEW_LINE);
			writer.write("</tr> " + ReporterConstants.NEW_LINE);

			writer.write("</thead> " + ReporterConstants.NEW_LINE);
			writer.write("</table> " + ReporterConstants.NEW_LINE);

			writer.write("<table id='header' class='testData'> " + ReporterConstants.NEW_LINE);
			writer.write("<colgroup> ");
			writer.write("<col style='width: 25%' /> ");
			writer.write("<col style='width: 25%' /> ");
			writer.write("<col style='width: 25%' /> ");
			writer.write(" <col style='width: 25%' /> ");
			writer.write("</colgroup> ");

			writer.write("<thead> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='heading'> " + ReporterConstants.NEW_LINE);
			writer.write("<th colspan='4' style='font-family:Copperplate Gothic Bold; font-size:1.4em;'> ");
			writer.write("Automation Execution Result Summary ");
			writer.write("</th> " + ReporterConstants.NEW_LINE);
			writer.write("</tr> " + ReporterConstants.NEW_LINE);
			writer.write("<tr class='subheading'> ");
			writer.write("<th>&nbsp;Date&nbsp;&&nbsp;Time&nbsp;:&nbsp;" + ""
					+ "</th> " + ReporterConstants.NEW_LINE);
			// writer.write("<th>&nbsp;:&nbsp;08-Apr-2013&nbsp;06:24:21&nbsp;PM</th> ");
			writer.write("<th> &nbsp;" + CReporter.dateTime() + "&nbsp;</th> ");
			writer.write("<th>&nbsp;OnError&nbsp;:&nbsp;</th> ");
			writer.write("<th>" + ReporterConstants.ON_ERROR_ACTION + "</th> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='subheading'> " + ReporterConstants.NEW_LINE);
			writer.write("<th>&nbsp;Suite Executed&nbsp;:&nbsp;</th> ");
			writer.write("<th>" + ReporterConstants.SUITE_NAME + "</th> ");
			writer.write("<th>&nbsp;Browser&nbsp;:</th> ");
			writer.write("<th>" + this.browserContext.getBrowserName()
					+ "</th> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='subheading'> " + ReporterConstants.NEW_LINE);
			writer.write("<th>&nbsp;Host Name&nbsp;:</th> ");
			writer.write("<th>" + InetAddress.getLocalHost().getHostName()
					+ "</th> ");
			writer.write("<th>Version&nbsp;:</th> ");
			writer.write("<th>" + browserContext.getBrowserVersion() + "</th> " + ReporterConstants.NEW_LINE);
			writer.write("</tr> " + ReporterConstants.NEW_LINE);
			/*writer.write("<th>&nbsp;No.&nbsp;Of&nbsp;Threads&nbsp;:&nbsp;</th> ");
			writer.write("<th>" + "NA" + "</th> ");
			writer.write("</tr> ");*/

			writer.write("<tr class='subheading'> " + ReporterConstants.NEW_LINE);
			/*writer.write("<th>&nbsp;Device&nbsp;Name&nbsp;:</th> ");
			writer.write("<th>" + ReporterConstants.DEVICE_NAME + "</th> ");*/
			writer.write("<th>&nbsp;Environment&nbsp;:</th> ");
			writer.write("<th>" + this.browserContext.getBrowserPlatform()+ "</th> ");
			/*writer.write("<th>Version&nbsp;:</th> ");
			writer.write("<th>" + ReporterConstants.BROWSER_VERSION + "</th> ");
			writer.write("</tr> ");*/
			writer.write("<th>&nbsp;No.&nbsp;Of&nbsp;Threads&nbsp;:&nbsp;</th> ");
			writer.write("<th>" + "NA" + "</th> " + ReporterConstants.NEW_LINE);
			writer.write("</tr> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='subheading'> " + ReporterConstants.NEW_LINE);
			writer.write("<th colspan='4'> ");
			writer.write("Application  -  " + Url + "");
			writer.write("</th> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);
			writer.write("</thead> " + ReporterConstants.NEW_LINE);
			writer.write("</table> " + ReporterConstants.NEW_LINE);
			writer.write("<div class='mainTableDiv4'>" + ReporterConstants.NEW_LINE);
			writer.write("<table id='main' class='testData'> " + ReporterConstants.NEW_LINE);
			writer.write("<colgroup> ");
			writer.write("<col style='width: 5%' /> ");
			writer.write("<col style='width: 38%' /> ");
			writer.write("<col style='width: 45%' /> ");
			writer.write("<col style='width: 4%' /> ");
			writer.write("<col style='width: 8%' /> ");
			writer.write("</colgroup> " + ReporterConstants.NEW_LINE);
			writer.write("<thead> " + ReporterConstants.NEW_LINE);
			writer.write("<tr class='heading'> " + ReporterConstants.NEW_LINE);
			writer.write("<th>S.NO</th> ");
			writer.write("<th>Test Case</th> ");
			writer.write("<th>Description</th> ");
			writer.write("<th>Time</th> ");
			writer.write("<th>Status</th> ");
			writer.write("</tr> ");
			writer.write("</thead> " + ReporterConstants.NEW_LINE);

			/* get corresponding map to browserContext */
			Map<String, String> testCaseRef = TestResult.mapBrowserContextTestCaseRef
					.get(this.browserContext);

			Iterator<Entry<String, String>> iterator1 = testCaseRef.entrySet()
					.iterator();
			int serialNo = 1;
			writer.write("<tbody> " + ReporterConstants.NEW_LINE);
			while (iterator1.hasNext()) {

				Map.Entry<String, String> mapEntry1 = iterator1
						.next();
				/* key contains packagename:testmethod */
				LOG.info("Key of mapEntry1 : " + mapEntry1.getKey());
				this.package_testname = mapEntry1.getKey().toString()
						.split(":");
				LOG.info("package is present in package_testname[0] : "
						+ this.package_testname[0]);
				LOG.info("test method is present in package_testname[1] : "
						+ this.package_testname[1]);
				String testCaseExecutionStatus = mapEntry1.getValue();
				LOG.info("value against package_testname is : "
						+ testCaseExecutionStatus);
				// writer.write("<tbody> ");
				writer.write("<tr class='content2' > ");
				writer.write("<td class='left'>" + serialNo + "</td>");
				if (testCaseExecutionStatus
						.equals(ReporterConstants.TEST_CASE_STATUS_PASS)) {
					writer.write("<td class='left'><a target='_blank' href='"
							+ package_testname[1]
							+ "_Results"
							/* + TestResult.timeStamp */+ ".html#'"
							+ "' target='about_blank'>"
							+ this.package_testname[1].substring(0,
									this.package_testname[1].indexOf("-"))
							+ "</a></td>");
				} else {
					writer.write("<td class='left'><a target='_blank' href='"
							+ this.package_testname[1]
							+ "_Results"
							/* + TestResult.timeStamp */+ ".html'"
							+ " target='about_blank'>"
							+ this.package_testname[1].substring(0,
									this.package_testname[1].indexOf("-"))
							+ "</a></td>");
				}
				String localTestDescription = "";
				if (TestResult.testDescription != null) {
					Map<String, String> mapTestDescription = TestResult.testDescription
							.get(this.browserContext);
					if (mapTestDescription != null) {
						localTestDescription = mapTestDescription
								.get(this.package_testname[1]);
					}
				}
				writer.write("<td class='left'>" + localTestDescription
						+ "</td>");

				writer.write("<td>"
						+ TestResult.executionTime.get(this.browserContext)
								.get(this.package_testname[1])
						+ " Seconds</td>");
				if (TestResult.testResults.get(this.browserContext)
						.get(this.package_testname[1])
						.equals(ReporterConstants.TEST_CASE_STATUS_PASS))
					writer.write("<td class='pass'>"
							+ ReporterConstants.TEST_CASE_STATUS_PASS
							+ "</td> ");
				else
					writer.write("<td class='fail'>"
							+ ReporterConstants.TEST_CASE_STATUS_FAIL
							+ "</td> ");
				writer.write("</tr>");
				// writer.write("</tbody> ");
				serialNo = serialNo + 1;
			}
			writer.write("</tbody> " + ReporterConstants.NEW_LINE);

		} catch (Exception e) {
			LOG.info("Excepiton Encountered : " + e.getMessage());
		}finally {
			writer.flush();
			writer.close();			
		}
	}

	/**
	 * 
	 * @param strStepName
	 * @param strStepDes
	 * @throws Exception 
	 */
	private void onSuccess(String strStepName, String strStepDes)
			throws Exception {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT)){
			/*Sreekanth Extent Report Logger*/
			this.extentLogger.pass("<u>Step </u>: " + strStepName + " <br><u>Step Description</u> : " + strStepDes);
		}
		else if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) 
		{
			File file = new File(this.filePath() + File.separatorChar
					+ TestResult.strTestName.get(this.browserContext) + "_Results"
					/* + TestResult.timeStamp */+ ".html");// "SummaryReport.html"
			Writer writer = null;
			Integer stepNumValue = TestResult.stepNum.get(this.browserContext);
			String imgSrc = "'." + File.separatorChar
					+ ReporterConstants.FOLDER_SCREENRSHOTS + File.separatorChar
					+ this.getFileName(ReporterConstants.LOCATION_PASSED_LOGO)
					+ "'";
			if (stepNumValue != null) {
	
				TestResult.stepNum.put(this.browserContext, stepNumValue + 1);
			}
	
			try {
				// testdescrption.put(TestTitleDetails.x.toString(),
				// TestResult.testDescription.get(TestTitleDetails.x));
				String strPackageName = TestResult.packageName
						.get(this.browserContext);
				String strTcName = TestResult.tc_name.get(this.browserContext);
				if (!TestResult.mapBrowserContextTestCaseRef
						.get(this.browserContext)
						.get(strPackageName + ":" + strTcName)
						.equals(ReporterConstants.TEST_CASE_STATUS_FAIL)) {
					TestResult.mapBrowserContextTestCaseRef
							.get(this.browserContext).put(
									strPackageName + ":" + strTcName,
									ReporterConstants.TEST_CASE_STATUS_PASS);
					// map.put(TestTitleDetails.x.toString(),
					// TestResult.testDescription.get(TestTitleDetails.x.toString()));
				}
				writer = new FileWriter(file, true);
				writer.write("<tr class='content2' >");
				writer.write("<td>" + TestResult.stepNum.get(this.browserContext)
						+ "</td> ");
				writer.write("<td class='left'>" + strStepName + "</td>");
				writer.write("<td class='left'>" + strStepDes + "</td> ");
	
				writer.write("<td class='Pass' align='center'><img  src=" + imgSrc
						+ " width='18' height='18'/></td> ");
	
				Integer passNumValue = TestResult.PassNum.get(this.browserContext);
				if (passNumValue != null) {
					TestResult.PassNum.put(this.browserContext, passNumValue + 1);
				}
	
				String strPassTime = CReporter.getTime();
				writer.write("<td><small>" + strPassTime + "</small></td> ");
				writer.write("</tr> " + ReporterConstants.NEW_LINE);
				writer.close();
	
			} catch (Exception e) {
				LOG.info("Exception Encountered : " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * @param strStepName
	 * @param strStepDes
	 * @param fileName
	 * @throws Throwable 
	 */
	private void onFailure(String strStepName, String strStepDes,
			String fileName) throws Throwable {
		
		String href = "./"
				+ fileName
						.substring(
								fileName.indexOf(ReporterConstants.FOLDER_SCREENRSHOTS),
								fileName.length());
		String imgSrc = "'." + File.separatorChar
				+ ReporterConstants.FOLDER_SCREENRSHOTS + File.separatorChar
				+ this.getFileName(ReporterConstants.LOCATION_FAILED_LOGO)
				+ "'";
		Writer writer = null;
		try {
			File file = new File(this.filePath() + File.separatorChar
					+ TestResult.strTestName.get(this.browserContext)
					+ "_Results" +
					/* + TestResult.timeStamp + */".html");// "SummaryReport.html"

			writer = new FileWriter(file, true);
			Integer stepNumValue = TestResult.stepNum.get(this.browserContext);
			if (stepNumValue != null) {

				TestResult.stepNum.put(this.browserContext, stepNumValue + 1);
			}

			writer.write("<tr class='content2' >");
			writer.write("<td>" + TestResult.stepNum.get(this.browserContext)
					+ "</td> ");
			writer.write("<td class='left' style=\"color:#FF0000\";>" + strStepName + "</td>");
			writer.write("<td class='left' style=\"color:#FF0000\";>" + strStepDes + "</td> ");

			Integer failNumValue = TestResult.FailNum.get(this.browserContext);
			if (stepNumValue != null) {

				TestResult.FailNum.put(this.browserContext, failNumValue + 1);
			}

			/*
			 * writer.write("<td class='Fail' align='center'><a  href='"+
			 * "./Screenshots"+"/" + strStepDes.replace(" ", "_") + "_" +
			 * TestResult.timeStamp + ".jpeg'"+
			 * " alt= Screenshot  width= 15 height=15 style='text-decoration:none;'><img  src='./Screenshots/failed.ico' width='18' height='18'/></a></td>"
			 * );
			 */
			// New Screen shot code to avoid overriding \\\\
			writer.write("<td class='Fail' align='center'><a target='_blank' href='"
					+ href
					+ "'"
					+ " alt= Screenshot  width= 15 height=15 style='text-decoration:none;'><img  src="
					+ imgSrc + "height='18'/></a></td>");

			String strFailTime = CReporter.getTime();
			writer.write("<td><small>" + strFailTime + "</small></td> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);
			writer.close();
			String strPackageName = TestResult.packageName
					.get(this.browserContext);
			String strTcName = TestResult.tc_name.get(this.browserContext);
			if (!TestResult.mapBrowserContextTestCaseRef
					.get(this.browserContext)
					.get(strPackageName + ":" + strTcName).equals("PASS")) {
				TestResult.mapBrowserContextTestCaseRef
						.get(this.browserContext).put(
								strPackageName + ":" + strTcName, "FAIL");
			}
		} catch (Exception e) {
			LOG.info("Exception Encountered : " + e.getMessage());
			e.printStackTrace();
		}

	}
	
	
	private void onWarning(String strStepName, String strStepDes) {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) 
		{
			Writer writer = null;
			try {
				
				File file = new File(this.filePath() + File.separatorChar
						+ TestResult.strTestName.get(this.browserContext)
						+ "_Results"
						/* + TestResult.timeStamp */+ ".html");// "SummaryReport.html"
				String imgSrc = "'." + File.separatorChar
						+ ReporterConstants.FOLDER_SCREENRSHOTS
						+ File.separatorChar
						+ this.getFileName(ReporterConstants.LOCATION_WARNING_LOGO)
						+ "'";
				writer = new FileWriter(file, true);
				Integer stepNumValue = TestResult.stepNum.get(this.browserContext);
				if (stepNumValue != null) {
					TestResult.stepNum.put(this.browserContext, stepNumValue + 1);
				}
	
				writer.write("<tr class='content2' >" + ReporterConstants.NEW_LINE);
				writer.write("<td>" + TestResult.stepNum.get(this.browserContext)
						+ "</td> ");
				writer.write("<td class='left'>" + strStepName + "</td>");
				writer.write("<td class='left'>" + strStepDes + "</td> ");
				// TestResult.FailNum = TestResult.FailNum + 1;
	
				writer.write("<td class='Fail'  align='center'><a target='_blank' href='"
						+ "."
						+ File.separatorChar
						+ ReporterConstants.FOLDER_SCREENRSHOTS
						+ File.separatorChar
						+ strStepDes /*.replace(" ", "_") 
														 * + "_" +
														 * TestResult.timeStamp
														 */
						+ ".jpeg'"
						+ " alt= Screenshot  width= 15 height=15 style='text-decoration:none;'><img  src="
						+ imgSrc + " width='18' height='18'/></a></td>");
	
				String strFailTime = CReporter.getTime();
				writer.write("<td><small>" + strFailTime + "</small></td> ");
				writer.write("</tr> " + ReporterConstants.NEW_LINE);
				writer.close();
	
			} catch (Exception e) {
				LOG.info("Exception Encountered : " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private void testHeader(String testName, boolean append) {
		Writer writer = null;

		try {
			String imgSrcClientLogo = "." + File.separatorChar
					+ ReporterConstants.FOLDER_SCREENRSHOTS
					+ File.separatorChar
					+ this.getFileName(ReporterConstants.LOCATION_CLIENT_LOGO);
			String imgSrcCompanyLogo = "." + File.separatorChar
					+ ReporterConstants.FOLDER_SCREENRSHOTS
					+ File.separatorChar
					+ this.getFileName(ReporterConstants.LOCATION_COMPANY_LOGO);
			TestResult.strTestName.put(this.browserContext, testName);
			File file = new File(this.filePath() + File.separatorChar
					+ TestResult.strTestName.get(this.browserContext)
					+ "_Results"
					/* + TestResult.timeStamp */+ ".html");// "Results.html"
			writer = new FileWriter(file, append);

			writer.write("<!DOCTYPE html> ");
			writer.write("<html>");
			writer.write("<head> ");
			writer.write("<meta charset='UTF-8'> ");
			writer.write("<title>"
					+ TestResult.strTestName.get(this.browserContext)
					+ " Execution Results</title> ");

			// Jquery java script
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/datatable/jquery.dataTables.css'>" + ReporterConstants.NEW_LINE);
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/jquery-ui.css'>" + ReporterConstants.NEW_LINE);
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/datatable/dataTables.jqueryui.css'>" + ReporterConstants.NEW_LINE);
			writer.write("<link rel='stylesheet' type='text/css' href='Screenshots/css/custom.css'>" + ReporterConstants.NEW_LINE);

			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/jquery-1.11.1.min.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/datatable/jquery.dataTables.min.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/datatable/dataTables.jqueryui.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/jquery-ui.min.js'></script>" + ReporterConstants.NEW_LINE);
			writer.write("<script type='text/javascript' language='javascript' src='Screenshots/js/custom.js'></script>" + ReporterConstants.NEW_LINE);

			writer.write("<style type='text/css'> " + ReporterConstants.NEW_LINE);
			writer.write("body { ");
			writer.write("background-color: #FFFFFF; ");
			writer.write("font-family: Verdana, Geneva, sans-serif; ");
			writer.write("text-align: left; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("small { ");
			writer.write("font-size: 0.7em; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("table { ");
			writer.write("box-shadow: 9px 9px 10px 4px #BDBDBD;");
			writer.write("border: 0px solid #4D7C7B; ");
			writer.write("border-collapse: collapse; ");
			writer.write("border-spacing: 0px; ");
			writer.write("width: 1000px; ");
			writer.write("margin-left: auto; ");
			writer.write("margin-right: auto; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.heading { ");
			writer.write("background-color: #041944; ");
			writer.write("color: #FFFFFF; ");
			writer.write("font-size: 0.7em; ");
			writer.write("font-weight: bold; ");
			writer.write("background:-o-linear-gradient(bottom, #999999 5%, #000000 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #999999), color-stop(1, #000000) );");
			writer.write("background:-moz-linear-gradient( center top, #999999 5%, #000000 100% );");
			writer.write("filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#999999, endColorstr=#000000);	background: -o-linear-gradient(top,#999999,000000);");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.subheading { ");
			writer.write("background-color: #FFFFFF; ");
			writer.write("color: #000000; ");
			writer.write("font-weight: bold; ");
			writer.write("font-size: 0.7em; ");
			writer.write("text-align: justify; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.section { ");
			writer.write("background-color: #A4A4A4; ");
			writer.write("color: #333300; ");
			writer.write("cursor: pointer; ");
			writer.write("font-weight: bold; ");
			writer.write("font-size: 0.7em; ");
			writer.write("text-align: justify; ");
			writer.write("background:-o-linear-gradient(bottom, #56aaff 5%, #e5e5e5 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #56aaff), color-stop(1, #e5e5e5) );");
			writer.write("background:-moz-linear-gradient( center top, #56aaff 5%, #e5e5e5 100% );");
			writer.write("filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#56aaff, endColorstr=#e5e5e5);	background: -o-linear-gradient(top,#56aaff,e5e5e5);");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.subsection { ");
			writer.write("cursor: pointer; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("tr.content { ");
			writer.write("background-color: #FFFFFF; ");
			writer.write("color: #000000; ");
			writer.write("font-size: 0.7em; ");
			writer.write("display: table-row; ");
			writer.write("} ");

			writer.write("tr.content2 { ");
			writer.write("background-color: #E1E1E1; ");
			writer.write("border: 1px solid #4D7C7B;");
			writer.write("color: #000000; ");
			writer.write("font-size: 0.75em; ");
			writer.write("display: table-row; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td, th { ");
			writer.write("padding: 5px; ");
			writer.write("border: 1px solid #4D7C7B; ");
			writer.write("text-align: inherit\0/; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("th.Logos { ");
			writer.write("padding: 5px; ");
			writer.write("border: 0px solid #4D7C7B; ");
			writer.write("text-align: inherit /;");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.left { ");
			writer.write("text-align: justify; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.pass { ");
			writer.write("font-weight: bold; ");
			writer.write("color: green; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.fail { ");
			writer.write("font-weight: bold; ");
			writer.write("color: red; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.done, td.screenshot { ");
			writer.write("font-weight: bold; ");
			writer.write("color: black; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.debug { ");
			writer.write("font-weight: bold;");
			writer.write("color: blue; ");
			writer.write("} " + ReporterConstants.NEW_LINE);

			writer.write("td.warning { ");
			writer.write("font-weight: bold; ");
			writer.write("color: orange; ");
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("</style> " + ReporterConstants.NEW_LINE);

			writer.write("<script> " + ReporterConstants.NEW_LINE);
			writer.write("function toggleMenu(objID) { " + ReporterConstants.NEW_LINE);
			writer.write("if (!document.getElementById) return; " + ReporterConstants.NEW_LINE);
			writer.write("var ob = document.getElementById(objID).style; " + ReporterConstants.NEW_LINE);
			writer.write("if(ob.display === 'none') { " + ReporterConstants.NEW_LINE);
			writer.write("try { " + ReporterConstants.NEW_LINE);
			writer.write("ob.display='table-row-group'; " + ReporterConstants.NEW_LINE);
			writer.write("} catch(ex) { " + ReporterConstants.NEW_LINE);
			writer.write("ob.display='block'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("else { " + ReporterConstants.NEW_LINE);
			writer.write("ob.display='none'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("function toggleSubMenu(objId) { " + ReporterConstants.NEW_LINE);
			writer.write("for(i=1; i<10000; i++) { " + ReporterConstants.NEW_LINE);
			writer.write("var ob = document.getElementById(objId.concat(i)); " + ReporterConstants.NEW_LINE);
			writer.write("if(ob === null) { " + ReporterConstants.NEW_LINE);
			writer.write("break; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("if(ob.style.display === 'none') { ");
			writer.write("try { " + ReporterConstants.NEW_LINE);
			writer.write("ob.style.display='table-row'; " + ReporterConstants.NEW_LINE);
			writer.write("} catch(ex) { " + ReporterConstants.NEW_LINE);
			writer.write("ob.style.display='block'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("else { " + ReporterConstants.NEW_LINE);
			writer.write("ob.style.display='none'; " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("} " + ReporterConstants.NEW_LINE);
			writer.write("</script> " + ReporterConstants.NEW_LINE);
			writer.write("</head> " + ReporterConstants.NEW_LINE);

			writer.write(" <body> " + ReporterConstants.NEW_LINE);
			writer.write("</br>" + ReporterConstants.NEW_LINE);

			writer.write("<table id='Logos' class='testData'>");
			writer.write("<colgroup>");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("</colgroup> ");
			writer.write("<thead> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='content'>");
			writer.write("<th class ='Logos' colspan='2' >");
			writer.write("<img align ='left' src= " + imgSrcClientLogo
					+ "></img>");
			writer.write("</th>");
			writer.write("<th class = 'Logos' colspan='2' > ");
			writer.write("<img align ='right' src= " + imgSrcCompanyLogo
					+ "></img>");
			writer.write("</th> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);
			writer.write("</thead> " + ReporterConstants.NEW_LINE);
			writer.write("</table> " + ReporterConstants.NEW_LINE);

			writer.write("<table id='header' class='testData'> ");
			writer.write("<colgroup> ");
			writer.write("<col style='width: 25%' /> ");
			writer.write("<col style='width: 25%' /> ");
			writer.write("<col style='width: 25%' /> ");
			writer.write("<col style='width: 25%' /> ");
			writer.write("</colgroup> " + ReporterConstants.NEW_LINE);

			writer.write(" <thead> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='heading'> ");
			writer.write("<th colspan='4' style='font-family:Copperplate Gothic Bold; font-size:1.4em;'> ");
			writer.write("**" + TestResult.strTestName.get(this.browserContext)
					+ " Execution Results **");
			writer.write("</th> " + ReporterConstants.NEW_LINE);
			writer.write("</tr> " + ReporterConstants.NEW_LINE);
			writer.write("<tr class='subheading'> ");
			writer.write("<th>&nbsp;Date&nbsp;&&nbsp;Time&nbsp;:&nbsp;</th> ");

			writer.write("<th>" + CReporter.dateTime() + "</th> ");
			writer.write("<th>Iteration&nbsp;Mode&nbsp;:&nbsp;</th> ");
			writer.write("<th>" + ReporterConstants.ITERAION_MODE + "</th> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='subheading'> ");
			/*writer.write("<th>Device&nbsp;Name&nbsp;:</th> ");
			writer.write("<th>" + ReporterConstants.DEVICE_NAME + "</th> ");*/
			writer.write("<th>Platform:</th> ");
			writer.write("<th>" + /*ReporterConstants.DEVICE_NAME*/ this.browserContext.getBrowserPlatform() + "</th> ");
			writer.write("<th>Version&nbsp;:</th> ");
			writer.write("<th>" + this.browserContext.getBrowserVersion() + "</th> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);

			writer.write("<tr class='subheading'> ");
			writer.write("<th>Browser&nbsp;:&nbsp;</th> ");
			writer.write("<th>" + this.browserContext.getBrowserName()
					+ "</th> ");
			writer.write(" <th>Executed&nbsp;on&nbsp;:&nbsp;</th> ");
			writer.write("<th>" + InetAddress.getLocalHost().getHostName()
					+ "</th> ");
			writer.write("</tr> " + ReporterConstants.NEW_LINE);
			writer.write("</thead> " + ReporterConstants.NEW_LINE);
			writer.write("</table> " + ReporterConstants.NEW_LINE);

			writer.write("<div class='mainTableDiv4'>" + ReporterConstants.NEW_LINE);
			writer.write("<table id='main' class='testData'> ");
			writer.write("<colgroup> ");
			writer.write("<col style='width: 3%' /> ");
			writer.write("<col style='width: 28%' /> ");
			writer.write("<col style='width: 51%' /> ");
			writer.write("<col style='width: 8%' /> ");
			writer.write("<col style='width: 10%' /> ");
			writer.write("</colgroup> ");
			writer.write("<thead> ");
			writer.write("<tr class='heading'> ");
			writer.write("<th>S.NO</th> ");
			writer.write("<th>Steps</th> ");
			writer.write("<th>Details</th> ");
			writer.write("<th>Status</th> ");
			writer.write("<th>Time</th> ");
			writer.write("</tr> ");
			writer.write("</thead> " + ReporterConstants.NEW_LINE);
			writer.close();

			String strPackageName = TestResult.packageName
					.get(this.browserContext);
			String strTcName = TestResult.tc_name.get(this.browserContext);

			/* get test case status map */
			Map<String, String> mapTestCaseStatus = TestResult.mapBrowserContextTestCaseRef
					.get(this.browserContext);
			if (mapTestCaseStatus == null) {
				mapTestCaseStatus = new HashMap<String, String>();
			}

			mapTestCaseStatus.put(strPackageName + ":" + strTcName, "status");
			TestResult.mapBrowserContextTestCaseRef.put(this.browserContext,
					mapTestCaseStatus);
		} catch (Exception e) {

		} finally {
			try {
				writer.flush();
				writer.close();
			} catch (Exception e) {

			}
		}
	}

	private void reportStep(String StepDesc) throws IOException {
		//StepDesc = StepDesc.replaceAll(" ", "_");

		File file = new File(this.filePath() + File.separatorChar
				+ TestResult.strTestName.get(this.browserContext) + "_Results"
				/* + TestResult.timeStamp */+ ".html");// "SummaryReport.html"
		Writer writer = null;

		try {
			writer = new FileWriter(file, true);
			Integer bFunctionNo = TestResult.BFunctionNo
					.get(this.browserContext);
			if (bFunctionNo != null && bFunctionNo > 0) {
				writer.write("</tbody>");
			}
			writer.write("<tbody>" + ReporterConstants.NEW_LINE);
			writer.write("<tr class='section'> ");
			writer.write("<td colspan='5' onclick=toggleMenu('" + StepDesc
					+ TestResult.stepNum.get(this.browserContext) + "')>+ "
					+ StepDesc + "</td>");
			writer.write("</tr> ");
			writer.write("</tbody>");
			writer.write("<tbody id='" + StepDesc
					+ TestResult.stepNum.get(this.browserContext)
					+ "' style='display:table-row-group'>" + ReporterConstants.NEW_LINE);
			writer.close();

			TestResult.BFunctionNo.put(this.browserContext, bFunctionNo + 1);
		} catch (Exception e) {

		}
	}

	public void closeDetailedReport() throws IOException {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) 
		{
			File file = new File(this.filePath() + File.separatorChar
					+ TestResult.strTestName.get(this.browserContext) + "_Results"
					/* + TestResult.timeStamp */+ ".html");// "SummaryReport.html"
			Writer writer = null;
	
			try {
				writer = new FileWriter(file, true);
				writer.write("</table></div>" + ReporterConstants.NEW_LINE);
				writer.write("<table id='footer' class='testData'>");
				writer.write("<colgroup>");
				writer.write("<col style='width: 25%' />");
				writer.write("<col style='width: 25%' />");
				writer.write("<col style='width: 25%' />");
				writer.write("<col style='width: 25%' />");
				writer.write("</colgroup>" + ReporterConstants.NEW_LINE);
				writer.write("<tfoot>" + ReporterConstants.NEW_LINE);
				writer.write("<tr class='heading'> " + ReporterConstants.NEW_LINE);
				writer.write("<th colspan='4'>Execution Time In Seconds (Includes Report Creation Time) : "
						+ TestResult.executionTime.get(this.browserContext).get(
								TestResult.tc_name.get(this.browserContext))
						+ "&nbsp;</th> ");
				writer.write("</tr> " + ReporterConstants.NEW_LINE);
				writer.write("<tr class='content'>");
				writer.write("<td class='pass'>&nbsp;Steps Passed&nbsp;:</td>");
				writer.write("<td class='pass'> "
						+ TestResult.PassNum.get(this.browserContext) + "</td>");
				writer.write("<td class='fail'>&nbsp;Steps Failed&nbsp;: </td>");
				writer.write("<td class='fail'>"
						+ TestResult.FailNum.get(this.browserContext) + "</td>");
				writer.write("</tr>" + ReporterConstants.NEW_LINE);
				writer.close();
			} catch (Exception e) {
	
			}
		}
	}

	public void closeSummaryReport() throws IOException {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) 
		{		
		
			File file = new File(this.filePath() + File.separatorChar
					+ "SummaryResults" 
					/* + TestResult.timeStamp */+ ".html");// "SummaryReport.html"
			Writer writer = null;
			try {
				// get pass/fail test cases count
				Integer passTestCasesCount = TestResult.passCounter
						.get(this.browserContext) == null ? 0
						: TestResult.passCounter.get(this.browserContext);
				Integer failTestCasesCount = TestResult.failCounter
						.get(this.browserContext) == null ? 0
						: TestResult.failCounter.get(this.browserContext);
	
				//
				writer = new FileWriter(file, true);
	
				writer.write("<table id='footer' class='testData'>" + ReporterConstants.NEW_LINE);
				writer.write("<colgroup>");
				writer.write("<col style='width: 25%' />");
				writer.write("<col style='width: 25%' />");
				writer.write("<col style='width: 25%' />");
				writer.write("<col style='width: 25%' /> ");
				writer.write("</colgroup> ");
				writer.write("<tfoot>" + ReporterConstants.NEW_LINE);
				writer.write("<tr class='heading'>");
				writer.write("<th colspan='4'>Total Duration  In Seconds (Including Report Creation) : "
						+ (int) ((double) TestResult.iSuiteExecutionTime
								.get(this.browserContext)) + "</th>");
				writer.write("</tr>" + ReporterConstants.NEW_LINE);
				writer.write("<tr class='content'>");
				writer.write("<td class='pass'>&nbsp;Tests Passed&nbsp;:</td>" + ReporterConstants.NEW_LINE);
	
				// entry for pass test cases count
				writer.write("<td class='pass'> " + passTestCasesCount + "</td>" + ReporterConstants.NEW_LINE);
				writer.write("<td class='fail'>&nbsp;Tests Failed&nbsp;:</td>" + ReporterConstants.NEW_LINE);
	
				// entry for fail test cases count
				writer.write("<td class='fail'> " + failTestCasesCount + "</td> " + ReporterConstants.NEW_LINE);
				writer.write("</tr>" + ReporterConstants.NEW_LINE);
				writer.write("</tfoot>" + ReporterConstants.NEW_LINE);
				writer.write("</table> " + ReporterConstants.NEW_LINE);
	
				writer.close();
			} catch (Exception e) {
	
			}
		}
	}

	/* copied from CReporter */
	private static String dateStamp() {
		DateFormat dateFormat = new SimpleDateFormat();
		Date date = new Date();
		return dateFormat.format(date).substring(0, 7);
	}

	//
	private static String dateTime() {
		Date todaysDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(
				"dd-MM-yyyy HH:mm:ss a");
		String formattedDate = formatter.format(todaysDate);
		return formattedDate;

	}

	private static String getTime() {
		Date todaysDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss a");
		String formattedDate = formatter.format(todaysDate);
		return formattedDate;

	}

	// return time

	// return time and date
	private static String timeStamp() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime()).toString();
	}

	// return environmental details
	private static String osEnvironment() {

		return "Current suit exicuted on : " + System.getProperty("os.name")
				+ "/version : " + System.getProperty("os.version")
				+ "/Architecture : " + System.getProperty("os.arch");
	}

	private static String getHostName() throws UnknownHostException {
		InetAddress addr = InetAddress.getLocalHost();
		String hostname = addr.getHostName();

		return hostname;
	}

	public void calculateTestCaseStartTime() {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) 
		{
			TestResult.iStartTime.put(this.browserContext,
					System.currentTimeMillis());
		}
	}

	/***
	 * This method is supposed to be used in the @AfterMethod to calculate the
	 * total test case execution time to show in Reports by taking the start
	 * time from the calculateTestCaseStartTime method.
	 */
	public void calculateTestCaseExecutionTime() {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) 
		{		
			TestResult.iEndTime
					.put(this.browserContext, System.currentTimeMillis());
			Long iExecutionTimeValue = TestResult.iEndTime.get(this.browserContext)
					- TestResult.iStartTime.get(this.browserContext);
			TestResult.iExecutionTime.put(this.browserContext, iExecutionTimeValue);
			long execTimeInSecs = TimeUnit.MILLISECONDS
					.toSeconds(TestResult.iExecutionTime.get(this.browserContext));
			String testCaseName = TestResult.tc_name.get(this.browserContext);
			Map<String, String> mapTCExecTime = TestResult.executionTime
					.get(this.browserContext);
			if (mapTCExecTime == null) {
				mapTCExecTime = new HashMap<String, String>();
			}
			mapTCExecTime.put(testCaseName, String.valueOf(execTimeInSecs));
			TestResult.executionTime.put(this.browserContext, mapTCExecTime);
		}
	}

	/***
	 * This method is supposed to be used in the @BeforeSuite in-order trigger
	 * the Suite Start Time which inturn used to calculate the Total Suite
	 * execution time to show in Reports.
	 */
	public void calculateSuiteStartTime() {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)) 
		{
			TestResult.iSuiteStartTime.put(this.browserContext,
				System.currentTimeMillis()); // Newly added
		}
	}

	/***
	 * This method is supposed to be used in the @AfterMethod to calculate the
	 * total suite execution time to show in Reports by taking the suite start
	 * time from the calculateSuiteStartTime method.
	 */
	public void calculateSuiteExecutionTime() {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT))
		{
	        TestResult.iSuiteEndTime.put(this.browserContext,
	                System.currentTimeMillis()); // Newly added
	        System.out.println("Start Time****"+TestEngine.startTime);
	        System.out.println("End Time is:*********"+TestResult.iSuiteEndTime
	                .get(this.browserContext));
	       // double dblSuiteexecTime = (TestResult.iSuiteEndTime.get(this.browserContext) - TestEngineWeb.startTime) / 1000.000;
	        double dblSuiteexecTime = (TestResult.iSuiteEndTime.get(this.browserContext) - TestEngine.startTime) / 1000.000;
	        Double DoubleSuiteExecTime = new Double(dblSuiteexecTime);
	        TestResult.iSuiteExecutionTime.put(this.browserContext,
	                DoubleSuiteExecTime);
		}
    }



	private void reportCreater() throws Throwable {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT))
		{		
			this.htmlCreateReport();
		}
	}

	public void SuccessReport(String strStepName, String strStepDes)
			throws Throwable {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT))
		{	
			if(ReporterConstants.REPORT_FORMAT.equalsIgnoreCase("html"))
			{
				/* take screen shot if Screenshot is required for passed test cases */
				if (ReporterConstants.BOOLEAN_ONSUCCESS_SCREENSHOT == true) {
					/*
					 * WebDriver webDriver = WebDriverFactory.getWebDriver(null,
					 * this
					 * .testContext.getCurrentXmlTest().getParameter("browser"));
					 * ActionEngine.screenShot(webDriver ,
					 * testUtil.filePath()+"/"+"Screenshots"+"/" +
					 * strStepDes.replace(" ", "_") + "_" + TestEngine.timeStamp +
					 * ".jpeg");
					 */
				}
				this.onSuccess(strStepName, strStepDes);
			}
		}
		else if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT))		
		{
				/* take screen shot if Screenshot is required for passed test cases */
				if (ReporterConstants.BOOLEAN_ONSUCCESS_SCREENSHOT == true) {
					/*
					 * WebDriver webDriver = WebDriverFactory.getWebDriver(null,
					 * this
					 * .testContext.getCurrentXmlTest().getParameter("browser"));
					 * ActionEngine.screenShot(webDriver ,
					 * testUtil.filePath()+"/"+"Screenshots"+"/" +
					 * strStepDes.replace(" ", "_") + "_" + TestEngine.timeStamp +
					 * ".jpeg");
					 */
				}
				this.onSuccess(strStepName, strStepDes);
			}
	}

	//For Regular report calling which was used in most of the cases
	public void failureReport(String strStepName, String strStepDes, 
			WebDriver... webDrivers)throws Throwable {
        	this.failureReport(strStepName,strStepDes, false, false, webDrivers);			
	}
			
	public void failureReport(String strStepName, String strStepDes, Boolean doNotThrowException, Boolean captureDesktopScreenshot,
			WebDriver... webDrivers) throws Throwable {
		String fileName = "";
		//String reportDescription = strStepDes;
		fileName = makeUniqueImagePath("jpeg");
/*		reportDescription = reportDescription.replaceAll(":", "_");
		reportDescription = reportDescription.replaceAll(",", "_");
		reportDescription = reportDescription.replaceAll("&", "_");
		reportDescription = reportDescription.replaceAll(" ", "_");			
*/		if(webDrivers!=null){
			for (WebDriver webDriver : webDrivers) {
				//commented by swarup as saucelab gives problem
				this.screenShot(captureDesktopScreenshot, webDriver, fileName);
				break;
			}
		}
		else
		{
			this.screenShot(captureDesktopScreenshot, null, fileName);
		}		
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
			this.onFailure(strStepName, strStepDes, fileName);
		}
		else if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT))
		{
			/*Sreekanth Extent Reports*/
			try
			{
				
				String extentFailureScreenShot = "./" + fileName.substring(fileName.lastIndexOf(IFrameworkConstant.RESULTS_FOLDER ) + (IFrameworkConstant.RESULTS_FOLDER).length()+1).replace(File.separator, "/");
				System.out.println(extentFailureScreenShot);
				this.extentLogger.fail("<span style='color:#FF0000';><u>Step </u>: " + strStepName + " <br><u>Step Description</u> : " + strStepDes + "</span>", MediaEntityBuilder.createScreenCaptureFromPath(extentFailureScreenShot, strStepName).build());
			}catch (Exception Ex)
			{
				Ex.printStackTrace();
				throw Ex;
			}
		}
		if(!doNotThrowException){
			throw new Exception(strStepDes);
		}
	}

	
	public void warningReport(String strStepName, String strStepDes)
			throws Throwable {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.EXTENTREPORT)){
			/*Sreekanth Extent Report*/
			this.extentLogger.warning("<span style='color:#ffa31a';><u>Step </u>: " + strStepName + " <br><u>Step Description</u> : " + strStepDes + "</span>", MediaEntityBuilder.createScreenCaptureFromPath("../22a955e5-0e2b-4b74-bb14-5a06d4f622a4.jpg", strStepName).build());
		}
		else if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
			
			if(ReporterConstants.REPORT_FORMAT.equalsIgnoreCase("html"))
			{
	
				/* logic to take screen shot */
				/*
				 * WebDriver webDriver = WebDriverFactory.getWebDriver(null,
				 * this.testContext.getCurrentXmlTest().getParameter("browser"));
				 * ActionEngine
				 * .screenShot(webDriver,testUtil.filePath()+"/"+"Screenshots"+"/" +
				 * strStepDes.replace(" ", "_") + "_" + TestEngine.timeStamp +
				 * ".jpeg");
				 */
	
				this.onWarning(strStepName, strStepDes);
			}
			else
			{
			/* logic to take screen shot */
				/*
				 * WebDriver webDriver = WebDriverFactory.getWebDriver(null,
				 * this.testContext.getCurrentXmlTest().getParameter("browser"));
				 * ActionEngine
				 * .screenShot(webDriver,testUtil.filePath()+"/"+"Screenshots"+"/" +
				 * strStepDes.replace(" ", "_") + "_" + TestEngine.timeStamp +
				 * ".jpeg");
				 */
	
				this.onWarning(strStepName, strStepDes);
			}
		}
	}

	// New Screen shot code to avoid overriding \\\\

	public String makeUniqueImagePath(String fileExtention) {
		UUID uuID = UUID.randomUUID();
		String newFileName = "";
		String fileName = this.filePath() + File.separatorChar
		+ ReporterConstants.FOLDER_SCREENRSHOTS
		+ File.separatorChar + uuID.toString();
				
		try {
			// Verifying if the file already exists, if so append the numbers
			// 1,2 so on to the fine name.
			newFileName = fileName + "." + fileExtention;
			File myPngImage = new File(newFileName);
			int counter = 1;
			while (myPngImage.exists()) {
				newFileName = fileName + "_" + counter + "." + fileExtention;
				myPngImage = new File(newFileName);
				counter++;
			}
			return newFileName;
		} catch (Exception e) {
			e.printStackTrace();
			return newFileName;
		}
	}

	private void screenShot(Boolean takeDesktopScreenshot, WebDriver driver, String fileName) {
		Boolean tookScreenShot=false;
		if(driver!=null && takeDesktopScreenshot!=true)
		{
			try {
				WebDriver driverScreenShot = new Augmenter().augment(driver);
				File scrFile = ((TakesScreenshot) driverScreenShot).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(fileName));
				tookScreenShot=true;
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		if((takeDesktopScreenshot==true) || tookScreenShot==false)
		{
			try
			{
				//--------------------------------------------
				Thread.sleep(3000);/*DO NOT REMOVE THIS HARD SLEEP ELSE SCREENSHOT WILL NOT BE TAKEN*/
				//--------------------------------------------
				//taking screenshot of the browser window
				BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
				ImageIO.write(image, "jpeg", new File(fileName));
				System.out.println("I am in Desktop");
			}catch (Exception e) {
				e.printStackTrace();
			}
		}				
	}
	

	public void updateTestCaseStatus() {
		if(IFrameworkConstant.REPORTING_TYPE.equalsIgnoreCase(IFrameworkConstant.REPORT_TYPE.CUSTOMREPORT)){
			
			if (TestResult.FailNum.get(this.browserContext) != 0) 
			{
				Integer failCount = TestResult.failCounter.get(this.browserContext) == null ? 1
						: TestResult.failCounter.get(this.browserContext) + 1;
				TestResult.failCounter.put(this.browserContext, failCount);
				Map<String, String> mapResult = TestResult.testResults
						.get(this.browserContext);
				if (mapResult == null) {
					mapResult = new HashMap<String, String>();
				}
				mapResult.put(TestResult.tc_name.get(this.browserContext),
						ReporterConstants.TEST_CASE_STATUS_FAIL);
				TestResult.testResults.put(this.browserContext, mapResult);
			}
			else {
				Integer passCount = TestResult.passCounter.get(this.browserContext) == null ? 1
						: TestResult.passCounter.get(this.browserContext) + 1;
				TestResult.passCounter.put(this.browserContext, passCount);
				Map<String, String> mapResult = TestResult.testResults
						.get(this.browserContext);
				if (mapResult == null) {
					mapResult = new HashMap<String, String>();
				}
				mapResult.put(TestResult.tc_name.get(this.browserContext),
						ReporterConstants.TEST_CASE_STATUS_PASS);
				TestResult.testResults.put(this.browserContext, mapResult);
			}
		}
	}

	private void initExtentTestLogger(ExtentTest extentTestLoggerObject){
		extentLogger=extentTestLoggerObject;
	}

}