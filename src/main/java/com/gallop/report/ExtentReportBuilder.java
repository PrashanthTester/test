package com.gallop.report;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.*;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReportBuilder {
	private static ExtentReports extent;
    private static ThreadLocal parentTest = new ThreadLocal();
    private static ThreadLocal test = new ThreadLocal();
    
	public ExtentReportBuilder() {
		// TODO Auto-generated constructor stub - Sreekanth To Add Code here
	}
	
	@BeforeSuite
	public void beforeSuite(String suiteName, String resultsFilePath) {
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(resultsFilePath);
		this.extent = ExtentManager.createInstance(htmlReporter, resultsFilePath, suiteName);
		
		htmlReporter.setAppendExisting(true);
		this.extent.attachReporter(htmlReporter);	
	}
	
    @BeforeClass
    public synchronized ExtentTest beforeClass(String className) {
        ExtentTest parent = extent.createTest(className);
        this.parentTest.set(parent);
        return parent;
    }

    @BeforeMethod
    public synchronized ExtentTest beforeMethod(Method method) {
        ExtentTest child =  ((ExtentTest) this.parentTest.get()).createNode(method.getName());
        this.test.set(child);
        return child;
    }

    @AfterMethod
    public synchronized void afterMethod(ITestResult result) {    	
        if (result.getStatus() == ITestResult.FAILURE)
            ((ExtentTest) this.test.get()).fail(result.getThrowable());
        else if (result.getStatus() == ITestResult.SKIP)
            ((ExtentTest) this.test.get()).skip(result.getThrowable());
        else
            ((ExtentTest) this.test.get()).pass("Test passed");
        
        extent.flush();
    }

    public static class ExtentManager {
        
        private static ExtentReports extent=null;
        
        public ExtentReports getInstance() {
        	if (extent == null)
        		//createInstance("test-output/extent.html", "Dummy");
        		System.out.println("You have to uncomment code in ExtentReportBuilder.getInstance");
            return extent;
        }
        
        public static ExtentReports createInstance(ExtentHtmlReporter htmlReporter, String fileName, String suiteName) {
            //ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
            htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
            htmlReporter.config().setChartVisibilityOnOpen(true);
            htmlReporter.config().setTheme(Theme.DARK);
            htmlReporter.config().setDocumentTitle(suiteName);
            htmlReporter.config().setEncoding("utf-8");
            htmlReporter.config().setReportName(suiteName);
            
            extent = new ExtentReports();
            extent.attachReporter(htmlReporter);
            
            return extent;
        }
    }
}
