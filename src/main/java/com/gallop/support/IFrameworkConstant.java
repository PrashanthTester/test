package com.gallop.support;

import com.gallop.report.ConfigFileReadWrite;

public interface IFrameworkConstant {	
	String FILE_FRAMEWORK = "resources/Framework.properties";
	String LOCATION_DATATABLE_EXCEL = ConfigFileReadWrite.read(FILE_FRAMEWORK, "LOCATION_DATATABLE_EXCEL");
	long WAIT_TIME_MAX_PAGE = Long.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "WAIT_TIME_MAX_PAGE"));
	int WAIT_TIME_MAXIMUM = Integer.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "WAIT_TIME_MAXIMUM"));
	int EXPLICIT_WAIT_TIME = Integer.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "EXPLICIT_WAIT_TIME"));
	int WAIT_TIME_MIN = Integer.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "WAIT_TIME_MIN"));
	int WAIT_TIME_MEDIUM = Integer.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "WAIT_TIME_MEDIUM"));
	
	String APPLICATION_URL = ConfigFileReadWrite.read(FILE_FRAMEWORK, "APPLICATION_URL");
	String APPLICATION_USER_ID = ConfigFileReadWrite.read(FILE_FRAMEWORK, "APPLICATION_USER_ID");
	String APPLICATION_PASSWORD = ConfigFileReadWrite.read(FILE_FRAMEWORK, "APPLICATION_PASSWORD");
	
	String RESULTS_FOLDER = ConfigFileReadWrite.read(FILE_FRAMEWORK, "RESULTS_FOLDER");
	Boolean CAPTURE_BROWSER_CONSOLE_LOGS = Boolean.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "CAPTURE_BROWSER_CONSOLE_LOGS"));
	String CAPTURE_BROWSER_CONSOLE_LOG_FILEPATH = ConfigFileReadWrite.read(FILE_FRAMEWORK, "CAPTURE_BROWSER_CONSOLE_LOG_FILEPATH");
	Boolean CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_FAIL = Boolean.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_FAIL"));
	Boolean CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_WARN = Boolean.valueOf(ConfigFileReadWrite.read(FILE_FRAMEWORK, "CAPTURE_BROWSER_CONSOLE_LOG_ONERROR_WARN"));
	
	//Only for iOS
	String IOS_UDID=ConfigFileReadWrite.read(FILE_FRAMEWORK, "IOS_UDID");
	String IOS_IPAPATH=ConfigFileReadWrite.read(FILE_FRAMEWORK, "IOS_IPAPATH");
	String IOS_BUNDLEID=ConfigFileReadWrite.read(FILE_FRAMEWORK, "IOS_BUNDLEID");
	
	//Only for android
	String ANDROID_APPACTIVITY=ConfigFileReadWrite.read(FILE_FRAMEWORK, "ANDROID_APPACTIVITY");
	String ANDROID_APPPACKAGE=ConfigFileReadWrite.read(FILE_FRAMEWORK, "ANDROID_APPPACKAGE");
	String ANDROID_APKPATH=ConfigFileReadWrite.read(FILE_FRAMEWORK, "ANDROID_APKPATH");

	
	String REPORTING_TYPE = ConfigFileReadWrite.read(FILE_FRAMEWORK, "REPORTING_TYPE"); 
	
/*	//GRID HUB URL
	String GRID_HUB_URL = ConfigFileReadWrite.read(FILE_FRAMEWORK, "GRID_HUB_URL");
*/
	public final class OS {
	    public static final String WINDOWS = "windows";
	    public static final String ANDROID = "android";
	    public static final String IOS= "ios";
	    private OS() { }
	}
	
	public final class EXECUTION_ON {

	    public static final String LOCAL = "local";
	    public static final String GRID = "grid";
	    public static final String SAUCELABS = "";
	    private EXECUTION_ON() { }
	}
	
	public final class BROWSER{
	    public static final String IE = "ie";
	    public static final String CHROME = "chrome";
	    public static final String FIREFOX = "firefox";
	    public static final String SAFARI = "safari";
	    private BROWSER() { }
	}
	
	public final class APPLICATION_TYPE{
		public static final String DESKTOP_BROWSER = "desktopbrowser";
		public static final String MAC_BROWSER = "macbrowser";
	    public static final String MOBILE_BROWSER = "mobilebrowser";
	    public static final String MOBILE_APP = "mobileapp";
	    private APPLICATION_TYPE() { }
	}

	public final class MOBILE_APP_TYPE{
		public static final String NATIVE_APP = "native";
		public static final String HYBRID_APP = "hybrid";
	    private MOBILE_APP_TYPE() { }
	}
	public final class SCROLL_TYPE {
	    public static final String UP = "UP";
	    public static final String DOWN = "DOWN";
	    private SCROLL_TYPE() { }
	}
	
	public final class REPORT_TYPE {
	    public static final String CUSTOMREPORT = "customreport";
	    public static final String EXTENTREPORT = "extentreport";
	    private REPORT_TYPE() { }
	}
	
}
